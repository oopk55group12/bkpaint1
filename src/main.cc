
/************************************************************************
        main.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include <QApplication>

#include "mainwindow.hh"
#include "canvas.hh"
#include "widget/colorpicker.hh"
#include "widget/newfileoptiondialog.hh"
#include <cstring>

#define xstr(s) str(s)
#define str(s) #s

//! Kiểm tra file đầu vào có phải là BKPaint's Replay file hay không
/*!
 * \param filename tên file
 * \return filename có phải là Replay file hay không
 */
bool isReplayFile (QString const &filename)
{
    return filename.endsWith (".rpl");
}

int main (int argc, char *argv[])
{
    QApplication a (argc, argv);
    a.setOrganizationName ("Boss14420");
    a.setApplicationName ("BK Paint");
    a.setApplicationVersion (xstr (BKPAINT_VERSION));
    a.setWindowIcon (QIcon (QPixmap(":/logo/Images/bkpaint.ico")));

    Canvas::initCursors();

    QDir appDir (a.applicationDirPath());
#ifdef Q_WS_WIN
    appDir.cd ("palettes");
#else
//    appDir.cd ("../share/palettes/");
    appDir.cdUp();
    appDir.cd ("share");
    appDir.cd ("bkpaint");
    appDir.cd ("palettes");
#endif
    ColorPicker::initPalette (appDir);

    MainWindow *w;

    if (argc > 1) {
        MainWindow::FileTypes ft = MainWindow::Default;

        QString filename = argv[1];

        if (!std::strcmp ("--sample", argv[1]) 
                || !std::strcmp ("-s", argv[1])) 
        {
            ft |= MainWindow::Sample;
            filename = argv[2];
        } 
       
        if (!isReplayFile (filename))
            ft |= MainWindow::Graphic;

        w = new MainWindow (filename, ft);

    } else {
        NewFileOptionDialog dialog;
        int ret = dialog.exec();
        if (ret == NewFileOptionDialog::Accepted) {
            auto opt = dialog.getNewFileOption();
            w = new MainWindow (&opt);
        } else
            return -1;
    }

    w->show();

    return a.exec();
}
