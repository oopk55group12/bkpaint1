
/************************************************************************
        mainwindow.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include <QtGlobal>
#ifdef __clang__
    #define Q_COMPILER_INITIALIZER_LISTS
#endif

#include "mainwindow.hh"
#include "canvas.hh"
#include "widget/colorpicker.hh"
#include "widget/newfileoptiondialog.hh"
#include "widget/resizedialog.hh"
#include "widget/aboutdialog.hh"
#include "widget/toolbar.hh"
#include "widget/setpentoolbar.hh"
#include "widget/setbrushtoolbar.hh"
#include "player.hh"

#include <QAction>
#include <QApplication>
#include <QCloseEvent>
#include <QColorDialog>
#include <QFileDialog>
#include <QFileSystemWatcher>
#include <QLabel>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPainter>
#include <QPixmap>
#include <QScrollArea>
#include <QSettings>
#include <QSignalMapper>
#include <QSlider>
#include <QSpinBox>
#include <QStatusBar>
#include <QThread>
#include <QProcess>
#include <QDesktopServices>
#include <QUrl>

#include <QDebug>

//! Phương thức khởi tạo. Tạo file mới
/*!
 * Khởi tạo một cửa sổ chính mới có file đi kèm là một file mới với tùy chọn
 * cho trước.
 * \param option tùy chọn cho file mới
 */
MainWindow::MainWindow (NewFileOption const *option)
{
    player = new Player (option, this);
    canvas = new Canvas (player, this);
    init();
    setCurrentFile ("");
}

//! Phương thức khởi tạo. Mở file
/*!
 * Khởi tạo một cửa sổ chính mới với nội dung lấy từ file cho trước.
 * \param filename file đầu vào
 * \param ft kiểu của file
 */
MainWindow::MainWindow (QString const &filename, FileTypes ft)
{
    player = new Player (this); 
    canvas = new Canvas (player, this);
    init();
    setCurrentFile ("");
    loadFile (filename, ft);
}

//! Phương thức hủy
MainWindow::~MainWindow()
{
#ifdef Q_WS_MAC
    menuBar->deleteLater();
#endif
}

//! Khởi tạo các thuộc tính của MainWindow
/*!
 * \sa initActions(), initWidgets(), initStatusBar(), initToolbars(),
 * initMenus()
 */
void MainWindow::init()
{
    //    setupUi(this);

    connect (player, SIGNAL (playerStopped()), SLOT (playerStopped()));
    connect (player, SIGNAL (playerModified (bool)),
             SLOT (playerModified (bool)));

    on_actionFreeDraw_triggered();

/*     fileWatcher = new QFileSystemWatcher (this);
 *     connect (fileWatcher, SIGNAL (fileChanged (QString const &)),
 *              SLOT (fileChanged (QString const &)));
 * 
 *     fileWatcherBlockTimer = new QTimer (this);
 *     fileWatcherBlockTimer->setInterval (BlockTime);
 *     fileWatcherBlockTimer->setSingleShot (true);
 *     connect (fileWatcherBlockTimer, SIGNAL (timeout()),
 *              SLOT (unblockFileWatcher()));
 */

    initActions();
    initWidgets();
    initStatusBar();
    initToolbars();
    initMenus();

    isUntitled = true;

    setAttribute (Qt::WA_DeleteOnClose);
    //setUnifiedTitleAndToolBarOnMac (true);

    readSettings();
}

//! Đọc và thực hiện từ file cấu hình
/*!
 * \sa writeSettings()
 */
void MainWindow::readSettings()
{
    QSettings settings;
    QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings.value("size", QSize(400, 530)).toSize();
    move(pos);
    resize(size);
}

//! Ghi vào file cấu hình
/*!
 * \sa readSettings()
 */
void MainWindow::writeSettings()
{
    QSettings settings;
    settings.setValue ("pos", pos());
    settings.setValue ("size", size());
}

//! Khởi tạo các widget
/*!
 * Các widget này không phải là ToolBar, StatusBar, Menu
 * \sa init(), initActions(), initToolbars(), initStatusBar(), initMenus()
 */
void MainWindow::initWidgets()
{
    //
    // central widget
    //

    scrollArea = new QScrollArea (this);
    //    QVBoxLayout *vLayout = new QVBoxLayout(scrollArea);
    //    vLayout->addStretch();
    //    vLayout->addWidget (canvas);
    //    vLayout->addStretch();
    //    vLayout->addWidget (new QLabel("hi", this));
    //    scrollArea->setLayout (vLayout);
    scrollArea->setWidget (canvas);
    //    scrollArea->setContentsMargins (QMargins(90,90,90,90));
    setCentralWidget (scrollArea);


    //
    // labelPosition
    //

    labelPosition = new QLabel ("x: 0\ny: 0", this);
    labelPosition->setMinimumWidth (50);

    connect (canvas, SIGNAL (cursorMoved (QPointF)),
             SLOT (updateLabelPosition (QPointF)));

    //
    // zoomFactorSpinBox
    //

    zoomFactorSpinBox = new QSpinBox (this);
    zoomFactorSpinBox->setRange (1, 1600);
    zoomFactorSpinBox->setValue (100);
    zoomFactorSpinBox->setSuffix (" %");
    zoomFactorSpinBox->setKeyboardTracking (false);

    connect (zoomFactorSpinBox, SIGNAL (valueChanged (int)),
             canvas, SLOT (setZoomFactor (int)));

    //
    // erase radius slider
    //

    eraseRadiusSlider = new QSlider (Qt::Vertical, this);
    eraseRadiusSlider->setRange (1, 40);
    eraseRadiusSlider->setMaximumSize (50, 50);
    eraseRadiusSlider->setValue (3);
    eraseRadiusSlider->setToolTip (tr ("Erase radius"));
    eraseRadiusSlider->setVisible (false);
    connect (eraseRadiusSlider, SIGNAL (valueChanged (int)),
             canvas, SLOT (setEraseRadius (int)));
    canvas->setEraseRadius (eraseRadiusSlider->value());

    
    //
    // speed slider
    //

    replaySpeedSlider = new QSlider (Qt::Horizontal, this);
    replaySpeedSlider->setMaximumSize (150, 150);
    replaySpeedSlider->setRange (10, 500);
    replaySpeedSlider->setValue (100);
    replaySpeedSlider->setTracking (false);
    replaySpeedSlider->setToolTip (tr ("Replay speed"));
    connect (replaySpeedSlider, SIGNAL (valueChanged (int)),
             player, SLOT (setIntervalBySpeed (int)));
    player->setIntervalBySpeed (replaySpeedSlider->value());


    //
    // colorPicker
    //

    colorPicker = new ColorPicker (this);
    connect (colorPicker, SIGNAL (penColorPicked (QColor)),
             SLOT (setPenColor (QColor)));
    connect (colorPicker, SIGNAL (brushColorPicked (QColor)),
             SLOT (setBrushColor (QColor)));
    connect (colorPicker, SIGNAL (colorHovered (QString const &)),
             SLOT (hoveredColor (QString const &)));


    //
    // regularPolygonNoCornerSpinBox 
    //

    regularPolygonNoCornerSpinBox = new QSpinBox (this);
    regularPolygonNoCornerSpinBox->setRange (3, 20);
    connect (regularPolygonNoCornerSpinBox, SIGNAL (valueChanged (int)),
             canvas, SLOT (setNoCorner (int)));
    regularPolygonNoCornerSpinBox->setValue (5);
}

//! Khởi tạo các action
/*!
 * \sa QAction, init(), initMenus(), initToolbars(), initStatusBar(), initWidgets()
 */
void MainWindow::initActions()
{
    const QString iconPath = ":/icons/Images/IconsSet/"
                      + QString::number (ToolBar::IconWidth) + 'x'
                      + QString::number (ToolBar::IconHeight) + '/';


    //
    // File actions
    //

    actionNew = new QAction (tr ("&New"), this);
    actionNew->setShortcut (QKeySequence::New);
    actionNew->setIcon (QIcon (iconPath + "FileNewIcon.png"));
    connect (actionNew, SIGNAL (triggered()),
             SLOT (on_actionNew_triggered()));

    actionOpen = new QAction (tr ("&Open"), this);
    actionOpen->setShortcut (QKeySequence::Open);
    actionOpen->setIcon (QIcon (iconPath + "FileOpenIcon.png"));
    connect (actionOpen, SIGNAL (triggered()),
             SLOT (on_actionOpen_triggered()));

    actionOpenSample = new QAction (tr ("Open &Sample"), this);
//    actionOpenSample->setShortcut (QKeySequence::OpenSample);
//    actionOpenSample->setIcon (QIcon (iconPath + "FileOpenSampleIcon.png"));
    connect (actionOpenSample, SIGNAL (triggered()),
             SLOT (on_actionOpenSample_triggered()));

    actionSave = new QAction (tr ("&Save"), this);
    actionSave->setShortcut (QKeySequence::Save);
    actionSave->setIcon (QIcon (iconPath + "FileSaveIcon.png"));
    connect (actionSave, SIGNAL (triggered()),
             SLOT (on_actionSave_triggered()));

    actionSave_As = new QAction (tr ("Save &As"), this);
    actionSave_As->setShortcut (QKeySequence::SaveAs);
    actionSave_As->setIcon (QIcon (iconPath + "FileSaveAsIcon.png"));
    connect (actionSave_As, SIGNAL (triggered()),
             SLOT (on_actionSave_As_triggered()));

    actionExport = new QAction (tr ("&Export to image"), this);
    actionExport->setIcon (QIcon (iconPath + "FileExportIcon.png"));
    connect (actionExport, SIGNAL (triggered()),
             SLOT (on_actionExport_triggered()));

    actionImport = new QAction (tr ("&Import from image"), this);
    actionImport->setIcon (QIcon (iconPath + "FileImportIcon.png"));
    connect (actionImport, SIGNAL (triggered()),
             SLOT (on_actionImport_triggered()));

    for (int i = 0; i != MaxRecentFiles; ++i) {
        recentFileActions[i] = new QAction (this);
        recentFileActions[i]->setVisible (false);
        connect (recentFileActions[i], SIGNAL (triggered()),
                 SLOT (openRecentFile()));
    }

    actionReload = new QAction (tr ("&Reload file"), this);
    actionReload->setIcon (QIcon (iconPath + "FileRefreshIcon.png"));
    connect (actionReload, SIGNAL (triggered()), SLOT (reloadCurrentFile()));

//    actionClose = new QAction (tr ("&Close"), this);
//    actionClose->setShortcut (QKeySequence::Close);
//    actionClose->setIcon (QIcon (iconPath + "FileCloseIcon.png"));
//    connect (actionClose, SIGNAL (triggered()), SLOT (close()));

    actionExit = new QAction (tr ("E&xit"), this);
    actionExit->setShortcut (QKeySequence::Quit);
    actionExit->setIcon (QIcon (iconPath + "FileCloseIcon.png"));
    connect (actionExit, SIGNAL (triggered()), qApp, SLOT (closeAllWindows()));

    //
    // Draw mode actions
    //

    drawModeActionGroup = new QActionGroup (this);

    actionFreeDraw = new QAction (tr ("&Free Draw"), this);
    actionFreeDraw->setShortcut (tr ("F"));
    actionFreeDraw->setIcon (QIcon (iconPath + "PencilToolIcon.png"));
    actionFreeDraw->setCheckable (true);
    actionFreeDraw->setChecked (true);
    drawModeActionGroup->addAction (actionFreeDraw);
    connect (actionFreeDraw, SIGNAL (triggered()),
             SLOT (on_actionFreeDraw_triggered()));

    actionLine = new QAction (tr ("&Line"), this);
    actionLine->setShortcut (tr ("L"));
    actionLine->setIcon (QIcon (iconPath + "LineToolIcon.png"));
    actionLine->setCheckable (true);
    drawModeActionGroup->addAction (actionLine);
    connect (actionLine, SIGNAL (triggered()),
             SLOT (on_actionLine_triggered()));

    actionRectangle = new QAction (tr ("&Rectangle"), this);
    actionRectangle->setShortcut (tr ("R"));
    actionRectangle->setIcon (QIcon (iconPath + "RectangleToolIcon.png"));
    actionRectangle->setCheckable (true);
    drawModeActionGroup->addAction (actionRectangle);
    connect (actionRectangle, SIGNAL (triggered()),
             SLOT (on_actionRectangle_triggered()));

    actionEllipse = new QAction (tr ("&Ellipse"), this);
    actionEllipse->setShortcut (tr ("E"));
    actionEllipse->setIcon (QIcon (iconPath + "EllipseToolIcon.png"));
    actionEllipse->setCheckable (true);
    drawModeActionGroup->addAction (actionEllipse);
    connect (actionEllipse, SIGNAL (triggered()),
             SLOT (on_actionEllipse_triggered()));

    actionPolygon = new QAction (tr ("&Polygon"), this);
    actionPolygon->setShortcut (tr ("P"));
    actionPolygon->setIcon (QIcon (iconPath + "PolygonToolIcon.png"));
    actionPolygon->setCheckable (true);
    drawModeActionGroup->addAction (actionPolygon);
    connect (actionPolygon, SIGNAL (triggered()),
             SLOT (on_actionPolygon_triggered()));

    actionCurve = new QAction (tr ("&Curve"), this);
    actionCurve->setShortcut (tr ("U"));
    actionCurve->setIcon (QIcon (iconPath + "CurveToolIcon.png"));
    actionCurve->setCheckable (true);
    drawModeActionGroup->addAction (actionCurve);
    connect (actionCurve, SIGNAL (triggered()),
             SLOT (on_actionCurve_triggered()));

    menuRegularPolygonAndStar = new QMenu ("&Regular polygon and star", this);
    menuRegularPolygonAndStar->setIcon (
                QIcon (iconPath + "RegularPolygonAndStarToolIcon.png"));

    actionRegularPolygon = new QAction (tr ("&Regular Polygon"), this);
    actionRegularPolygon->setShortcut (tr ("G"));
    actionRegularPolygon->setIcon (
                QIcon (iconPath + "RegularPolygonToolIcon.png"));
    actionRegularPolygon->setCheckable (true);
    drawModeActionGroup->addAction (actionRegularPolygon);
    menuRegularPolygonAndStar->addAction (actionRegularPolygon);
    connect (actionRegularPolygon, SIGNAL (triggered()),
             SLOT (on_actionRegularPolygon_triggered()));

    actionStar = new QAction (tr ("&Star"), this);
    actionStar->setShortcut (tr ("S"));
    actionStar->setIcon (QIcon (iconPath + "StarToolIcon.png"));
    actionStar->setCheckable (true);
    drawModeActionGroup->addAction (actionStar);
    menuRegularPolygonAndStar->addAction (actionStar);
    connect (actionStar, SIGNAL (triggered()),
             SLOT (on_actionStar_triggered()));

    actionFill = new QAction (tr ("F&ill"), this);
    actionFill->setShortcut (tr ("B"));
    actionFill->setIcon (QIcon (iconPath + "PaintBucketIcon.png"));
    actionFill->setCheckable (true);
    drawModeActionGroup->addAction (actionFill);
    connect (actionFill, SIGNAL (triggered()),
             SLOT (on_actionFill_triggered()));

    actionErase = new QAction (tr ("Era&se"), this);
    actionErase->setCheckable (true);
    actionErase->setIcon (QIcon (iconPath + "EraserToolIcon.png"));
    actionErase->setShortcut (tr ("C"));
    drawModeActionGroup->addAction (actionErase);
    connect (actionErase, SIGNAL (triggered()),
             SLOT (on_actionErase_triggered()));



    //
    // Edit actions
    //

    actionUndo = new QAction (tr ("&Undo"), this);
    actionUndo->setShortcut (QKeySequence::Undo);
    actionUndo->setIcon (QIcon (iconPath + "UndoToolIcon.png"));
    actionUndo->setEnabled (false);
    connect (player, SIGNAL (canUndoChanged (bool)),
             actionUndo, SLOT (setEnabled (bool)));
    connect (actionUndo, SIGNAL (triggered()),
             SLOT (on_actionUndo_triggered()));

    actionRedo = new QAction (tr ("&Redo"), this);
    actionRedo->setShortcut (QKeySequence::Redo);
    actionRedo->setIcon (QIcon (iconPath + "RedoToolIcon.png"));
    actionRedo->setEnabled (false);
    connect (player, SIGNAL (canRedoChanged (bool)),
             actionRedo, SLOT (setEnabled (bool)));
    connect (actionRedo, SIGNAL (triggered()),
             SLOT (on_actionRedo_triggered()));

    actionResize = new QAction (tr ("Re&size"), this);
    connect (actionResize, SIGNAL (triggered()),
             SLOT (on_actionResize_triggered()));


    //
    // Replay actions
    //

    replayActionGroup = new QActionGroup (this);

    actionReplay = new QAction (tr ("&Replay"), this);
    actionReplay->setShortcut (tr ("Ctrl+P"));
    actionReplay->setIcon (QIcon (iconPath + "PlayerPlayIcon.png"));
    replayActionGroup->addAction (actionReplay);
    connect (actionReplay, SIGNAL (triggered()),
             SLOT (on_actionReplay_triggered()));

    actionPause = new QAction (tr ("&Pause"), this);
    actionPause->setShortcut (tr ("Space"));
    actionPause->setIcon (QIcon (iconPath + "PlayerPauseIcon.png"));
    actionPause->setVisible (false);
    replayActionGroup->addAction (actionPause);
    connect (actionPause, SIGNAL (triggered()),
             SLOT (on_actionPause_triggered()));

    actionResume = new QAction (tr ("R&esume"), this);
    actionResume->setShortcut (tr ("Space"));
    actionResume->setIcon (QIcon (iconPath + "PlayerPlayIcon.png"));
    actionResume->setVisible (false);
    replayActionGroup->addAction (actionResume);
    connect (actionResume, SIGNAL (triggered()),
             SLOT (on_actionResume_triggered()));

    actionStop = new QAction (tr ("&Stop"), this);
    actionStop->setIcon (QIcon (iconPath + "PlayerStopIcon.png"));
    actionStop->setEnabled (false);
    replayActionGroup->addAction (actionStop);
    connect (actionStop, SIGNAL (triggered()),
             SLOT (on_actionStop_triggered()));


    //
    // Zoom actions
    //

    actionOriginal_size = new QAction (tr ("Original &Size"), this);
    actionOriginal_size->setIcon (QIcon (iconPath + "OriginalSizeIcon.png"));
    actionOriginal_size->setShortcut (tr ("Ctrl+0"));
    connect (actionOriginal_size, SIGNAL (triggered()),
             SLOT (on_actionOriginal_size_triggered()));

    actionZoom_1_2 = new QAction (tr ("Zoom &1:2"), this);
    actionZoom_1_2->setShortcut (tr ("Ctrl+1"));
    connect (actionZoom_1_2, SIGNAL (triggered()),
             SLOT (on_actionZoom_1_2_triggered()));

    actionZoom_2_1 = new QAction (tr ("Zoom &2:1"), this);
    actionZoom_2_1->setShortcut (tr ("Ctrl+2"));
    connect (actionZoom_2_1, SIGNAL (triggered()),
             SLOT (on_actionZoom_2_1_triggered()));

    actionZoom_In = new QAction (tr ("Zoom &In"), this);
    actionZoom_In->setShortcut (QKeySequence::ZoomIn);
    actionZoom_In->setIcon (QIcon (iconPath + "ZoomInIcon.png"));
    connect (actionZoom_In, SIGNAL (triggered()),
             SLOT (on_actionZoom_In_triggered()));

    actionZoom_Out = new QAction (tr ("Zoom &Out"), this);
    actionZoom_Out->setIcon (QIcon (iconPath + "ZoomOutIcon.png"));
    actionZoom_Out->setShortcut (QKeySequence::ZoomOut);
    connect (actionZoom_Out, SIGNAL (triggered()),
             SLOT (on_actionZoom_Out_triggered()));


    // Renders hint actions

    renderHintActionGroup = new QActionGroup (this);
    renderHintActionGroup->setExclusive (false);

    actionAntialiasing = new QAction (tr ("&Antialiasing"), this);
    actionAntialiasing->setData (QPainter::Antialiasing);
    actionAntialiasing->setCheckable (true);
    renderHintActionGroup->addAction (actionAntialiasing);
    connect (actionAntialiasing, SIGNAL (toggled (bool)), 
             SLOT (setRenderHint (bool)));

    actionTextAntialiasing = new QAction (tr ("&Text Antialiasing"), this);
    actionTextAntialiasing->setData (QPainter::TextAntialiasing);
    actionTextAntialiasing->setCheckable (true);
    renderHintActionGroup->addAction (actionTextAntialiasing);
    connect (actionTextAntialiasing, SIGNAL (toggled (bool)), 
             SLOT (setRenderHint (bool)));

    actionSmoothPixmapTransform = 
                new QAction (tr ("&Smooth Pixmap Transform"), this);
    actionSmoothPixmapTransform->setData (QPainter::SmoothPixmapTransform);
    actionSmoothPixmapTransform->setCheckable (true);
    renderHintActionGroup->addAction (actionSmoothPixmapTransform);
    connect (actionSmoothPixmapTransform, SIGNAL (toggled (bool)), 
                SLOT (setRenderHint (bool)));

    actionHighQualityAntialiasing = 
                new QAction (tr ("&High Quality Antialiasing"), this);
    actionHighQualityAntialiasing->setData (QPainter::HighQualityAntialiasing);
    actionHighQualityAntialiasing->setCheckable (true);
    renderHintActionGroup->addAction (actionHighQualityAntialiasing);
    connect (actionHighQualityAntialiasing, SIGNAL (toggled (bool)), 
                SLOT (setRenderHint (bool)));

    actionNonCosmeticDefaultPen = 
                new QAction (tr ("&Non Cosmetic Default Pen"), this);
    actionNonCosmeticDefaultPen->setData (QPainter::NonCosmeticDefaultPen);
    actionNonCosmeticDefaultPen->setCheckable (true);
    renderHintActionGroup->addAction (actionNonCosmeticDefaultPen);
    connect (actionNonCosmeticDefaultPen, SIGNAL (toggled (bool)), 
                SLOT (setRenderHint (bool)));


    // Help actions
    //

    actionManual = new QAction (tr ("&Help"), this);
    actionManual->setShortcut (QKeySequence::HelpContents);
    connect (actionManual, SIGNAL (triggered()),
             SLOT (on_actionManual_triggered()));

    actionAbout = new QAction (tr ("&About"), this);
    connect (actionAbout, SIGNAL (triggered()),
             SLOT (on_actionAbout_triggered()));

    actionAboutQt = new QAction (tr ("About &Qt"), this);
    connect (actionAboutQt, SIGNAL (triggered()), qApp, SLOT (aboutQt()));
}

//! Khởi tạo thanh trạng thái
/*!
 * \sa QStatusBar, init(), initActions(), initToolbars(), initWidgets(), initMenus()
 */
void MainWindow::initStatusBar()
{
    statusBar = new QStatusBar (this);
    setStatusBar (statusBar);

    statusBar->addPermanentWidget (labelPosition);
    QLabel *zoomLabel = new QLabel (tr ("Zoom"), this);

    zoomLabel->setBuddy (zoomFactorSpinBox);
    statusBar->addPermanentWidget (zoomLabel);
    statusBar->addPermanentWidget (zoomFactorSpinBox);
}

//! Khởi tạo các thanh công cụ
/*!
 * \sa ToolBar, init(), initActions(), initWidgets(), initMenus(),
 * initStatusBar()
 */
void MainWindow::initToolbars()
{

    //
    // File ToolBar
    //

    fileToolBar = new ToolBar (tr ("File toolbar"), this);
    addToolBar (Qt::TopToolBarArea, fileToolBar);

    fileToolBar->addAction (actionNew);
    fileToolBar->addAction (actionOpen);
    fileToolBar->addAction (actionSave);
    fileToolBar->addAction (actionSave_As);
    fileToolBar->addAction (actionReload);
    fileToolBar->addAction (actionImport);
    fileToolBar->addAction (actionExport);
    fileToolBar->addAction (actionExit);


    //
    // Main ToolBar
    //

    mainToolBar = new ToolBar (tr ("Draw toolbar"), this);
    addToolBar (Qt::LeftToolBarArea, mainToolBar);

    mainToolBar->addAction (actionFreeDraw);
    mainToolBar->addAction (actionLine);
    mainToolBar->addAction (actionRectangle);
    mainToolBar->addAction (actionEllipse);
    mainToolBar->addAction (actionPolygon);
    mainToolBar->addAction (actionCurve);
    mainToolBar->addAction (menuRegularPolygonAndStar->menuAction());
    mainToolBar->addAction (actionFill);
    mainToolBar->addAction (actionErase);

    mainToolBar->addSeparator();

    // regularPolygonNoCornerSpinBox
    QAction *cornerLabelAction =
        mainToolBar->addWidget (new QLabel (tr ("Corners: ")));
    connect (actionRegularPolygon, SIGNAL (toggled (bool)),
             cornerLabelAction, SLOT (setVisible (bool)));
    connect (actionStar, SIGNAL (toggled (bool)),
             cornerLabelAction, SLOT (setVisible (bool)));
    cornerLabelAction->setVisible (false);

    QAction *cornerSpinBoxAction = 
        mainToolBar->addWidget (regularPolygonNoCornerSpinBox);
    connect (actionRegularPolygon, SIGNAL (toggled (bool)),
             cornerSpinBoxAction, SLOT (setVisible (bool)));
    connect (actionStar, SIGNAL (toggled (bool)),
             cornerSpinBoxAction, SLOT (setVisible (bool)));
    cornerSpinBoxAction->setVisible (false);

    // erase radius slider
    QAction *eraseRadiusLabelAction =
        mainToolBar->addWidget (new QLabel (tr ("Radius: ")));
    connect (actionErase, SIGNAL (toggled (bool)),
             eraseRadiusLabelAction, SLOT (setVisible (bool)));
    eraseRadiusLabelAction->setVisible (false);

    QAction *eraseRadiusAction = mainToolBar->addWidget (eraseRadiusSlider);
    connect (actionErase, SIGNAL (toggled (bool)),
             eraseRadiusAction, SLOT (setVisible (bool)));
    connect (mainToolBar, SIGNAL (orientationChanged (Qt::Orientation)),
             eraseRadiusSlider, SLOT (setOrientation (Qt::Orientation)));

    //
    // Replay toolbar
    //

    replayToolBar = new ToolBar (tr ("Replay toolbar"), this);
    addToolBar (Qt::TopToolBarArea, replayToolBar);

    replayToolBar->addAction (actionReplay);
    replayToolBar->addAction (actionPause);
    replayToolBar->addAction (actionResume);
    replayToolBar->addAction (actionStop);

    // speed slider
    replayToolBar->addSpace (ToolBar::IconWidth / 2);
    replayToolBar->addWidget (new QLabel (tr ("Speed: ")));
    replayToolBar->addWidget (replaySpeedSlider);

    // Thay đổi hướng của slider khi toolbar đổi hướng
    connect (replayToolBar, SIGNAL (orientationChanged (Qt::Orientation)),
             replaySpeedSlider, SLOT (setOrientation (Qt::Orientation)));


    //
    // Zoom ToolBar
    //

    zoomToolBar = new ToolBar (tr ("Zoom toolbar"), this);
    addToolBar (Qt::TopToolBarArea, zoomToolBar);

    zoomToolBar->addAction (actionZoom_In);
    zoomToolBar->addAction (actionOriginal_size);
    zoomToolBar->addAction (actionZoom_Out);


    //
    // SetPen toolbar
    //

    addToolBarBreak (Qt::TopToolBarArea);

    setPenToolBar = new SetPenToolBar (tr ("Set Pen toolbar"), this);
    addToolBar (Qt::TopToolBarArea, setPenToolBar);

    connect (setPenToolBar, SIGNAL (penColorNeeded()),
             SLOT (openPenColorDialog()));
    connect (setPenToolBar, SIGNAL (penStyleChanged (int)),
             SLOT (setPenStyle (int)));
    connect (setPenToolBar, SIGNAL (penWidthChanged (int)),
             SLOT (setPenWidth (int)));

    // Render hint

    setPenToolBar->addSpace (ToolBar::IconWidth / 2);
    renderHintMenu = new QMenu (tr ("Render hint"), this);
    renderHintMenu->addAction (actionAntialiasing);
    renderHintMenu->addAction (actionTextAntialiasing);
    renderHintMenu->addAction (actionSmoothPixmapTransform);
    renderHintMenu->addAction (actionHighQualityAntialiasing);
    renderHintMenu->addAction (actionNonCosmeticDefaultPen);
    setPenToolBar->addAction (renderHintMenu->menuAction());

    
    //
    // SetBrush toolbar
    //

    setBrushToolBar = new SetBrushToolBar (tr ("Set Brush toolbar"), this);
    addToolBar (Qt::TopToolBarArea, setBrushToolBar);

    connect (setBrushToolBar, SIGNAL (brushColorNeeded()),
             SLOT (openBrushColorDialog()));
    connect (setBrushToolBar, SIGNAL (brushStyleChanged (int)),
             SLOT (setBrushStyle (int)));

    //
    // Color picker toolbar
    //

    colorPickerToolBar = new ToolBar (tr ("Color-picker toolbar"), this);
    colorPickerToolBar->setMovable (false);
    colorPickerToolBar->setFloatable (false);
    addToolBar (Qt::BottomToolBarArea, colorPickerToolBar);

    QScrollArea *colorPickerScrollArea = new QScrollArea (this);

    colorPickerScrollArea->setHorizontalScrollBarPolicy (Qt::ScrollBarAlwaysOn);
    colorPickerScrollArea->setVerticalScrollBarPolicy (Qt::ScrollBarAlwaysOff);
    colorPickerScrollArea->setWidget (colorPicker);

    colorPickerToolBar->setMaximumHeight (colorPicker->height() + 24);
    colorPickerToolBar->addWidget (colorPickerScrollArea);

}

//! Khởi tạo các menu
/*!
 * \sa QMenu, init(), initActions(), initWidgets(), initStatusBar(),
 * initToolbars()
 */
void MainWindow::initMenus()
{
#ifdef Q_WS_MAC
    menuBar = new QMenuBar();
#else
    menuBar = new QMenuBar (this);
#endif
    setMenuBar (menuBar);

    //
    // File menu
    //

    menuFile = new QMenu (tr ("&File"), menuBar);
    menuBar->addAction (menuFile->menuAction());

    menuFile->addAction (actionNew);
    menuFile->addAction (actionOpen);
    menuFile->addAction (actionOpenSample);
    menuFile->addAction (actionSave);
    menuFile->addAction (actionSave_As);
    menuFile->addAction (actionReload);
//    menuFile->addAction (actionClose);
    menuFile->addSeparator();
    menuFile->addAction (actionImport);
    menuFile->addAction (actionExport);
    menuFile->addSeparator();
    for (QAction * act : recentFileActions)
        menuFile->addAction (act);
    menuFile->addSeparator();
    menuFile->addAction (actionExit);

    updateRecentFileActions();

    //
    // Edit menu
    //

    menuEdit = new QMenu (tr ("&Edit"), menuBar);
    menuBar->addAction (menuEdit->menuAction());

    menuEdit->addAction (actionUndo);
    menuEdit->addAction (actionRedo);
    menuEdit->addSeparator();
    menuEdit->addAction (actionResize);

    //
    // Draw menu
    //

    menuDraw = new QMenu (tr ("&Draw"), menuBar);
    menuBar->addAction (menuDraw->menuAction());

    menuDraw->addAction (actionFreeDraw);
    menuDraw->addAction (actionLine);
    menuDraw->addAction (actionRectangle);
    menuDraw->addAction (actionEllipse);
    menuDraw->addAction (actionPolygon);
    menuDraw->addAction (actionCurve);
    menuDraw->addMenu (menuRegularPolygonAndStar);
    menuDraw->addAction (actionFill);
    menuDraw->addAction (actionErase);

    menuDraw->addSeparator();
    menuDraw->addMenu (renderHintMenu);

    //
    // Replay menu
    //

    menuReplay = new QMenu (tr ("&Replay"), menuBar);
    menuBar->addAction (menuReplay->menuAction());

    menuReplay->addAction (actionReplay);
    menuReplay->addAction (actionPause);
    menuReplay->addAction (actionResume);
    menuReplay->addAction (actionStop);

    //
    // View menu
    //

    menuView = new QMenu (tr ("&View"), menuBar);
    menuBar->addAction (menuView->menuAction());

    menuZoom = new QMenu (tr ("&Zoom"), menuView);
    menuView->addAction (menuZoom->menuAction());

    menuZoom->addAction (actionOriginal_size);
    menuZoom->addAction (actionZoom_1_2);
    menuZoom->addAction (actionZoom_2_1);
    menuZoom->addSeparator();
    menuZoom->addAction (actionZoom_In);
    menuZoom->addAction (actionZoom_Out);

    menuToolbar = new QMenu (tr ("ToolBar"), menuView);
    menuView->addAction (menuToolbar->menuAction());

    menuToolbar->addAction (mainToolBar->toggleViewAction());
    menuToolbar->addAction (fileToolBar->toggleViewAction());
    menuToolbar->addAction (replayToolBar->toggleViewAction());
    menuToolbar->addAction (setPenToolBar->toggleViewAction());
    menuToolbar->addAction (setBrushToolBar->toggleViewAction());
    menuToolbar->addAction (colorPickerToolBar->toggleViewAction());
    menuToolbar->addAction (zoomToolBar->toggleViewAction());


    //
    // Help menu
    //

    menuHelp = new QMenu (tr ("&Help"), menuBar);
    menuBar->addMenu (menuHelp);

    menuHelp->addAction (actionManual);
    menuHelp->addSeparator();
    menuHelp->addAction (actionAbout);
    menuHelp->addAction (actionAboutQt);
}

//! Xử lý sự kiện đóng cửa sổ
/*!
 * Hỏi nguời dùng lưu file truớc khi đóng cửa sổ
 * \param event Sự kiện đóng cửa sổ
 */
void MainWindow::closeEvent (QCloseEvent *event)
{
    if (maybeSave()) {
        writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
}

//! Mở / vô hiệu hoá các thành phần điểu khiển liên quan đến việc vẽ
/*!
 * Được dùng khi ở chế độ Replay. Mọi hành động liên quan đến việc vẽ đều phải
 * bị vô hiệu hoá.
 */
void MainWindow::enablePainting (bool b)
{
    colorPickerToolBar->setEnabled (b);
    drawModeActionGroup->setEnabled (b);
    renderHintActionGroup->setEnabled (b);
    setPenToolBar->setEnabled (b);
    setBrushToolBar->setEnabled (b);
    menuEdit->setEnabled (b);
}


//! Thay đổi độ rộng nét vẽ
/*!
 * Được gọi khi người dùng thay đổi độ rộng nét vẽ bằng #setPenToolBar
 * \param value độ rộng mới
 * \sa SetPenToolBar::penWidthChanged(), QAbstractSlider::valueChanged()
 */
void MainWindow::setPenWidth (int value)
{
    QPen pen = canvas->getPen();

    pen.setWidth (value);
    canvas->setPen (pen);
}

//! Thay đổi kiểu nét vẽ
/*!
 * Được gọi đến khi người dùng chọn một kiểu nét vẽ từ #setPenToolBar
 * \param id kiểu nét vẽ
 * \sa SetPenToolBar::penStyleChanged()
 */
void MainWindow::setPenStyle (int id)
{
    setPenToolBar->reloadPenStyleIcon (id);

    QPen pen = canvas->getPen();

    pen.setStyle (static_cast<Qt::PenStyle> (id));
    canvas->setPen (pen);
}

//! Thay đổi RenderHint
/*!
 * Được gọi khi người dùng bật / tắt một RenderHint từ #renderHintMenu.
 * RenderHint sẽ được xác định bằng QAction phát ra tín hiệu này
 * \param on trạng thái mới (bật / tắt)
 * \sa renderHintMenu
 */
void MainWindow::setRenderHint (bool on)
{
    auto hint = static_cast<QPainter::RenderHint> (
                    qobject_cast<QAction*>(sender())->data().toInt());
    canvas->setRenderHint (hint, on);
}


//! Thay đổi kiểu chổi lông
/*!
 * Được gọi đến khi người dùng chọn một kiểu chổi lông từ #setBrushToolBar
 * \param id kiểu chổi lông
 * \sa SetBrushToolBar::brushStyleChanged()
 */
void MainWindow::setBrushStyle (int id)
{
    setBrushToolBar->reloadBrushStyleIcon (id);

    QBrush brush = canvas->getBrush();

    brush.setStyle (static_cast<Qt::BrushStyle> (id));
    canvas->setBrush (brush);
}

/* void MainWindow::fileChanged (QString const &path)
 * {
 *     int ret = QMessageBox::warning (this,
 *               windowTitle(),
 *               tr
 *               ("The file \"%1\" has been modified outside %2."
 *                "Do you want to reload it ?")
 *               .arg (path).arg (qApp->applicationName()),
 *               QMessageBox::Cancel | QMessageBox::Ok);
 * 
 *     if (ret == QMessageBox::Ok)
 *         reloadCurrentFile();
 * 
 *     qDebug() << "Thread" << QThread::currentThreadId() << "," << path <<
 *               "changed";
 * 
 * }
 */

/* void MainWindow::unblockFileWatcher()
 * {
 *     fileWatcher->blockSignals (false);
 * }
 */

void MainWindow::on_actionNew_triggered()
{
    QProcess::startDetached (qApp->applicationFilePath(), {});
    
    if (isUntitled && !isWindowModified())
        close();
}

void MainWindow::on_actionOpen_triggered()
{
    static QString filters = tr ("BKPaint Replay file (*.rpl)")
                             + ";;" + tr ("All files (*.*)");

    QSettings settings;
    QString path = settings.value ("last_path", curFile).toString();

    QString filename =
        QFileDialog::getOpenFileName (this, tr ("Open file"), path, filters);

    if (filename.isEmpty() || filename == curFile)
        return;

    settings.setValue ("last_path", QFileInfo (filename).absolutePath());

    if (isUntitled && !isWindowModified())
        loadFile (filename);
    else {
        QProcess::startDetached (qApp->applicationFilePath(),
                                 QStringList (filename));
    }
}

void MainWindow::on_actionOpenSample_triggered()
{
#ifdef Q_WS_WIN
    static const QString samplePath = qApp->applicationDirPath() + "\\samples";
#else
    static const QString samplePath = qApp->applicationDirPath()
                                        + "/../share/bkpaint/samples/";
#endif

    static const QString filters = tr ("All files (*.*)") + ";;" + 
                                   tr ("BKPaint Replay file (*.rpl)");

    QString filename = QFileDialog::getOpenFileName (this, tr ("Open file"), 
                                                     samplePath, filters);

    if (filename.isEmpty() || filename == curFile)
        return;

    if (isUntitled && !isWindowModified()) {
        if (filename.endsWith (".rpl"))
            loadFile (filename, Sample);
        else
            loadFile (filename, Sample | Graphic);
    } else {
        QProcess::startDetached (qApp->applicationFilePath(),
                                { "--sample", filename });
    }
}

bool MainWindow::on_actionSave_triggered()
{
    if (isUntitled) {
        return on_actionSave_As_triggered();
    } else {
        return saveReplayFile (curFile);
    }
}

void MainWindow::reloadCurrentFile()
{
    if(!isUntitled)
        loadFile (curFile);
}

bool MainWindow::on_actionSave_As_triggered()
{
    static QString filters = tr ("BKPaint Replay file (*.rpl)")
                             + ";;" + tr ("All files (*.*)");
    QSettings settings;
    QString path = settings.value ("last_path", curFile).toString();

    QString filename =
        QFileDialog::getSaveFileName (this, tr ("Save as"), path, filters);

    if (filename.isEmpty())
        return false;

    settings.setValue ("last_path", QFileInfo (filename).absolutePath());

    if (!filename.endsWith (".rpl"))
        filename += ".rpl";

    return saveReplayFile (filename);
}

void MainWindow::on_actionImport_triggered()
{
    static QString formats[][2] = {
        {tr ("Portable Network Graphics"), "png"},
        {tr ("Windows Bitmap"), "bmp"},
        {tr ("Joint Photographic Experts Group"), "jpg"},
        {tr ("Joint Photographic Experts Group"), "jpeg"},
        {tr ("Portable Pixmap"), "ppm"},
        {tr ("Tagged Image File Format"), "tiff"},
        {tr ("X11 Bitmap"), "xbm"},
        {tr ("X11 Pixmap"), "xpm"},
        {tr ("Graphic Interchange Format"), "gif"},
        {tr ("Multiple-image Network Graphics"), "mng"},
        {tr ("Portable Bitmap"), "pbm"},
        {tr ("Portable Graymap"), "pgm"},
        {tr ("Scalable Vector Graphics"), "svg"},
        {tr ("Targa Image Format"), "tga"}
    };

    QString filters = tr ("All image formats") + " (";

    for (uint i = 0; i < sizeof(formats)/sizeof(QString)/2; ++i)
        filters += "*." + formats[i][1] + ' ';
    filters += ");;";

    for (uint i = 0; i < sizeof(formats)/sizeof(QString)/2; ++i)
        filters += formats[i][0] + " (*." + formats[i][1] + ");;";
    filters += tr ("All files (*.*)");

    QSettings settings;
    QString path = settings.value ("last_image_path", ".").toString();

    QString filename = QFileDialog::getOpenFileName (this,
                            tr ("Import image"), path, filters);

    if (filename.isEmpty())
        return;

    settings.setValue ("last_image_path", QFileInfo (filename).absolutePath());

    if (isUntitled && !isWindowModified())
        loadFile (filename, Graphic);
    else {
        QProcess::startDetached (qApp->applicationFilePath(),
                                { filename });
    }

}

bool MainWindow::on_actionExport_triggered()
{
    static QString formats[][2] = {
        {tr ("Portable Network Graphics"), "png"},
        {tr ("Windows Bitmap"), "bmp"},
        {tr ("Joint Photographic Experts Group"), "jpg"},
        {tr ("Joint Photographic Experts Group"), "jpeg"},
        {tr ("Portable Pixmap"), "ppm"},
        {tr ("Tagged Image File Format"), "tiff"},
        {tr ("X11 Bitmap"), "xbm"},
        {tr ("X11 Pixmap"), "xpm"}
    };

    QString filters;

    for (uint i = 0; i < sizeof(formats)/sizeof(QString)/2; ++i)
        filters += formats[i][0] + " (*." + formats[i][1] + ");;";
    filters += tr ("All files (*.*)");

    QSettings settings;
    QString path = settings.value ("last_image_path", ".").toString();

    QString selectedFilter;
    QString filename =
        QFileDialog::getSaveFileName (this, tr ("Save image as"), path,
                filters, &selectedFilter);

    if (filename.isEmpty())
        return false;

    settings.setValue ("last_image_path", QFileInfo (filename).absolutePath());

    QString selectedFormat = QFileInfo (filename).suffix();

    if (selectedFilter == tr ("All files (*.*)")) {

        uint i = 0;
        for (; i != sizeof (formats)/sizeof(QString)/2; ++i)
            if (formats[i][1] == selectedFormat)
                break;
        if (i == sizeof (formats)/sizeof (QString)/2) {
            QMessageBox::critical (this, tr ("Error"), 
                            tr ("Invalid image format "));
            return false;
        }
    } else {
        if (!filename.endsWith (selectedFormat))
            filename += "." + selectedFormat;
    }

    return saveGraphicFile (filename);
}

void MainWindow::openRecentFile()
{
    QAction *action = qobject_cast<QAction *>(sender());

    if (action) {
        QString filePath = action->data().toString();

        if (filePath.isEmpty() || filePath == curFile)
            return;

        if (isUntitled && !isWindowModified())
            loadFile (filePath);
        else {
            QProcess::startDetached (qApp->applicationFilePath(),
                                     QStringList (filePath));
        }
    }
}

//! Ghi nội dung bức vẽ vào file ảnh
/*!
 * \param filename tên file ảnh sẽ ghi vào
 * \return ghi file thành công hay không
 * \sa saveReplayFile(), loadFile()
 */
bool MainWindow::saveGraphicFile (QString const &filename)
{
    QFile file (filename);

    if (!file.open (QFile::WriteOnly)) {
        QMessageBox::critical (this, qApp->applicationName(),
                               tr ("Cannot write file %1: \n%2.")
                               .arg (filename)
                               .arg (file.errorString()));
        return false;
    }
    file.close();

    QApplication::setOverrideCursor (Qt::WaitCursor);
    canvas->getBuffer()->save (filename);
    QApplication::restoreOverrideCursor();

    return true;
}

//! File hiện tại có thể lưu hay không ?
/*!
 * Khi người dùng đóng cửa sổ thì sẽ có hộp thoại yêu cầu lưu file (nếu file
 * chưa được lưu). Nếu người dùng bỏ qua (QMessageBox::Discard) hoặc chấp nhận
 * (QMessageBox::Save) thì trả về có. Nếu hủy hành động đóng cửa sổ
 * (QMessageBox::Cancel) thì sẽ trả về không.
 * \return file có thể lưu hay không
 * \sa closeEvent(), saveReplayFile()
 */
bool MainWindow::maybeSave()
{
    if (isWindowModified()) {
        int ret = QMessageBox::warning (this,
                  windowTitle(),
                  tr ("The document \"%1\"  has been modified.\n"
                      "Do you want to save your changes?").arg (curFile),
                  QMessageBox:: Save | QMessageBox::Discard | 
                  QMessageBox:: Cancel);

        if (ret == QMessageBox::Save)
            return on_actionSave_triggered();
        else if (ret == QMessageBox::Cancel)
            return false;
    }

    return true;
}

//! Tải nội dung từ file
/*!
 * File đầu vào có thể là file Replay hoặc là file ảnh
 * \param filename tên file
 * \param ft kiểu file
 * \sa saveGraphicFile(), saveReplayFile()
 */
void MainWindow::loadFile (QString const &filename, FileTypes ft)
{
    QFile file (filename);

    if (!file.open (QFile::ReadOnly)) {
        QMessageBox::critical (this, qApp->applicationName(),
                               tr ("Cannot read file %1:\n%2.")
                               .arg (filename)
                               .arg (file.errorString()));
        return;
    }

    bool loadSuccess = false;
    QApplication::setOverrideCursor (Qt::WaitCursor);
    if (ft & Graphic) {
        if (canvas->load (filename))
            loadSuccess = true;
    } else if (player->load (file)) {
        loadSuccess = true;
        canvas->reloadPlayer();
        QApplication::restoreOverrideCursor();
        reloadControl();

        if (!(ft & Sample))
            setCurrentFile (filename);
    }

    QApplication::restoreOverrideCursor();
    if (loadSuccess) {
        statusBar->showMessage (tr ("File loaded"), 2000);
    } else {
        QMessageBox::critical (this, qApp->applicationName(),
                            tr ("File %1 read error.").arg (filename));

        statusBar->showMessage (tr ("File loaded error"), 2000);
    }
}

//! Ghi danh sách các thao tác vẽ vào file Replay
/*!
 * \param filename tên file
 * \return ghi file thành công hay không
 * \sa saveGraphicFile(), loadFile()
 */
bool MainWindow::saveReplayFile (QString const &filename)
{
//    fileWatcher->blockSignals (true);

    QFile file (filename);

    file.open (QFile::WriteOnly);
    if (!file.isWritable()) {
        QMessageBox::critical (this, qApp->applicationName(),
                               tr ("Cannot write file %1: \n%2.")
                               .arg (filename)
                               .arg (file.errorString()));

//        fileWatcher->blockSignals (false);
        return false;
    }
//    file.close();

    QApplication::setOverrideCursor (Qt::WaitCursor);
    player->save (file);
    file.close();
    QApplication::restoreOverrideCursor();
//    fileWatcherBlockTimer->start();

    setCurrentFile (filename);
    statusBar->showMessage (tr ("File saved"), 2000);

    return true;
}

//! Thay đổi file hiện tại
/*!
 * Thay đổi file gắn với MainWindow. Nếu không có thì MainWindow này là
 * Untitle.
 * \param filename tên file
 * \sa isUntitled, curFile
 */
void MainWindow::setCurrentFile (QString const &filename)
{
    static int sequenceNumber = 1;

    //    qDebug() << fileWatcher->files();

    //    if(!isUntitled)
    //        fileWatcher->removePath(curFile);

    isUntitled = filename.isEmpty();
    if (isUntitled) {
        curFile = tr ("untitled%1.rpl").arg (sequenceNumber++);
    } else {
        curFile = QFileInfo (filename).canonicalFilePath();
        //        fileWatcher->addPath(curFile);

        //
        // update recent files

        QSettings settings;
        QStringList files = settings.value ("recentFileList").toStringList();

        files.removeAll (curFile);
        files.prepend (curFile);
        while (files.size() > MaxRecentFiles)
            files.removeLast();
        settings.setValue ("recentFileList", files);

        updateRecentFileActions();
    }

    setWindowModified (false);
    setWindowFilePath (curFile);

    //    qDebug() << fileWatcher->files();
}

//! Cập nhật danh sách các file gần nhất
/*!
 * \sa recentFileActions, MaxRecentFiles
 */
void MainWindow::updateRecentFileActions()
{
    QSettings settings;
    QStringList files = settings.value ("recentFileList").toStringList();

    int numRecentFiles = qMin (files.size(), (int) MaxRecentFiles);

    for (int i = 0; i != numRecentFiles; ++i) {
        QString text = tr ("&%1 %2").arg (i + 1).arg (strippedName (files[i]));

        recentFileActions[i]->setText (text);
        recentFileActions[i]->setData (files[i]);
        recentFileActions[i]->setVisible (true);
    }

    for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
        recentFileActions[j]->setVisible (false);
}

//! Lấy tên file (không kèm theo đường dẫn đến thư mục)
/*!
 * \param fullFileName tên file đầy đủ
 */
QString MainWindow::strippedName (QString const &fullFileName)
{
    return QFileInfo (fullFileName).fileName();
}

void MainWindow::on_actionReplay_triggered()
{
    actionPause->setVisible (true);
    actionReplay->setVisible (false);
    actionStop->setEnabled (true);
    enablePainting (false);
    player->play();
}

void MainWindow::on_actionPause_triggered()
{
    actionResume->setVisible (true);
    actionPause->setVisible (false);
    player->pause();
}

void MainWindow::on_actionResume_triggered()
{
    actionPause->setVisible (true);
    actionResume->setVisible (false);
    player->resume();
}

void MainWindow::on_actionStop_triggered()
{
    playerStopped();
    player->stop();
}

//! #player đã kết thúc Replay
/*!
 * Được gọi do player tự kết thúc Replay hay do người dùng yêu cầu kết thúc
 * \sa Player, Player::playerStopped()
 */
void MainWindow::playerStopped()
{
    actionPause->setVisible (false);
    actionResume->setVisible (false);
    actionReplay->setVisible (true);
    actionReplay->setChecked (false);
    actionStop->setEnabled (false);
    enablePainting (true);
}

//! #player đã thay đổi trạng thái chỉnh sửa hay không
/*!
 * \param b player có thay đổi (so với nội dung file đã được lưu) hay không
 * \sa Player::playerModified()
 */
void MainWindow::playerModified (bool b)
{
    if (isWindowModified() != b)
        setWindowModified (b);
}

void MainWindow::on_actionLine_triggered()
{
    canvas->setMode (Canvas::DrawLine);
}

void MainWindow::on_actionRectangle_triggered()
{
    canvas->setMode (Canvas::DrawRect);
}

void MainWindow::on_actionFreeDraw_triggered()
{
    canvas->setMode (Canvas::FreeDraw);
}

void MainWindow::on_actionEllipse_triggered()
{
    canvas->setMode (Canvas::DrawEllipse);
}

void MainWindow::on_actionPolygon_triggered()
{
    canvas->setMode (Canvas::DrawPolygon);
}

void MainWindow::on_actionCurve_triggered()
{
    canvas->setMode (Canvas::DrawCurve);
}

void MainWindow::on_actionRegularPolygon_triggered()
{
    canvas->setMode (Canvas::DrawRegularPolygon);
}

void MainWindow::on_actionStar_triggered()
{
    canvas->setMode (Canvas::DrawStar);
}

void MainWindow::on_actionFill_triggered()
{
    canvas->setMode (Canvas::FillArea);
}

void MainWindow::on_actionErase_triggered()
{
    canvas->setMode (Canvas::EraseArea);
}

void MainWindow::on_actionOriginal_size_triggered()
{
    zoomFactorSpinBox->setValue (100);
}

void MainWindow::on_actionZoom_2_1_triggered()
{
    zoomFactorSpinBox->setValue (200);
}

void MainWindow::on_actionZoom_1_2_triggered()
{
    zoomFactorSpinBox->setValue (50);
}

void MainWindow::on_actionZoom_In_triggered()
{
    zoomFactorSpinBox->setValue (zoomFactorSpinBox->value() * 1.2);
}

void MainWindow::on_actionZoom_Out_triggered()
{
    zoomFactorSpinBox->setValue (zoomFactorSpinBox->value() / 1.2);
}

//! Thay đổi màu nét vẽ
/*!
 * Đồng thời cập nhật icon màu của #setPenToolBar.
 * Được gọi khi người dùng chọn màu nét vẽ bằng #colorPicker hoặc bằng
 * setPenColor
 * \param color màu mới
 * \sa ColorPicker::penColorPicked(), openPenColorDialog()
 */
void MainWindow::setPenColor (QColor color)
{
    setPenToolBar->reloadPenColorIcon (color);

    QPen pen = canvas->getPen();

    pen.setColor (color);
    canvas->setPen (pen);
}

//! Thay đổi màu chổi lông
/*!
 * Đồng thời cập nhật icon màu của #setBrushToolBar.
 * Được gọi khi người dùng chọn chổi lông vẽ bằng #colorPicker hoặc bằng
 * setBrushColor
 * \param color màu mới
 * \sa ColorPicker::brushColorPicked(), openBrushColorDialog()
 */
void MainWindow::setBrushColor (QColor color)
{
    setBrushToolBar->reloadBrushColorIcon (color);

    QBrush brush = canvas->getBrush();

    brush.setColor (color);
    canvas->setBrush (brush);
}

//! Cập nhật thông tin về màu đang chọn trên #colorPicker
/*!
 * Hiện thông tin về màu tại điểm mà con trỏ chuột đang trỏ trên colorPicker
 * lên #statusBar.
 * \param colorName tên màu đang được chọn
 * \sa #colorPicker, ColorPicker::colorHovered()
 */
void MainWindow::hoveredColor (QString const& colorName) 
{
//    statusBar->clearMessage();
    statusBar->showMessage (tr ("Color ") + colorName +
            tr ("; Click to set pen color, Shift+Click to set brush color"));
}


//! Mở hộp thoại chọn màu nét vẽ và thay đổi theo màu mới chọn
/*!
 * Được gọi khi người dùng yêu cầu chọn màu nét vẽ bằng #setPenToolBar
 * \sa openBrushColorDialog(), SetPenToolBar::penColorNeeded()
 */
void MainWindow::openPenColorDialog()
{
    auto old_color = canvas->getPen().color();
    auto color = QColorDialog::getColor (old_color, this,
                 tr ("Select pen color"),
                 QColorDialog::ShowAlphaChannel);

    if (color.isValid())
        setPenColor (color);
}


//! Mở hộp thoại chọn màu chổi lông và thay đổi theo màu mới chọn
/*!
 * Được gọi khi người dùng yêu cầu chọn màu chổi lông bằng #setBrushToolBar
 * \sa openPenColorDialog(), SetBrushToolBar::brushColorNeeded()
 */
void MainWindow::openBrushColorDialog()
{
    auto old_color = canvas->getBrush().color();
    auto color = QColorDialog::getColor (old_color, this,
                 tr ("Select brush color"),
                 QColorDialog::ShowAlphaChannel);

    if (color.isValid())
        setBrushColor (color);
}

void MainWindow::on_actionUndo_triggered()
{
    canvas->undo();
    reloadControl();
}

void MainWindow::on_actionRedo_triggered()
{
    canvas->redo();
    reloadControl();
}

void MainWindow::on_actionResize_triggered()
{
    ResizeDialog dialog (player->size(), this);
    if (dialog.exec() == QDialog::Accepted) {
        QSize newSize = dialog.getNewSize();
        if (newSize != player->size()) {
            player->resizeBuffer (newSize);
            canvas->reloadPlayer();
            setWindowModified (true);
        }
    }
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog dialog (this);
    dialog.exec();
}

void MainWindow::on_actionManual_triggered()
{
    QString manualFilePath = qApp->applicationDirPath();

#ifdef Q_WS_WIN
    manualFilePath += "\\manual.pdf";
#else
    manualFilePath += "/../share/bkpaint/manual.pdf";
#endif

    QDesktopServices::openUrl (QUrl::fromLocalFile (manualFilePath));
}

//! Cập nhật vị trí mới của con trỏ chuột lên #labelPosition
/*!
 * \param pos vị trí mới
 * \sa labelPosition, Canvas::cursorMoved()
 */
void MainWindow::updateLabelPosition (QPointF pos)
{
    labelPosition->setText ("x: " + QString::number (pos.x(), 'f', 2)
                            + "\ny: " + QString::number (pos.y(), 'f', 2));
}

//! Cập nhật các thành phần điểu khiển
/*!
 * Khi nội dung bức vẽ thay đổi do tải từ file hoặc do undo, redo thì các thuộc
 * tính của bức vẽ như Pen, Brush, RenderHint cũng thay đổi. Kéo theo các thành
 * phần điểu khiển của MainWindow quy định các thuộc tính này cũng thay đổi.
 * Phương thức này có nhiệm vụ cập nhật các thay đổi đó.
 * \sa setPenToolBar, setBrushToolBar, renderHintMenu
 */
void MainWindow::reloadControl()
{
    // Pen
    QPen pen = canvas->getPen();

    setPenToolBar->blockSignals (true);
    setPenToolBar->setPenWidth (pen.width());

    setPenToolBar->reloadPenStyleIcon (pen.style());
    setPenToolBar->reloadPenColorIcon (pen.color());

    setPenToolBar->blockSignals (false);

    // Render hint

    auto hints = canvas->getRenderHints();

    actionAntialiasing->blockSignals (true);
    actionAntialiasing->setChecked (hints & QPainter::Antialiasing);
    actionAntialiasing->blockSignals (false);

    actionTextAntialiasing->blockSignals (true);
    actionTextAntialiasing->setChecked (hints & QPainter::TextAntialiasing);
    actionTextAntialiasing->blockSignals (false);

    actionSmoothPixmapTransform->blockSignals (true);
    actionSmoothPixmapTransform->setChecked (
                    hints & QPainter::SmoothPixmapTransform);
    actionSmoothPixmapTransform->blockSignals (false);

    actionHighQualityAntialiasing->blockSignals (true);
    actionHighQualityAntialiasing->setChecked (
                    hints & QPainter::HighQualityAntialiasing);
    actionHighQualityAntialiasing->blockSignals (false);

    actionNonCosmeticDefaultPen->blockSignals (true);
    actionNonCosmeticDefaultPen->setChecked (
                    hints & QPainter::NonCosmeticDefaultPen);
    actionNonCosmeticDefaultPen->blockSignals (false);

    // Brush
    QBrush brush = canvas->getBrush();

    setBrushToolBar->reloadBrushStyleIcon (brush.style());
    setBrushToolBar->reloadBrushColorIcon (brush.color());
}
