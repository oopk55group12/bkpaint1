/************************************************************************
        canvas.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef CANVAS_HH
#define CANVAS_HH

#include <QWidget>
#include <QPointF>
#include <QList>
#include <QTimer>
#include <QPen>
#include <QPainter>

class QImage;
class QPicture;
class PaintOperation;
class Player;


//! Bức vẽ
/*!
 * Lớp Canvas đại diện cho một bức vẽ, là đối tượng điều khiển chính của chương
 * trình. Người dùng vẽ bằng các thao tác trực tiếp lên Canvas hoặc gián tiếp
 * thông qua các điều khiển của MainWindow.
 *
 * Lớp Canvas có nhiều chế độ vẽ khác nhau, được quy định bởi thuộc tính #mode
 * kiểu DrawMode. Được cài đặt các sự kiện chuột (#mousePressEvent(),
 * #mouseMoveEvent(), #mouseReleaseEvent()), tùy vào chế độ vẽ hiện tại mà các
 * cách xử lý các sự kiện này khác nhau. Các chế độ vẽ này được người dùng chọn
 * lựa thông qua các điều khiển của MainWindow.
 *
 * Canvas không tự vẽ các hình đồ họa trực tiếp lên mình mà gián tiếp thông qua
 * #buffer. Mỗi khi có yêu cầu vẽ lại (nhận được sự kiện #paintEvent()) thì
 * phần cần vẽ được sao chép từ buffer. Nhờ đó, khi số lượng thao tác vẽ rất
 * lớn thì không cần phải thực hiện lại từ đầu tất cả các thao tác.
 *
 * Lớp Canvas có thuộc tính #player để quản lý việc lưu trữ cũng như thực hiện
 * lại các thao tác vẽ. Ở chế độ Replay thì phương thức xử lý sự kiện vẽ lại
 * (#paintEvent()) sử dụng buffer của player thay cho buffer của canvas.
 *
 */ 
class Canvas : public QWidget {
    Q_OBJECT

public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    Canvas (Player *player, QWidget *parent = 0);

    //! Phương thức hủy
    virtual ~Canvas();


    // Static Public attributes
    //

    //! Kiểu chế độ vẽ
    enum DrawMode {
        FreeDraw,           //!< Vẽ các đường tự do
        DrawLine,           //!< Vẽ đường thẳng
        DrawRect,           //!< Vẽ hình chữ nhật, hình vuồng
        DrawEllipse,        //!< Vẽ hình elip, hình tròn
        DrawPolygon,        //!< Vẽ đa giác
        DrawRegularPolygon, //!< Vẽ đa giác đều
        DrawStar,           //!< Vẽ ngôi sao
        DrawCurve,          //!< Vẽ đương cong Bezier
        EraseArea,          //!< Tẩy một vùng trên bức vẽ
        FillArea,           //!< Tô màu
        ZoomIn,             //!< Phóng to
        ZoomOut,            //!< Thu nhỏ
        Replay,             //!< Thực hiện lại các thao tác vẽ
        ModeCount           //!< Số chế độ vẽ
    };

    //! Con trỏ chuột tương ứng với các chế độ vẽ khác nhau
    static QCursor cursors[ModeCount];


    // Static Public methods
    //

    //! Khởi tạo các con trỏ chuột
    static void initCursors();

    //! Sinh con trỏ chuột cho chế đô tẩy (DrawMode::EraseArea)
    static QCursor genEraseCursor (qreal radius);

    //!
    static QPolygonF regularPolygon (int, QPointF const &, QPointF const &);

    //!
    static QPolygonF regularStar (int, QPointF const &, QPointF const &);


    // Public methods
    //

    //! Lưu nội dung bức vẽ vào file ảnh
    bool save (QString const &file);

    //! Tải nội dung file ảnh lên bức vẽ
    bool load (QString const &file);

    //! Cập nhật sự thay đổi của player
    void reloadPlayer();

    // Public accessor methods
    //

    //! Thay đổi chế độ vẽ
    void setMode (DrawMode m);

    //! Lấy buffer
    /*!
     * \return buffer của bức vẽ
     */
    QImage const *getBuffer() {
        return buffer;
    }

    //! Lấy pen 
    /*!
     * \return pen gắn với #painter hiện tại
     */
    QPen getPen() {
        return painter->pen();
    }

    //! Lấy renderHints 
    /*!
     * \return RenderHints gắn với #painter hiện tại
     */
    QPainter::RenderHints getRenderHints() {
        return painter->renderHints();
    }

    //! Lấy brush 
    /*!
     * \return Brush gắn với #painter hiện tại
     */
    QBrush getBrush() {
        return painter->brush();
    }

signals:
    //! Tín hiệu được phát ra khi chuôt di chuyển đển một điểm trên Canvas
    void cursorMoved (QPointF);

public slots:
    //! Thay đổi bút vẽ
    void setPen (QPen const &pen);

    //! Thay đổi renderHint
    void setRenderHint (QPainter::RenderHint hint, bool on);

    //! Thay đổi chổi lông
    void setBrush (QBrush const &brush);

    //! Thay đổi bán kính vùng tẩy
    void setEraseRadius (int radius);

    //! Thay đổi số đỉnh của đa giác đều hoặc ngôi sao
    void setNoCorner (int);

    //! Thay đổi tỉ lệ phóng to/thu nhỏ
    void setZoomFactor (int percent);

    //! Trả lại trạng thái trước khi thực hiện thao tác gần nhất
    void undo();

    //! Thực hiện lại thao tác vừa undo
    void redo();

protected:
    // Protected methods
    //

    //! Sự kiện vẽ lại
    void paintEvent (QPaintEvent *event) override;

    //! Sự kiện nhấn chuột
    void mousePressEvent (QMouseEvent *event) override;

    //! Sự kiện di chuyển chuột
    void mouseMoveEvent (QMouseEvent *event) override;

    //! Sự kiện nhả chuột
    void mouseReleaseEvent (QMouseEvent *event) override;

private:
    // Private methods
    //

    //! Thực hiện thao tác
    void operate (PaintOperation *po);

    //! Cập nhật con trỏ chuột
    void updateCursor();

    //! Vẽ background
    void fillBackground (QPainter &bgPainter, QRect const &rect);

    //! Khởi tạo buffer mới
    void regenerateBuffer();

private slots:
    //! Thực hiện thao tác vẽ tiếp theo ở chế độ Replay
    void playNextOp (QRectF const &rect);	

    //! Khởi động chế độ replay ở canvas
    void startPlayer();

    //! Kết thúc chế độ replay
    void stopPlayer();

private:
    // Private attributes
    //

    //! buffer
    QImage *buffer;

    //! Đối tượng vẽ gắn với buffer
    QPainter *painter;

    //! Đối tượng lưu trữ, thực hiện các thao tác vẽ
    Player *player;

    //! Chế độ vẽ hiện tại
    DrawMode mode;

    //! Chế độ vẽ trước đó
    DrawMode old_mode;

    //! Tỉ lệ phóng to/thu nhỏ
    qreal zoomFactor;

    //! Tỉ lệ phóng to/thu nhỏ trước đó
    qreal old_zoomFactor;

    //! Bán kính của vùng bị tẩy trong chế độ tẩy
    int eraseRadius;

    //! Số đỉnh của đa giác đều (hoặc số đỉnh nhọn của ngôi sao)
    int corner;

    //! Điểm bắt đầu (của một số hình đồ hoạ như đường tự do, đa giác).
    QPointF startPoint;
    
    //! Điểm trước đó
    QPointF lastPoint;

    //! control point thứ nhất của đường cong Bezier đang vẽ
    QPointF controlPoint1;
            
    //! control point thứ hai của đường cong Bezier đang vẽ
    QPointF controlPoint2;

    //! Đã bắt đầu vẽ hình
    bool drawStarted;

    //! Chuột đã được nhấn hay chưa. 
    /*!
     * Cho biết hiện tại chuột trái có được nhấn hay không. Được sử dụng khi vẽ
     * những hình phức tạp như đa giác, đường cong.
     */
    bool mousePressed;
};

#endif // CANVAS_HH
