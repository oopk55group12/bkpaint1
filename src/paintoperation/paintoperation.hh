
/************************************************************************
        paintoperation.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef PAINTOPERATION_HH
#define PAINTOPERATION_HH

#include <QPainter>
#include <QDataStream>
#include <QRectF>

//! Lớp cơ sở cho tất cả các thao tác vẽ.
/*!
 * Một thao tác vẽ là một hành động làm thay đổi thiết bị vẽ (QPaintDevice)
 * hoặc đối tượng vẽ (QPainter). Hành động này có thể là thêm một đối tượng đồ
 * hoạ (AddShape), một hình ảnh (AddImage) hay thay đổi nét vẽ (SetPen) ...
 * Các thao tác vẽ liên tiếp nhau tạo thành một bức vẽ hoàn chỉnh.
 *
 * Tất cả các lớp con của PaintOperation đều được cài đặt lại các hàm thuần ảo
 * #operate(), #load(), #save(), #type().
 *
 * Phương thức quan trọng nhất là operate (QPainter &), phương thức này nhận
 * đối số là một đối tượng vẽ (QPainter), và tuỳ theo loại thao tác mà tác động
 * lên đối tượng vẽ này theo một cách nào đó. VD: drawPath(), fillRect(), ...
 * Phương thức operate() trả về một giá trị kiểu QRectF để chỉ vùng hình chữ
 * nhật trên thiết bị vẽ (QPaintDevice) gắn với đối tương vẽ đã bị thay đối
 * (nếu có) bởi phương thức.
 * Việc replay một bức vẽ tương đương với việc gọi phương thức operate() với
 * các thao tác liên tục với một chu kỳ nào đó.
 *
 * Các thao tác có thể được lưu vào file bằng phương thức save(), đọc từ file
 * bằng phương thức load(). Để biết kiểu của một thao tác (khi gọi từ con trỏ
 * PaintOperation*), ta sử dụng phương thức type().
 */
class PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức hủy ảo
    virtual ~PaintOperation();

    
    // Public methods
    //

    //! Kiểu của thao tác vẽ
    /*!
     * Phương thức thuần ảo, được cài đặt ở các lớp con.
     * Các đối tượng thuộc cùng một lớp con luôn trả về cùng một giá trị. Dùng
     * để phân biệt loại thao tác khi tất cả đều được lưu dưới dạng con trỏ
     * PaintOperation.
     */
    virtual qint32 type() const = 0;

    //! Thực hiện thao tác
    /*!
     * Phương thức thuần ảo, được cài đặt ở các lớp con.
     * \param painter đối tượng vẽ
     * \return Vùng sẽ bị thay đổi sau khi thực hiện
     */
    virtual QRectF operate (QPainter &painter) const = 0;

    //! Lưu trạng thái của thao tác vẽ vào stream
    /*!
     * Phương thức thuần ảo, được cài đặt ở các lớp con.
     * Vì việc đọc dữ liệu được thực hiện bởi con trỏ lớp cha (PaintOperation*)
     * nên trong cài đặt của phương thức này ở lớp con phải ghi kiểu (#type())
     * của thao tác đầu tiên.
     * \param stream luồng dữ liệu ra
     * \sa load (QDataStream &)
     */
    virtual void save (QDataStream &stream) const = 0;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    /*!
     * \param stream luồng dữ liệu vào
     * \sa save (QDataStream &)
     */
    virtual void load (QDataStream &stream) = 0;
};

#endif // PAINTOPERATION_HH
