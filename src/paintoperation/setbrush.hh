/************************************************************************
        setbrush.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef SETBRUSH_HH
#define SETBRUSH_HH
#include "paintoperation.hh"

#include <QBrush>

//! Lớp đại diện cho thao tác thay đối bút lông.
/**
 * Tất cả các thao tác vẽ (PaintOperation) đều chia sẽ cùng một đối tượng vẽ
 * QPainter. Trong lớp QPainter có thuộc tính kiểu QBrush (QPainter::brush()) quy định bút lông,
 * bao gồm kiểu bút lông (Solid pattern, đường dọc, đường ngang ...) và màu 
 * bút lông. Các hình chữ nhật, hình elip khi vẽ sẽ có phần phía trong quy định
 * bởi thuộc tính này.
 *
 * Lớp SetBrush có thuộc tính #brush, khi thực hiện thì sẽ thay đối thuộc tính
 * tương ứng của đối tượng vẽ. Do đó các thao tác vẽ sau này đều sẽ tạo ra các
 * hình vẽ được quy định bởi brush.
 *
 * \sa QPainter, QBrush, QPainter::brush()
 */
class SetBrush : public PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    SetBrush (QBrush const &brush = QBrush());


    // Static Public attributes
    //

    //! Kiểu của thao tác
    static const qint32 Type = 4;


    // Public methods
    //

    //! Kiểu của thao tác
    /**
     * Thực chất trả về #Type.
     * \return qint32 kiểu của thao tác
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }

    //! Thực hiện thao tác
    QRectF operate (QPainter &painter) const override;

    //! Lưu trạng thái của thao tác vẽ vào stream
    void save (QDataStream &stream) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &stream) override;

private:
    // Private attributes
    //

    //! Bút lông mới
    QBrush brush;
};

#endif // SETBRUSH_HH
