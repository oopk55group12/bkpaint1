
/************************************************************************
        fill.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "fill.hh"
#include <QImage>
#include <QDebug>
#include <stack>

// Constructors/Destructors
//

//! Phương thức khởi tạo
/**
 * Khởi tạo tất cả các thuộc tính của lớp Fill
 * \sa seedPoint
 */
Fill::Fill (QPoint seedPoint /* = QPoint() */) : seedPoint (seedPoint)
{
}


//
// Methods
//

//! Thực hiện thao tác 
/**
 * Tô màu các điểm cùng màu với màu tại #seedPoint thành màu của painter.brush
 * \param painter đối tượng vẽ sẽ thao tác
 * \return Vùng bức vẽ sẽ bị thay đổi sau khi thực hiện
 */
QRectF Fill::operate (QPainter &painter) const
{
    QImage *image = static_cast<QImage *>(painter.device());
    int width = image->width();
    int height = image->height();
    auto zoomFactor = painter.transform().m11();

    if (seedPoint.x() < 0 || seedPoint.y() < 0 ||
        seedPoint.x() * zoomFactor >= width ||
        seedPoint.y() * zoomFactor >= height)
        return QRectF (0, 0, 0, 0);

    auto fillColor = painter.brush().color().rgba();
    auto targetColor = image->pixel (seedPoint * zoomFactor);

    //
    // alpha blending
    auto srcA = QColor (fillColor).alphaF();
    auto srcRGB = QColor (fillColor).rgb();
    auto dstA = QColor (targetColor).alphaF();
    auto dstRGB = QColor (targetColor).rgb();

    auto outA = srcA + dstA * (1 - srcA);
    auto outRGB = srcRGB;

    if (outA != 0)
        outRGB = (srcRGB * srcA + dstRGB * dstA * (1 - srcA)) / outA;
    else
        outRGB = 0;
    QColor outColor;

    outColor.setRgb (outRGB);
    outColor.setAlphaF (outA);

    if (outColor == targetColor) return QRectF (0,0,0,0);

    //
    // AreaFilling 2

//    QImage clone = image->copy();
    std::deque<QPoint> P { seedPoint * zoomFactor };
    bool uc, dc;
    QPoint p1, p2, p3, p4, up, dp;
    
    while (!P.empty()) {
        std::deque<QPoint> PT;
        for (auto p : P) {
            int x, y = p.y();

            if (y > 0) {

                // Pha tiến
                for (x = p.x() + 1, uc = false; x < width
                            && image->pixel (x, y) == targetColor; ++x)
                {
                    p1 = { x    , y - 1 };
                    p2 = { x - 1, y - 1 };
                    if (image->pixel (p1) == targetColor) {
                        uc = true;
                        up = p1;
                    } else if (image->pixel (p2) == targetColor)
                        PT.push_back (p2);
                }
                if (uc) PT.push_back (up);

                // Pha lùi
                for (x = p.x() - 1, uc = false; x > -1 
                            && image->pixel (x, y) == targetColor; --x)
                {
                    p1 = { x    , y - 1 };
                    p2 = { x + 1, y - 1 };
                    if (image->pixel (p1) == targetColor) {
                        uc = true;
                        up = p1;
                    } else if (image->pixel (p2) == targetColor)
                        PT.push_back (p2);
                }
                if (uc) PT.push_back (up);

            }

            if (y < height - 1) {

                // Pha tiến
                for (x = p.x() + 1, dc = false; x < width 
                            && image->pixel (x, y) == targetColor; ++x)
                {
                    p3 = { x    , y + 1 };
                    p4 = { x - 1, y + 1 };
                    if (image->pixel (p3) == targetColor) {
                        dc = true;
                        dp = p3;
                    } else if (image->pixel (p4) == targetColor)
                        PT.push_back (p4);
                }
                if (dc) PT.push_back (dp);

                // Pha lùi
                for (x = p.x() - 1, dc = false; x > -1 
                            && image->pixel (x, y) == targetColor; --x)
                {
                    p3 = { x    , y + 1 };
                    p4 = { x + 1, y + 1 };
                    if (image->pixel (p3) == targetColor) {
                        dc = true;
                        dp = p3;
                    } else if (image->pixel (p4) == targetColor)
                        PT.push_back (p4);
                }
                if (dc) PT.push_back (dp);
            }

            x = p.x();
            while (x < width && image->pixel (x, y) == targetColor)
                image->setPixel (x++, y, outColor.rgba());
            x = p.x() - 1;
            while (x > -1 && image->pixel (x, y) == targetColor)
                image->setPixel (x--, y, outColor.rgba());
        }

        P.swap (PT);
    }
    
    return QRectF (0, 0, width, height);
}

//! Đọc dữ liệu cho thao tác vẽ từ stream
/**
 * Đọc #seedPoint từ stream,
 * #Type sẽ được đọc trước đó để xác định kiểu của thao tác vẽ.
 * \param stream luồng dữ liệu vào
 * \sa save (QDataStream &)
 */
void Fill::load (QDataStream &stream)
{
    stream >> seedPoint;
}

//! Lưu trạng thái của thao tác vẽ vào stream
/**
 * Ghi #Type, #seedPoint vào stream.
 * \param stream luồng dữ liệu ra
 * \sa load (QDataStream &)
 */
void Fill::save (QDataStream &stream) const
{
    stream << Type << seedPoint;
}

