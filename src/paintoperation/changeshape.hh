/************************************************************************
        changeshape.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef CHANGESHAPE_HH
#define CHANGESHAPE_HH
#include "paintoperation.hh"
#include "addshape.hh"

#include <QThreadStorage>
#include <QSharedPointer>

#include <QPainterPath>
class QImage;

//! Lớp đại diện cho thao tác thay đổi một đối tượng đồ họa đã vẽ
/**
 * Khi người dùng vẽ một đường thẳng, hình chữ nhật, hình elip thì hình dạng và
 * kích thước liên tục thay đối theo đường kéo chuột. Trước khi thay đổi cần
 * phải trả lại trạng thái cũ rồi mới vẽ hình mới lên.
 *
 * Thành phần tĩnh #changedPart kiểu QImage* là con trỏ trỏ đến một phần đã bị 
 * thay đối trước khi thực hiện việc thao tác ChangeShape trước đó, 
 * #lastTopLeft là điểm góc trên bên trái của phần bị thay đổi. Thực hiện thao 
 * tác ChangeShape tức là vẽ lại changedPart, chụp lại phần sẽ bị thay đối (để 
 * dành cho thao tác ChangeShape tiếp theo) khi vẽ #shape rồi mới vẽ shape.
 *
 * Do các thao tác được thực hiện liên tục theo thứ tự trước sau nên các thao
 * tác ChangeShape thuộc cùng một Player có thể chia sẻ cùng nhau changedPart và
 * lastTopLeft để tiết kiệm bộ nhớ. Ngoài ra, do mỗi Player được hoạt động trên
 * một thread riêng biệt nên tốt nhất changedPart và lastTopLeft là thành phần
 * static thread local storage của lớp ChangeShape. Thao tác này thực hiện sẽ 
 * nhận changedPart và lastTopLeft của thao tác trước đó đồng thời thay đổi 
 * changedPart và lastTopLeft cho thao tác sau. Các con trỏ changedPart, 
 * lastTopLeft có thể được thay đổi bởi đối tượng Player bằng các phương thức 
 * #setChangedPart() và #setLastTopLeft().
 *
 * Sau khi người dùng thả chuột thì đối tượng đã được vẽ xong, chuỗi
 * ChangeShape cũng kết thúc. Dó đó các con trỏ changedPart và lastTopLeft vốn
 * được chia sẻ cho tất cả các đối tượng ChangeShape được sử đổi bởi thao tác
 * ChangeShape cuối cùng của chuối vẫn sẽ được lưu lại. Chuỗi ChangeShape sau
 * đó nhận các giá trị này sẽ có thể thực hiện sai. Vì vật cần một thao tác đặc
 * biệt là MousePress được thực hiện khi người dùng bắt đầu nhấn chuột bắt đầu
 * một chuỗi thay đổi, thao tác này có tác dụng "reset" lại changedPart và
 * lastTopLeft.
 *
 * \sa MousePress, Player, AddShape, changedPart, lastTopLeft, shape
 */
class ChangeShape : public PaintOperation {

public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    ChangeShape (QPainterPath const &shape = QPainterPath());


    // Static Public attributes
    //

    //! Kiểu của thao tác
    static const qint32 Type = 2;


    // Public methods
    //

    //! Kiểu của thao tác vẽ
    /**
     * Thực chất trả về Type
     * \return qint32 kiểu của thao tác
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }

    //! Thực hiện thao tác
    QRectF operate (QPainter &painter) const override;

    //! Lưu trạng thái của thao tác vẽ vào stream
    void save (QDataStream &stream) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &stream) override;

private:
    // Typedefs
    //

    //! typedef cho QThreadStorage<QSharedPointer<QImage>>
    typedef QThreadStorage<QSharedPointer<QImage>> TPImage;

    //! typedef cho QThreadStorage<QSharedPointer<QPointF>>
    typedef QThreadStorage<QSharedPointer<QPointF>> TPPointF;

private:
    // Private attributes
    //

    //! Đối tượng đồ họa mới sẽ vẽ
    QPainterPath shape;

    //! Một phần của bức vẽ đã bị thay đổi bởi thao tác vẽ trước đó.     
    /**
     * Được chia sẻ cho tất cả các đối tượng kiểu ChangeShape. Mỗi thread
     * (tương ứng với 1 Player) sẽ sở hữu một changedPart riêng biệt.
     */ 
    static TPImage changedPart;

    //! Điểm góc trên bên trái của phần bị thay đổi.
    /**
     * Được chia sẻ cho tất cả các đối tượng kiểu ChangeShape. Mỗi thread
     * (tương ứng với 1 Player) sẽ sở hữu một lastTopLeft riêng biệt.
     */ 
    static TPPointF lastTopLeft;

public:
    // Private attribute mutator methods
    //

    //! Thay đối giá trị của #changedPart.
    /**
     * \param pointer giá trị mới
     * \sa changedPart
     */
    static void setChangedPart (QSharedPointer<QImage> const &pointer) {
        changedPart.setLocalData (pointer);
    }

    //! Thay đổi giá trị của #lastTopLeft.
    /**
     * \param pointer giá trị mới
     * \sa lastTopLeft
     */
    static void setLastTopLeft (QSharedPointer<QPointF> const &pointer) {
        lastTopLeft.setLocalData (pointer);
    }

    //! Trả về đối tượng AddShape tương ứng.
    /**
     * Đối tượng AddShape này có shape giống với #shape của nó. Thực hiện thao
     * tác tương đương với việc thực hiện ChangeShape nhưng không cần trả về
     * trạng thái cũ, lưu lại phần sẽ thay đổi của thiết bị vẽ.
     * \sa AddShape, shape
     */
    AddShape *getAddShape() {
        return new AddShape (shape);
    }
};

#endif // CHANGESHAPE_HH
