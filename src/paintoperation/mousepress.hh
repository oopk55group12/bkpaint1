/************************************************************************
        mousepress.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef MOUSEPRESS_HH
#define MOUSEPRESS_HH

#include "paintoperation.hh"
#include <QPointF>
#include <QThreadStorage>
#include <QSharedPointer>

class QImage;

//! Lớp đại diện cho thao tác nhấn chuột, bắt đầu cho một chuỗi ChangeShape hoặc AddShape
/** 
 * Với một chuỗi ChangeShape, khi bắt đầu thao tác đầu tiên của chuỗi (tương
 * ứng với việc người dùng nhấn chuột) thì các thành viên tĩnh
 * \link ChangeShape::changedPart \endlink và \link ChangeShape::lastTopLeft 
 * \endlink (vốn được chia sẽ cho tất cả các đối tượng ChangeShape thuộc cùng
 * một Player) giữ giá trị của thao tác ChangeShape cuối cùng của chuỗi trước 
 * đó. Thực hiện thao tác ChangeShape đầu tiên của chuỗi mới sẽ vẽ lại 
 * changedPart này lên thiết bị vẽ, có thể làm sai kết quả. Thao tác MousePress 
 * thực hiện trước mỗi chuỗi ChangeShape có nhiệm vụ "làm sạch" các giá trị
 * này (cụ thể là changedPart sẽ được đưa về một QImage rỗng).
 *
 * Ngoài ra, một thao tác MousePress sẽ giúp nhận biết được tiếp theo sẽ là một
 * chuỗi các thao tác ChangeShape hoặc AddShape mới. Điều này có tác dụng trong
 * việc phân nhóm các thao tác thành 1 "thao tác có ích" (\link OpPair::effOp 
 * \endlink)
 *
 * \sa ChangeShape, AddShape, Player
 **/
class MousePress : public PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    MousePress();


    // Static Public attributes
    //

    //! Kiểu của thao tác
    static const qint32 Type = 8;


    // Public methods
    //

    //! Kiểu của thao tác vẽ
    /**
     * Thực chất trả về Type
     * \return qint32 kiểu của thao tác
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }

    //! Thực hiện thao tác
    QRectF operate (QPainter &) const override;

    //! Lưu trạng thái của thao tác vẽ vào stream
    void save (QDataStream &) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &) override;

private:
    // Typedefs
    //

    //! typedef cho QThreadStorage<QSharedPointer<QImage>>
    typedef QThreadStorage<QSharedPointer<QImage>> TPImage;

    //! typedef cho QThreadStorage<QSharedPointer<QPointF>>
    typedef QThreadStorage<QSharedPointer<QPointF>> TPPointF;

private:
    // Private attributes
    //

    //! Một phần của bức vẽ đã bị thay đổi bởi thao tác vẽ trước đó.     
    /**
     * Được chia sẻ cho tất cả các đối tượng kiểu ChangeShape và MousePress 
     * thuộc cùng một Player.
     * \sa ChangeShape::changedPart, Player::changedPart
     */ 
    static TPImage changedPart;

    //! Điểm góc trên bên trái của phần bị thay đổi.
    /**
     * Được chia sẻ cho tất cả các đối tượng kiểu ChangeShape và MousePress 
     * thuộc cùng một Player.
     * \sa ChangeShape::lastTopLeft, Player::lastTopLeft
     */ 
    static TPPointF lastTopLeft;

public:
    // Private attribute mutator methods
    //
    
    //! Thay đối giá trị của #changedPart.
    /**
     * \param pointer giá trị mới
     * \sa changedPart
     */
    static void setChangedPart (QSharedPointer<QImage> const &pointer) {
        changedPart.setLocalData (pointer);
    }

    //! Thay đổi giá trị của #lastTopLeft.
    /**
     * \param pointer giá trị mới
     * \sa lastTopLeft
     */
    static void setLastTopLeft (QSharedPointer<QPointF> const &pointer) {
        lastTopLeft.setLocalData (pointer);
    }

};

#endif // MOUSEPRESS_HH
