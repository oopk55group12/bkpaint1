
/************************************************************************
        addshape.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "addshape.hh"

// Constructors/Destructors
//

//! Phương thức khởi tạo
/**
 * Khởi tạo thuộc tính #shape
 * \sa shape
 */
    AddShape::AddShape (QPainterPath const &shape /* = QPainterPath() */)
    : shape (shape)
{
}


//
// Methods
//


//! Thực hiện thao tác
/**
 * Vẽ đối tượng QPainterPath #shape vào thiểt bị vẽ (QPaintDevice) gắn với
 * painter
 * \param painter đối tượng vẽ sẽ thao tác
 * \return Vùng bức vẽ sẽ bị thay đổi sau khi thực hiện
 */
QRectF AddShape::operate (QPainter &painter) const 
{
    painter.drawPath (shape);
    
    qreal extra = 2.5f * (1 + painter.pen().width());
    return shape.controlPointRect().adjusted (-extra, -extra, extra, extra);
}

//! Đọc dữ liệu cho thao tác vẽ từ stream
/**
 * Nhập #shape từ stream, #Type sẽ được đọc trước đó để xác định kiểu của
 * thao tác vẽ
 * \param stream luồng dữ liệu ra
 * \sa save (QDataStream &)
 */
void AddShape::load (QDataStream &stream)
{
    stream >> shape;
}

//! Lưu thao tác vẽ vào stream
/**
 * Ghi Type, shape vào stream.
 * \param stream luồng dữ liệu vào
 * \sa load (QDataStream &)
 */
void AddShape::save (QDataStream &stream) const 
{
    stream << Type << shape;
}

//! Hợp với một thao tác AddShape khác
/**
 * #shape sẽ lưu thêm đối tượng đồ hoạ được lưu bởi as.shape. Thực hiện
 * thao tác này sẽ tương đương với thực hiện hai thao tác cũ.
 * \param as đối tượng AddShape sẽ được ghép với đối tượng hiện tại
 * \sa shape
 */
void AddShape::append (AddShape const &as)
{
    shape.addPath (as.shape);
}
