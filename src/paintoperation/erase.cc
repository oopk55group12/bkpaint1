
/************************************************************************
        erase.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "erase.hh"

// Constructors/Destructors
//

//! Phương thức khởi tạo
/**
 * Khởi tạo tất cả các thuộc tính của lớp Erase
 * \sa centre, radius
 */
Erase::Erase (QPointF centre /* = QPointF */ , qint16 radius /* = 0 */)
    : centre (centre), radius (radius)
{
}

//
// Methods
//

//! Thực hiện thao tác
/*
 * Xoá phần hình tròn có tâm #centre, bán kính #radius trên bức vẽ.
 * \param painter đối tượng vẽ sẽ thao tác
 * \return Vùng bức vẽ sẽ bị thay đổi sau khi thực hiện
 */
QRectF Erase::operate (QPainter &painter) const
{
    QPen oldpen = painter.pen();
    QBrush oldbrush = painter.brush();

    painter.setPen (Qt::NoPen);
    painter.setBrush (painter.background());

    painter.drawEllipse (centre, radius, radius);

    painter.setPen (oldpen);
    painter.setBrush (oldbrush);

    return QRectF (centre.x()-radius-2, centre.y()-radius-2, 
                   radius * 2 + 2, radius * 2 + 2);
}

//! Đọc dữ liệu cho thao tác vẽ từ stream
/**
 * Đọc #centre, #radius từ stream,
 * #Type sẽ được đọc trước đó để xác định kiểu của thao tác vẽ.
 * \param stream luồng dữ liệu vào
 * \sa save (QDataStream &)
 */
void Erase::load (QDataStream &stream)
{
    stream >> centre >> radius;
}

//! Lưu trạng thái của thao tác vẽ vào stream
/**
 * Ghi #Type, #centre, radius vào stream.
 * \param stream luồng dữ liệu ra
 * \sa load (QDataStream &)
 */
void Erase::save (QDataStream &stream) const
{
    stream << Type << centre << radius;
}

