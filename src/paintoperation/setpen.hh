/************************************************************************
        setpen.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef SETPEN_HH
#define SETPEN_HH
#include "paintoperation.hh"

#include <QPen>

//! Lớp đại diện cho thao tác thay đối nét vẽ
/**
 * Tất cả các thao tác vẽ (PaintOperation) đều chia sẽ cùng một đối tượng vẽ
 * QPainter. Trong lớp QPainter có thuộc tính kiểu QPen (QPainter::pen()) quy định nét vẽ,
 * bao gồm kiểu nét vẽ (liền nét, chấm, chấm - gạch, ...), độ rộng và màu nét 
 * vẽ.
 *
 * Lớp SetPen có thuộc tính #pen, khi thực hiện thì sẽ thay đối thuộc tính
 * tương ứng của đối tượng vẽ. Do đó các thao tác vẽ sau này đều sẽ tạo ra các
 * nét vẽ được quy định bởi pen.
 *
 * \sa QPainter, QPen, QPainter::pen()
 */
class SetPen : public PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    SetPen (QPen const &pen = QPen());


    // Static Public attributes
    //

    //! Kiểu của thao tác
    static const qint32 Type = 3;

    // Public methods
    //

    //! Kiểu của thao tác
    /**
     * Thực chất trả về #Type.
     * \return qint32 kiểu của thao tác
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }

    //! Thực hiện thao tác
    QRectF operate (QPainter &painter) const override;

    //! Lưu trạng thái của thao tác vẽ vào stream
    void save (QDataStream &stream) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &stream) override;

private:
    // Private attributes
    //

    //! Bút vẽ mới
    QPen pen;
};

#endif // SETPEN_HH
