/************************************************************************
        addshape.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef ADDSHAPE_HH
#define ADDSHAPE_HH
#include "paintoperation.hh"

#include <QPainterPath>

//! Lớp đại diện cho thao tác thêm một đối tượng đồ họa vào Canvas.
/*!
 * Đối tượng đồ họa có thể là một đường thẳng, hình chữ nhật, hình elip hay tổ
 * hợp của các đối tượng cơ bản đó. Thuộc tính #shape kiểu QPainterPath sẽ lưu
 * tất cả các loại đối tượng này.
 *
 * Có thể gộp hai thao tác AddShape thành một thao tác bằng phương thức
 * #append (AddShape const &).
 *
 * \sa QPainterPath
 */
class AddShape : public PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    AddShape (QPainterPath const &shape = QPainterPath());


    // Static Public attributes
    //

    //! Kiểu của thao tác
    static const qint32 Type = 1;
    
    // Public methods
    //

    //! Kiểu của thao tác
    /**
     * Thực chất trả về Type
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }

    //! Thực hiện thao tác
    QRectF operate (QPainter &painter) const override;

    //! Lưu thao tác vẽ vào stream
    void save (QDataStream &stream) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &stream) override;

    //! Hợp với một thao tác AddShape khác
    void append (AddShape const &as);

private:

    // Private attributes
    //

    //! Đối tượng đồ họa cần thêm vào
    QPainterPath shape;
};

#endif // ADDSHAPE_HH
