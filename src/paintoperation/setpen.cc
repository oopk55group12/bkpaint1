
/************************************************************************
        setpen.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "setpen.hh"

// Constructors/Destructors
//

//! Phương thức khởi tạo
SetPen::SetPen (QPen const &pen /* = QPen */) :
    pen (pen)
{
}

//
// Methods
//

//! Thực hiện thao tác
/**
 * Thay bút vẽ cũ của painter bằng #pen
 * \param painter đối tượng vẽ sẽ thao tác
 * \return Vùng bức vẽ sẽ bị thay đổi sau khi thực hiện
 */
QRectF SetPen::operate (QPainter &painter) const
{
    painter.setPen (pen); 
    return QRectF (0,0,0,0);
}

//! Lưu trạng thái của thao tác vẽ vào stream
/**
 * Ghi #Type, #pen vào stream.
 * \param stream luồng dữ liệu ra
 * \sa load (QDataStream &)
 */
void SetPen::save (QDataStream &stream) const
{
    stream << Type << pen;
}

//! Đọc dữ liệu cho thao tác vẽ từ stream
/**
 * Đọc #pen từ stream. #Type sẽ được đọc trước đó để xác định kiểu của thao tác
 * vẽ.
 * \param stream luồng dữ liệu vào
 * \sa save (QDataStream &)
 */
void SetPen::load (QDataStream &stream)
{
    stream >> pen;
}

