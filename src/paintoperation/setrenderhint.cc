
/************************************************************************
        setrenderhint.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "setrenderhint.hh"

// Constructors/Destructors
//

//! Phương thức khởi tạo
SetRenderHint::SetRenderHint (QPainter::RenderHint hint, bool on)
    : hint (hint), on (on)
{
}

//
// Methods
//

//! Thực hiện thao tác
/**
 * Bật hoặc tắt #hint
 * \param painter đối tượng vẽ sẽ thao tác
 * \return Vùng bức vẽ sẽ bị thay đổi sau khi thực hiện
 */
QRectF SetRenderHint::operate (QPainter &painter) const
{
    painter.setRenderHint (hint, on); 
    return QRectF (0,0,0,0);
}

//! Lưu trạng thái của thao tác vẽ vào stream
/**
 * Ghi #Type, #hint, #on vào stream.
 * \param stream luồng dữ liệu vào
 * \sa load (QDataStream &)
 */
void SetRenderHint::save (QDataStream &stream) const
{
    stream << Type << static_cast<qint32> (hint) << on;
}

//! Đọc dữ liệu cho thao tác vẽ từ stream
/**
 * Đọc #hint, #on từ stream. #Type sẽ được đọc trước đó để xác định kiểu của 
 * thao tác vẽ.
 * \param stream luồng dữ liệu ra
 * \sa save (QDataStream &)
 */
void SetRenderHint::load (QDataStream &stream)
{
    qint32 i;
    stream >> i >> on;
    hint = static_cast<QPainter::RenderHint> (i);
}

