
/************************************************************************
        fill.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef FLOODFILL_HH
#define FLOODFILL_HH
#include "paintoperation.hh"

#include <QPoint>


//! Lớp đại diện cho thao tác tô màu.
/*!
 *  Vùng được tô màu được bắt đầu bởi điểm #seedPoint sau đó lan ra các điểm 
 *  xung quanh có màu trùng với màu gốc tại #seedPoint. Màu được tô là màu của 
 *  bút lông.
 *
 *  \sa SetBrush.
 */
class Fill : public PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    Fill (QPoint seedPoint = QPoint());


    // Static Public attributes
    //

    //! Kiểu của thao tác vẽ
    static const qint32 Type = 6;


    // Public attributes
    //

    //! Kiểu của thao tác vẽ
    /**
     * Thực chất trả về Type
     * \return qint32 kiểu của thao tác
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }


    // Public methods
    //

    //! Thực hiện thao tác
    QRectF operate (QPainter &painter) const override;

    //! Lưu trạng thái của thao tác vẽ vào stream
    void save (QDataStream &stream) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &stream) override;

private:
    // Private attributes
    //

    //! Điểm bắt đầu được tô màu
    QPoint seedPoint;
};

#endif // FLOODFILL_HH
