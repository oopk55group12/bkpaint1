/************************************************************************
        addimage.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef ADDIMAGE_HH
#define ADDIMAGE_HH
#include "paintoperation.hh"

#include <QImage>
#include <QPointF>


//! Lớp đại diện cho thao tác thêm một hình vào Canvas
/*!
 * \sa QImage
 */
class AddImage : public PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    AddImage (QImage const &image = QImage(), QPointF topLeft = QPointF());

    // Static Public attributes
    //

    //! Kiểu của thao tác
    static const qint32 Type = 7;

    // Public methods
    //

    //! Kiểu của thao tác
    /**
     * Thực chất trả về #Type.
     * Thường được gọi từ một con trỏ kiểu PaintOperation
     * \return qint32 kiểu của thao tác
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }


    //! Thực hiện thao tác 
    QRectF operate (QPainter &painter) const override;

    //! Lưu trạng thái của thao tác vẽ vào stream
    void save (QDataStream &stream) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &stream) override;

private:

    // Private attributes
    //

    //! Hình sẽ thêm vào
    QImage image;

    //! Vị trí của hình sẽ thêm vào trên Canvas
    QPointF topLeft;
};

#endif // ADDIMAGE_HH
