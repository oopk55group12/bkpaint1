
/************************************************************************
        changeshape.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "changeshape.hh"

#include <QImage>
#include <QDebug>

// Static members
//

ChangeShape::TPImage ChangeShape::changedPart;
ChangeShape::TPPointF ChangeShape::lastTopLeft;

// Constructors/Destructors
//

//! Phương thức khởi tạo
/**
 * Khởi tạo tất cả các thuộc tính của lớp ChangeShape
 * \sa shape,
 */
ChangeShape::ChangeShape (QPainterPath const &shape /* = QPainterPath()  */)
        : shape (shape)
{
}

//
// Methods
//

//! Thực hiện thao tác 
/**
 * \fn ChangeShape::operate (QPainter &painter)
 * Trả lại trạng thái cũ cho thiết bị vẽ bằng cách vẽ lại #changedPart tại
 * #lastTopLeft, lưu lại phần sẽ bị thay đổi và vẽ #shape
 * \param painter đối tượng vẽ sẽ thao tác
 * \return Vùng bức vẽ sẽ bị thay đổi sau khi thực hiện
 */
QRectF ChangeShape::operate (QPainter &painter) const 
{

    //
    // Trả lại trạng thái cũ của bức vẽ

    // Lưu lại trạng thái của painter
    qreal zoomFactor = painter.transform().m11();
    auto pen = painter.pen();
    auto background = painter.background().color();
    auto compositionMode = painter.compositionMode();

    painter.scale (1.0/zoomFactor, 1.0/zoomFactor);

    if (background == Qt::transparent)
        painter.setCompositionMode (QPainter::CompositionMode_Source);

    painter.drawImage (*(lastTopLeft.localData()),
                       *(changedPart.localData()));

    QRectF oldChangeRect (*(lastTopLeft.localData()) / zoomFactor,
                          (changedPart.localData())->size() / zoomFactor);

    painter.setCompositionMode (compositionMode);
    painter.scale (zoomFactor, zoomFactor);

    QColor penColor = pen.color();
    QPen tmpPen = pen;
    tmpPen.setColor (QColor ( penColor.red()*.5 + background.red()*.5,
                              penColor.green()*.2 + background.green()*.8,
                              penColor.blue()*.5 + background.blue()*.5,
                              penColor.alpha()*.8 + background.alpha()*.2
                            )
                    );
    painter.setPen (tmpPen);

    //
    // Lưu lại phần sẽ thay đổi
    auto extra = 2.5f * (1 + painter.pen().widthF());
    auto changeRect = shape.controlPointRect();

    changeRect = changeRect.adjusted (-extra, -extra, extra, extra);
    changeRect =
        QRectF (changeRect.x() * zoomFactor, changeRect.y() * zoomFactor,
                changeRect.width() * zoomFactor,
                changeRect.height() * zoomFactor);

    *(changedPart.localData()) = static_cast<QImage *>(painter.device())
                                    ->copy (changeRect.toRect());
    *(lastTopLeft.localData()) = changeRect.topLeft().toPoint();

    // Vẽ hình mới
    painter.drawPath (shape);


    painter.setPen (pen);
    
    return oldChangeRect | QRectF (
                    changeRect.topLeft()/zoomFactor,
                    changeRect.bottomRight()/zoomFactor);
}

//! Đọc dữ liệu cho thao tác vẽ từ stream
/**
 * Đọc #shape từ stream,
 * #Type sẽ được đọc trước đó để xác định kiểu của thao tác vẽ. 
 * \param stream luồng dữ liệu ra
 * \sa save (QDataStream &)
 */
void ChangeShape::load (QDataStream &stream)
{
    stream >> shape;
}

//! Lưu trạng thái của thao tác vẽ vào stream
/**
 * Ghi #Type, #shape vào stream.
 * \param stream luồng dữ liệu vào
 * \sa load (QDataStream &)
 */
void ChangeShape::save (QDataStream &stream) const 
{
    stream << Type << shape;
}

