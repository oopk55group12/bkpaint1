/************************************************************************
        erase.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef ERASE_HH
#define ERASE_HH
#include "paintoperation.hh"

#include <QPointF>

//! Lớp đại diện cho thao tác xoá một phần bức vẽ
/**
 * Phần bị xoá sẽ là một hình tròn có tâm là #centre và bán kính #radius.
 */
class Erase : public PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    Erase (QPointF centre = QPointF(), qint16 radius = 0);


    // Static Public attributes
    //

    //! Kiểu của thao tác
    static const qint32 Type = 5;


    // Public methods
    //

    //! Kiểu của thao tác vẽ
    /**
     * Thực chất trả về Type
     * \return qint32 kiểu của thao tác
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }

    //! Thực hiện thao tác
    QRectF operate (QPainter &painter) const override;

    //! Lưu trạng thái của thao tác vẽ vào stream
    void save (QDataStream &stream) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &stream) override;

private:
    // Private attributes
    //

    //! Tâm của phần hình tròn sẽ bị xoá
    QPointF centre;

    //! Bán kính phần sẽ bị xoá
    qint16 radius;
};

#endif // ERASE_HH
