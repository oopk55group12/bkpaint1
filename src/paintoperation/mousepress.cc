
/************************************************************************
        mousepress.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "mousepress.hh"
#include <QImage>

// Static members
//

MousePress::TPImage MousePress::changedPart;
MousePress::TPPointF MousePress::lastTopLeft;

// Constructors/Destructors
//

//! Phương thức khởi tạo
/**
 */
MousePress::MousePress()
{
}

//
// Methods
//

//! Thực hiện thao tác 
/**
 * "Làm sạch" #changedPart để bắt đầu cho một chuỗi ChangeShape
 * \param painter đối tượng vẽ sẽ thao tác
 * \return Vùng bức vẽ sẽ bị thay đổi sau khi thực hiện
 */
QRectF MousePress::operate (QPainter &painter) const
{
    *(changedPart.localData()) = static_cast<QImage *>(painter.device())
                                ->copy ( { 0, 0, 1, 1 });

    *(lastTopLeft.localData()) = { 0, 0 };

    return QRectF (0,0,0,0);
}

//! Đọc dữ liệu cho thao tác vẽ từ stream
/**
 * Thực chất phương thức này không làm gì.
 * #Type sẽ được đọc trước đó để xác định kiểu của thao tác vẽ. 
 * \param stream luồng dữ liệu vào
 * \sa save (QDataStream &)
 */
void MousePress::load (QDataStream &stream)
{
    Q_UNUSED (stream)
}

//! Lưu trạng thái của thao tác vẽ vào stream
/**
 * Ghi #Type vào stream.
 * \param stream luồng dữ liệu ra
 * \sa load (QDataStream &)
 */
void MousePress::save (QDataStream &stream) const
{
    stream << Type;
}
