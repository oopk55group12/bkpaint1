
/************************************************************************
        setbrush.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "setbrush.hh"

// Constructors/Destructors
//

//! Phương thức khởi tạo
SetBrush::SetBrush (QBrush const &brush /* = QBrush() */) :
    brush (brush)
{
}

//
// Methods
//

//! Thực hiện thao tác
/**
 * Thay bút lông cũ của painter bằng #brush
 * \param painter đối tượng vẽ sẽ thao tác
 * \return Vùng bức vẽ sẽ bị thay đổi sau khi thực hiện
 */
QRectF SetBrush::operate (QPainter &painter) const
{
    painter.setBrush (brush); 
    return QRectF (0,0,0,0);
}

//! Lưu trạng thái của thao tác vẽ vào stream
/**
 * Ghi #Type, #brush vào stream.
 * \param stream luồng dữ liệu ra
 * \sa load (QDataStream &)
 */
void SetBrush::save (QDataStream &stream) const
{
    stream << Type << brush;
}

//! Đọc dữ liệu cho thao tác vẽ từ stream
/**
 * Đọc #brush từ stream. #Type sẽ được đọc trước đó để xác định kiểu của thao 
 * tác vẽ.
 * \param stream luồng dữ liệu vào
 * \sa save (QDataStream &)
 */
void SetBrush::load (QDataStream &stream)
{
    stream >> brush;
}

