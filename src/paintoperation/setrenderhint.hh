/************************************************************************
        setrenderhint.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef SETRENDERHINT_HH
#define SETRENDERHINT_HH
#include "paintoperation.hh"

//! Lớp đại diện cho thao tác thay đối render hint
/*!
 * Tất cả các thao tác vẽ (PaintOperation) đều chia sẽ cùng một đối tượng vẽ
 * QPainter. Trong lớp QPainter có thuộc tính kiểu Painter::RenderHints 
 * bao gồm các cờ (kiểu QPainter::RenderHint) quy định việc render các thao tác
 * vẽ hình ra Canvas.
 * 
 * Lớp SetPen có thuộc tính #hint, khi thực hiện thì sẽ bật hoặc tắt cơ tương
 * ứng (tùy thuộc vào thuộc tính #on).
 *
 * \sa QPainter, \qt{qpainter.html#RenderHint-enum, QPainter::RenderHints}
 */
class SetRenderHint : public PaintOperation {
public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    SetRenderHint (QPainter::RenderHint hint = QPainter::RenderHint(), 
                        bool on = true);


    // Static Public attributes
    //

    //! Kiểu của thao tác
    static const qint32 Type = 9;

    // Public methods
    //

    //! Kiểu của thao tác
    /**
     * Thực chất trả về #Type.
     * \return qint32 kiểu của thao tác
     * \sa Type
     */
    qint32 type() const override {
        return Type;
    }

    //! Thực hiện thao tác
    QRectF operate (QPainter &painter) const override;

    //! Lưu trạng thái của thao tác vẽ vào stream
    void save (QDataStream &stream) const override;

    //! Đọc dữ liệu cho thao tác vẽ từ stream
    void load (QDataStream &stream) override;

private:
    // Private attributes
    //

    //! hint 
    QPainter::RenderHint hint;

    //! Trạng thái (bật/tắt)
    bool on;
};

#endif // SETRENDERHINT_HH
