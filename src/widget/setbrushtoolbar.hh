
/************************************************************************
        setbrushtoolbar.hh

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef SETBRUSHTOOLBAR_HH
#define SETBRUSHTOOLBAR_HH

#include "toolbar.hh"

class QSignalMapper;
class QMenu;
class QActionGroup;

//! Thanh công cụ thay đổi chổi lông
/*!
 * Với SetBrushToolBar, có thể thay đổi được màu chổi lông (#actionBrushColor),
 * kiểu mẫu tô (#brushStyleMenu).
 * \sa SetBrush, \qt{qt.html#BrushStyle-enum, Qt::BrushStyle}, QBrush::color()
 */
class SetBrushToolBar : public ToolBar {
    
    Q_OBJECT

public:

    // Constructors / Destructors
    //

    //! Phương thức khởi tạo
    SetBrushToolBar (QWidget *parent = nullptr);

    //! Phương thức khởi tạo
    SetBrushToolBar (QString const &title, QWidget *parent = nullptr);


    // Public Methods
    //

    //! Thay đổi icon của #brushStyleMenu
    void reloadBrushStyleIcon (int);

    //! Thay đổi màu của #actionBrushColor
    void reloadBrushColorIcon (QColor const &);

signals:
    // Signals
    //

    //! Yêu cầu chọn màu
    /*!
     * Được phát ra khi #actionBrushColor được kích họat
     * \sa actionBrushColor
     */
    void brushColorNeeded();

    //! Yêu cầu thay đổi kiểu chổi lông
    /*!
     * Được phát ra khi một trong các action thuộc #brushStyleMenu được kích hoạt
     * \param style kiểu chổi lông được chọn
     * \sa brushStyleMenu, \qt{qt.html#BrushStyle-enum,Qt::BrushStyle}
     */
    void brushStyleChanged (int style);

private:
    // Private Methods
    //

    //! Khởi tạo thanh công cụ
    void init();

    //! Khởi tạo các widget thuộc thanh công cụ
    void initWidgets();

    //! Khởi tạo các action thuộc thanh công cụ
    void initActions();

private:
    // Private attributes
    //

    //! Yêu cầu thay đổi màu chổi lông
    /*!
     * \sa QColor, QBrush::color()
     */
    QAction *actionBrushColor;

    //! Trình đơn chọn kiểu mẫu tô
    /*!
     * \sa \qt{qt.html#BrushStyle-enum, Qt::BrushStyle}, QBrush::style()
     */
    QMenu *brushStyleMenu;

    //! Nhóm các hành động chọn kiểu mẫu tô
    /*!
     * \sa brushStyleMenu
     */
    QActionGroup *brushStyleActionGroup;

    //! Ánh xạ các hành động chọn kiểu mẫu tô vào một signal duy nhất
    /*!
     * \sa brushStyleMenu
     */
    QSignalMapper *brushStyleSignalMapper;

    //! Đối tượng Style với icon có chiều rộng lớn
    LargeIconWidthStyle largeIconWidthStyle;
};

#endif // SETBRUSHTOOLBAR_HH
