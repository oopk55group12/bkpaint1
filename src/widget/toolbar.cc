
/************************************************************************
        toolbar.cc

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "toolbar.hh"

// Static initialization
//

const int ToolBar::IconWidth = 24;
const int ToolBar::IconHeight = 24;
const int ToolBar::LargeIconWidth = 70;

// Constructors / Destructors
//

//! Phương thức khởi tạo
/*!
 * Khởi tạo với cha cho trước
 * \param parent đối tượng cha
 * \sa QToolBar::QToolBar(QWidget *), ToolBar(QString const &, QWidget *)
 */
ToolBar::ToolBar (QWidget *parent)
    : QToolBar (parent)
{
    setIconSize ({IconWidth, IconHeight});
}

//! Phương thức khởi tạo
/*!
 * Khởi tạo với cha và tiêu đề cho trước
 * \param title tiêu đề
 * \param parent đối tượng cha
 * \sa ToolBar(QWidget *), QToolBar:QToolBar(QString const &, QWidget *)
 */
ToolBar::ToolBar (QString const &title, QWidget *parent)
    : QToolBar (title, parent)
{
    setIconSize ({IconWidth, IconHeight});
}


//! Phương thức hủy ảo
/*!
 * \sa ToolBar(QWidget*), ToolBar(QString const &, QWidget*)
 */
ToolBar::~ToolBar()
{}


// Methods
//

//! Thêm một khoảng trống vào cuối thanh công cụ
/*!
 * \param spaceSize kích thước khoảng trống
 */
void ToolBar::addSpace (int spaceSize)
{
    QWidget *stretchWidget = new QWidget();
    stretchWidget->resize (spaceSize, spaceSize);
    stretchWidget->setMinimumSize (stretchWidget->size());
    addWidget (stretchWidget);
}


//! Sinh icon với màu và kích thước cho trước
/*!
 * Icon sẽ có dạng một hình chữ nhật kích thước width x height và được tô màu
 * color
 * \param color Màu của icon
 * \param width chiều rộng
 * \param height chiều cao
 * \return Icon 
 */
QIcon ToolBar::genColorIcon (QColor const &color, int width, int height)
{
    QPixmap pixmap (width, height);

    pixmap.fill (color);
    return QIcon (pixmap);
}
