#ifndef COLORPICKER_HH
#define COLORPICKER_HH

#include <QWidget>
#include <QDir>
#include <vector>

class QMenu;
class QActionGroup;

//! Thanh chọn màu
/*!
 * Khi người dùng click chuột trái vào ColorPicker thì sẽ phát ra thông điệp
 * #penColorPicked(QColor), nếu nhấn thêm Shift thì phát ra thông điệp
 * #brushColorPicked(QColor). Giá trị QColor này là màu của ColorPicker tại
 * điểm click chuột.
 * Các đối tượng điều khiển khác có thể sử nhận các tín hiệu này và sửa lại màu
 * đang sử dụng.
 *
 * Ngoài ra, nếu di chuột qua một điểm trên ColorPicker thì sẽ phát ra tín hiệu
 * #colorHovered (QString) với QString là tên của màu tại điểm đó (\sa
 * PaletteColor::colorName).
 *
 * ColorPicker có sẵn nhiều bảng màu (Palette) khác nhau, được lưu dưới dạng
 * file .gpl trong thư mục /usr/share/bkpaint/palettes (trên Windows là thư mục
 * palettes có thư mục cha trùng với file chạy chương trình). Chọn bảng màu
 * bằng cách click chuột phải.
 */
class ColorPicker : public QWidget {

    Q_OBJECT

private:
    //
    // Types

    //! Lớp đại diện cho một màu thuộc bảng màu
    struct PaletteColor {
        //! Màu
        QColor color;

        //! Tên màu
        QString colorName;
    };

    //! Lớp đại diện cho một bảng màu
    struct Palette {
        //! Tên bảng màu
        QString paletteName;

        //! Danh sách các màu thuộc bảng màu
        std::vector<PaletteColor> colors;
    };

    //! typedef cho kiểu con chạy đến một bảng màu trong danh sách bảng màu
    typedef std::vector<Palette>::const_iterator PaletteIter;

public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    explicit ColorPicker (QWidget *parent = 0);

    //! Phương thức huỷ
    virtual ~ColorPicker();


    //
    // Static Publics methods

    //! Nhập danh sách bẳng màu từ các file .gpl
    static void initPalette(QDir const &dir);


    // 
    // Public methods

    //! Chọn bảng màu
    void setPalette (PaletteIter, int);

signals:
    //! Chọn màu nét vẽ
    void penColorPicked (QColor);

    //! Chọn màu tô
    void brushColorPicked (QColor);

    //! C
    void colorHovered (QString const &colorName);

protected:
    //! Sự kiện vẽ lại
    void paintEvent (QPaintEvent *) override;

    //! Sự kiện nhấn chuột
    void mousePressEvent (QMouseEvent *) override;

    //! Sự kiện di chuột
    void mouseMoveEvent (QMouseEvent *) override;

private:
    // Private methods
    //

    //! Lấy màu tại một điểm
    QColor colorAt (QPoint const &p) const;

    //! Khởi tạo trình đơn ngữ cảnh
    void initContexMenu();

private slots:
    //! Chọn một bảng màu
    void paletteAction_triggered();

private:
    // Static Private attributes
    //

    //! Danh sách các bảng màu 
    static std::vector<Palette> paletteList;

    //! Bảng màu mặc định
    static PaletteIter defaultPalette;


    // Private attributes
    //

    //! Bảng màu đang sử dụng
    PaletteIter usingPalette;

    //! Buffer, được vẽ trên đó dải các ô vuông có màu thuộc bảng màu đang sử dụng
    QImage *buffer;

    //! Kích thước (pixel) của ô vuông
    int atomicSize;

    //! Nhóm các QAction dành cho việc chọn bảng màu
    QActionGroup *paletteActionGroup;
};

#endif // COLORPICKER_HH
