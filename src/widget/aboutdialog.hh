
/************************************************************************
        aboutdialog.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef ABOUTDIALOG_HH
#define ABOUTDIALOG_HH

#include <QDialog>

class QDialogButtonBox;
class QPushButton;
class QLabel;

//! Thông tin về chương trình
class AboutDialog : public QDialog {

    Q_OBJECT

public:
    //
    // Constructors/Destructors

    //! Phương thức khởi tạo
    AboutDialog (QWidget *parent = nullptr);


    // Public Methods
    //

    //! Lấy kích thước mới
    QSize getNewSize() const;

private slots:
    //! Hiện thông tin về tác giả 
    void authorsButtonClicked();
    
    //! Hiện thông tin về giấy phép
    void licenseButtonClicked();

private:
    //! Hiện nội dung file
    void displayFile (QString const &fileName, QString const &title);

private:
    //! Nhãn hiện logo
    QLabel *logo;

    //! Nhãn hiện tên chương trình
    QLabel *name;

    //! Nhãn hiện số hiệu phiên bản
    QLabel *version;

    //! Nhãn hiện thông tin mô tả chương trình
    QLabel *description;

    //! Nhãn hiện tên tác giả
    QLabel *author;

    //! Nhãn hiện địa chỉ trang chủ
    QLabel *siteLink;

    QDialogButtonBox *buttonBox;

    //! Nút hiện thông tin tác giả
    QPushButton *authorsButton;

    //! Nút hiện thông tin về giấy phép
    QPushButton *licenseButton;

    //! Nút đóng hộp thoại
    QPushButton *closeButton;
};

#endif // ABOUTDIALOG_HH
