
/************************************************************************
        newfileoptiondialog.cc

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "newfileoptiondialog.hh"

#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QIntValidator>
#include <QSettings>
#include <QMessageBox>

//! Phương thức khởi tạo
NewFileOptionDialog::NewFileOptionDialog (QWidget *parent)
    : QDialog (parent)
{

    QSettings settings;


    QVBoxLayout *mainLayout = new QVBoxLayout();

    //
    // Size

    QHBoxLayout *sizeEditLayout = new QHBoxLayout();
    mainLayout->addLayout (sizeEditLayout);

    QLabel *labelWidth = new QLabel (tr ("&Width"), this);
    QString lastWidth = settings.value ("new_file/width", "400").toString();
    editWidth = new QLineEdit (lastWidth, this);
    editWidth->setValidator (new QIntValidator (1,5000,this));
    labelWidth->setBuddy (editWidth);

    QLabel *labelHeight = new QLabel (tr ("&Height"), this);
    QString lastHeight = settings.value ("new_file/height", "500").toString();
    editHeight = new QLineEdit (lastHeight, this);
    editHeight->setValidator (new QIntValidator (1,5000, this));
    labelHeight->setBuddy (editHeight);

    sizeEditLayout->addWidget (labelWidth);
    sizeEditLayout->addWidget (editWidth);
    sizeEditLayout->addStretch();
    sizeEditLayout->addWidget (labelHeight);
    sizeEditLayout->addWidget (editHeight);


    //
    // From clipboard

    clipboardCheckBox = new QCheckBox (this);
    clipboardCheckBox->setText (tr ("&From clipboard"));
    mainLayout->addWidget (clipboardCheckBox);
    
    connect (clipboardCheckBox, SIGNAL (toggled (bool)),
             editWidth, SLOT (setDisabled (bool)));
    connect (clipboardCheckBox, SIGNAL (toggled (bool)),
             editHeight, SLOT (setDisabled (bool)));

    //
    // Transparent background

    transparentCheckBox = new QCheckBox (this);
    transparentCheckBox->setText (tr ("&Transparent background"));
    mainLayout->addWidget (transparentCheckBox);

    bool isTransparent = settings.value ("new_file/transparent", true).toBool();
    transparentCheckBox->setChecked (isTransparent);

    //
    // Button box

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
            | QDialogButtonBox::Cancel);
    mainLayout->addWidget (buttonBox);

    connect(buttonBox, SIGNAL (accepted()), this, SLOT (dialogAccepted()));
    connect(buttonBox, SIGNAL (rejected()), this, SLOT (reject()));
    
    
    setLayout (mainLayout);

    QPoint lastPos = settings.value ("new_file/pos").toPoint();
    move (lastPos);

}


//! Lấy thuộc tính file
/*!
 * Tập thuộc tính được xây dựng từ trạng thái của các ô nhập liệu, các
 * checkbox.
 * \return tập thuộc tính của file
 */
NewFileOption NewFileOptionDialog::getNewFileOption() const 
{
    return { clipboardCheckBox->isChecked(),
             transparentCheckBox->isChecked(),
             QSize { editWidth->text().toInt(), editHeight->text().toInt() }
           };
}


//! Chấp nhận
/*!
 * Được gọi khi người dùng bấm nút OK
 */
void NewFileOptionDialog::dialogAccepted ()
{
    if (editWidth->text().toInt() == 0 || editHeight->text().toInt() == 0) {
        QMessageBox::warning (this, tr ("Error"), tr ("Invalid size!"));
    } else {
        QSettings settings;
        settings.setValue ("new_file/width", editWidth->text());
        settings.setValue ("new_file/height", editHeight->text());
        settings.setValue ("new_file/transparent", 
                                transparentCheckBox->isChecked());
        settings.setValue ("new_file/pos", pos());
        accept();
    }
}
