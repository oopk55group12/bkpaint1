
/************************************************************************
        newfileoptiondialog.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef NEWFILEOPTIONDIALOG__HH
#define NEWFILEOPTIONDIALOG__HH

#include <QDialog>

class QDialogButtonBox;
class QLineEdit;
class QCheckBox;

//! Các thuộc tính của file mới tạo
struct NewFileOption {
    //! Nhập ảnh từ clipboard
    bool isFromClipboard;

    //! Nền trong suốt
    bool isTransparent;

    //! Kích thước ảnh
    /*!
     * Nếu #isFromClipboard = true thì thuộc tính này bị bỏ qua
     */
    QSize size;
};


//! Hộp thoại chọn thuộc tính cho file mới tạo
class NewFileOptionDialog : public QDialog {

    Q_OBJECT

public:
    //
    // Constructors/Destructors

    //! Phương thức khởi tạo
    NewFileOptionDialog (QWidget *parent = nullptr);


    // Public Methods
    //

    //! Lấy thuộc tính file
    NewFileOption getNewFileOption() const;

private slots:
    //! Chấp nhận
    void dialogAccepted();

private:
    QDialogButtonBox *buttonBox;

    //! Ô nhập độ rộng bức vẽ (pixel)
    QLineEdit *editWidth;
    //! Ô nhập chiều cao bức vẽ (pixel)
    QLineEdit *editHeight;

    //! Tùy chọn có nhập ảnh từ clipboard hay không
    QCheckBox *clipboardCheckBox;

    //! Tùy chọn nền trong suốt hay không
    QCheckBox *transparentCheckBox;
};

#endif // NEWFILEOPTIONDIALOG__HH
