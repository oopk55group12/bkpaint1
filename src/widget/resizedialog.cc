
/************************************************************************
        resizedialog.cc

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "resizedialog.hh"

#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QIntValidator>
#include <QMessageBox>
#include <QSettings>

//! Phương thức khởi tạo
ResizeDialog::ResizeDialog (QSize const &size, QWidget *parent)
    : QDialog (parent), currentSize (size)
{

    QSettings settings;


    QVBoxLayout *mainLayout = new QVBoxLayout();

    //
    // Size

    QHBoxLayout *sizeEditLayout = new QHBoxLayout();
    mainLayout->addLayout (sizeEditLayout);

    QLabel *labelWidth = new QLabel (tr ("&Width"), this);
    editWidth = new QLineEdit (QString::number (currentSize.width()), this);
    editWidth->setValidator (new QIntValidator (1,5000,this));
    labelWidth->setBuddy (editWidth);

    QLabel *labelHeight = new QLabel (tr ("&Height"), this);
    editHeight = new QLineEdit (QString::number (currentSize.height()), this);
    editHeight->setValidator (new QIntValidator (1,5000, this));
    labelHeight->setBuddy (editHeight);

    sizeEditLayout->addWidget (labelWidth);
    sizeEditLayout->addWidget (editWidth);
    sizeEditLayout->addStretch();
    sizeEditLayout->addWidget (labelHeight);
    sizeEditLayout->addWidget (editHeight);


    //
    // Button box

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
            | QDialogButtonBox::Cancel);
    mainLayout->addWidget (buttonBox);

    connect(buttonBox, SIGNAL (accepted()), this, SLOT (dialogAccepted()));
    connect(buttonBox, SIGNAL (rejected()), this, SLOT (reject()));
    
    
    setLayout (mainLayout);

    QPoint lastPos = settings.value ("new_file/pos").toPoint();
    move (lastPos);
}


//! Lấy kích thước mới của file
/*!
 * \return kích thước mới của file
 */
QSize ResizeDialog::getNewSize() const 
{
    return QSize (editWidth->text().toInt(), editHeight->text().toInt());
}


//! Chấp nhận
/*!
 * Được gọi khi người dùng bấm nút OK
 */
void ResizeDialog::dialogAccepted ()
{
    if (editWidth->text().toInt() == 0 || editHeight->text().toInt() == 0) {
        QMessageBox::warning (this, tr ("Error"), tr ("Invalid size!"));
    } else {
        QSettings settings;
        settings.setValue ("new_file/pos", pos());
        accept();
    }
}
