
/************************************************************************
        aboutdialog.cc

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "aboutdialog.hh"

#include <QDialogButtonBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QApplication>
#include <QTextEdit>
#include <QTextStream>
#include <QFile>

//! Phương thức khởi tạo
AboutDialog::AboutDialog (QWidget *parent) : QDialog (parent)
{

    setWindowTitle (tr ("About %1").arg (qApp->applicationName()));
    QVBoxLayout *mainLayout = new QVBoxLayout();

    //
    // Labels

    auto centerAlignment = Qt::AlignHCenter | Qt::AlignVCenter;

    logo = new QLabel();
    logo->setAlignment (centerAlignment);
    logo->setPixmap (qApp->windowIcon().pixmap (128, 128));
    mainLayout->addWidget (logo);

    name = new QLabel (qApp->applicationName());
    QFont nameFont = name->font();
    nameFont.setBold (true);
    nameFont.setPointSize (16);
    name->setFont (nameFont);
    name->setAlignment (centerAlignment);
    mainLayout->addWidget (name);

    version = new QLabel (tr ("Version ") + qApp->applicationVersion());
    version->setAlignment (centerAlignment);
    mainLayout->addWidget (version);

    description = new QLabel (tr ("A extended paint application with "
                                    "replay painting feature."));
    description->setAlignment (centerAlignment);
    mainLayout->addWidget (description);

    author = new QLabel (tr ("Copyright © 2012 Group 12 - K55"));
    author->setAlignment (centerAlignment);
    mainLayout->addWidget (author);

    siteLink = new QLabel (
            "<a href=\"https://bitbucket.org/oopk55group12/bkpaint1/\">"
            "https://bitbucket.org/oopk55group12/bkpaint1/</a>");
    siteLink->setAlignment (centerAlignment);
    siteLink->setOpenExternalLinks (true);
    mainLayout->addWidget (siteLink);


    //
    // Button box

    buttonBox = new QDialogButtonBox();
    mainLayout->addWidget (buttonBox);

    authorsButton = new QPushButton (tr ("&Authors"));
    buttonBox->addButton (authorsButton, QDialogButtonBox::ActionRole);
    connect (authorsButton, SIGNAL (clicked()), SLOT (authorsButtonClicked()));

    licenseButton = new QPushButton (tr ("&License"));
    buttonBox->addButton (licenseButton, QDialogButtonBox::ActionRole);
    connect (licenseButton, SIGNAL (clicked()), SLOT (licenseButtonClicked()));

    closeButton = new QPushButton (tr ("&Close"));
    buttonBox->addButton (closeButton, QDialogButtonBox::RejectRole);
    connect(buttonBox, SIGNAL (rejected()), SLOT (reject()));
    
    setLayout (mainLayout);
}

//! Hiện nội dung file
/*!
 * Nội dung file sẽ được hiển thị trong một hộp thoại mới
 * \param fileName tên file
 * \param title tiêu đề của hộp thoại hiển thị
 */
void AboutDialog::displayFile (const QString &fileName, const QString &title)
{
    QDialog dialog(this);
    QVBoxLayout layout (&dialog);
    QTextEdit textEdit (&dialog);
    QDialogButtonBox box (QDialogButtonBox::Close, 
                                        Qt::Horizontal, &dialog);

    textEdit.setLayoutDirection (Qt::LeftToRight);

    QFile file (fileName);
    if (!file.open (QIODevice::ReadOnly))
        return;

    QTextStream stream (&file);
    stream.setCodec ("UTF-8");
    QString text = stream.readAll();
    // force the content of the text editor to be LTR, and monospaced.
    textEdit.setHtml (QString (QLatin1String ("<pre>%1</pre>")).arg(text));

    textEdit.setReadOnly (true);
    connect(&box, SIGNAL (rejected()), &dialog, SLOT (close()));
    box.setCenterButtons (true);
    layout.addWidget (&textEdit);
    layout.addWidget (&box);
    layout.setMargin (6);

    dialog.setLayout (&layout);
    dialog.setWindowTitle (title);
    dialog.setWindowFlags (Qt::Sheet);
    dialog.resize (600, 350);
    dialog.exec();
}

//! Hiện thông tin về tác giả
void AboutDialog::authorsButtonClicked()
{
    displayFile (QLatin1String (":/files/AUTHORS"), tr ("Authors"));
}

//! Hiện thông tin về giấy phép
void AboutDialog::licenseButtonClicked()
{
    displayFile (QLatin1String (":/files/LICENSE"), tr ("License"));
}

