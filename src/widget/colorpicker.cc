#include "colorpicker.hh"

#include <QPaintEvent>
#include <QMouseEvent>
#include <QPainter>
#include <QDirIterator>
#include <QFile>
#include <QTextStream>
#include <QActionGroup>
#include <QAction>
#include <QMenu>

std::vector<ColorPicker::Palette> ColorPicker::paletteList;
ColorPicker::PaletteIter ColorPicker::defaultPalette;

// Constructors/Destructors
//

//! Phương thức khởi tạo
/*!
 * usingPalette được khởi tạo là bẳng màu mặc định.
 * Nếu chưa có bảng màu nào thì #buffer sẽ bằng nullptr.
 * \param parent widget cha
 * \sa defaultPalette, usingPalette, buffer
 */
ColorPicker::ColorPicker (QWidget *parent)
    : QWidget (parent), usingPalette(defaultPalette), buffer (nullptr)
{
    setBackgroundRole (QPalette::Base);
    setMouseTracking (true);

    if (usingPalette != paletteList.end())
        setPalette (usingPalette, 20);
    else {
        setMaximumSize (QSize (0, 0));
        resize (QSize (0, 0));
    }

    initContexMenu();
}

//! Phương thức huỷ
ColorPicker::~ColorPicker()
{
    delete buffer;
}



// Methods
//

//! Khởi tạo trình đơn ngữ cảnh
/*!
 * Mỗi một bảng màu trong danh sách bảng màu (#paletteList) sẽ tương ứng với
 * một item của trình đơn ngữ cảnh.
 * \sa paletteActionGroup
 */
void ColorPicker::initContexMenu() {
    setContextMenuPolicy (Qt::ActionsContextMenu);

    if (paletteList.empty()) return;

    paletteActionGroup = new QActionGroup (this);
    for (uint i = 0; i != paletteList.size (); ++i) {
        QAction *act = new QAction (paletteList[i].paletteName, this);
        act->setCheckable (true);
        act->setData (i);
        connect (act, SIGNAL (triggered()), SLOT (paletteAction_triggered()));

        paletteActionGroup->addAction (act);
        addAction (act);
    }
    
    actions()[usingPalette - paletteList.cbegin()]->setChecked (true);
}

//! Chọn một bảng màu
/*! 
 * slot được gọi khi người dùng chọn một bảng màu trong trình đơn ngữ
 * cảnh.
 */
void ColorPicker::paletteAction_triggered()
{
    auto paletteIndex = qobject_cast<QAction*> (sender())->data().toUInt();
    setPalette (paletteList.cbegin() + paletteIndex, atomicSize);
}

//! Chọn bảng màu
/*!
 * Vẽ dải các ô vuông với màu thuộc bảng màu
 * \param palette con chạy đến bảng màu sẽ sử dụng trong danh sách bảng màu
 * \param atomicSize kích thước của một ô vuông
 * \sa usingPalette, atomicSize
 */
void ColorPicker::setPalette (PaletteIter palette, int atomicSize)
{
    this->usingPalette = palette;
    this->atomicSize = atomicSize;

    delete buffer;
    buffer = new QImage (QSize (palette->colors.size() * atomicSize, 
                                atomicSize), 
                         QImage::Format_ARGB32_Premultiplied);
    buffer->fill (Qt::transparent);
    QPainter painter (buffer);
    painter.setPen (Qt::NoPen);

    int x = 0;

    for (auto &pc : palette->colors) {
        painter.fillRect (x, 0, atomicSize, atomicSize, QBrush (pc.color));
        x += atomicSize;
    }

    setMaximumSize (buffer->size());
    resize (buffer->size());
}

//! Sự kiện vẽ lại
void ColorPicker::paintEvent (QPaintEvent *event)
{
    Q_UNUSED (event)

    QPainter painter (this);

    painter.drawImage (0, 0, *buffer);
}

//! Sự kiện nhấn chuột
/*!
 * Các tín hiệu #penColorPicked(), #brushColorPicked() sẽ được phát ra từ đây
 * \param event sư kiện chuột
 * \sa penColorPicked(), brushColorPicked()
 */
void ColorPicker::mousePressEvent (QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        QColor color = colorAt (event->pos());

        if (event->modifiers() & Qt::ShiftModifier)
            emit brushColorPicked (color);
        else
            emit penColorPicked (color);
    }
}

//! Sự kiện di chuột
/*!
 * Tín hiệu #colorHovered() sẽ được phát ra tại đây
 * \param event sự kiện chuột
 * \sa colorHovered()
 */
void ColorPicker::mouseMoveEvent (QMouseEvent *event)
{
    auto index = event->pos().x() / atomicSize;
    emit colorHovered (usingPalette->colors[index].colorName);
}

//! Lấy màu tại một điểm thuộc ColorPicker
/*!
 * \param p điểm thuộc ColorPicker cần lấy màu
 * \return màu tại p
 */
QColor ColorPicker::colorAt (QPoint const &p) const
{
    return QColor (buffer->pixel (p));
}

//! Nhập danh sách bẳng màu từ các file .gpl
/*!
 * Bảng màu nào có "default" ở cuối tên sẽ được chọn làm bảng màu mặc định.
 * Là phương thức tĩnh của lớp ColorPicker, được gọi trước khi bất cứ đối tượng
 * ColorPicker nào được khởi tạo.
 * \param dir thư mục chứa các file .gpl
 * \sa paletteList, defaultPalette
 */
void ColorPicker::initPalette (QDir const &dir)
{
    QStringList gplFilters { "*.gpl" };
    QDir::Filters filters = QDir::Files | QDir::Readable;
    for (auto &info : dir.entryInfoList (gplFilters, filters)) {

        QFile file (info.filePath());
        if (!file.open (QIODevice::ReadOnly))
            continue;

        QTextStream stream (&file);

        if (stream.readLine() != "GIMP Palette")
            continue;

        QString paletteName = stream.readLine();
        if (paletteName.startsWith("Name:"))
            paletteName = paletteName.mid (5).trimmed();
        else
            paletteName = info.baseName();

#if defined(__clang__) && __clang_major__ == 3 && __clang_minor__ <=1
        std::vector<PaletteColor> transparent;
        transparent.push_back ({QColor (Qt::transparent), tr ("Transparent")});
        Palette p { paletteName, transparent } ;
#else
        Palette p { paletteName, {{QColor (0,0,0,0), tr ("Transparent")}} };
#endif

        while (stream.readLine()[0] != '#');

        int r, g, b;
        while (!(stream >> r).atEnd()) {
            stream >> g >> b;
            p.colors.push_back ( { 
                    QColor (r, g, b), 
                    stream.readLine().trimmed()
                } );
        }

        paletteList.push_back (std::move (p));
    }

    defaultPalette = PaletteIter();

    for (auto iter = paletteList.cbegin(); iter != paletteList.cend(); ++iter) {
        if (iter->paletteName.endsWith (" default")) {
            defaultPalette = iter;
            break;
        }
    }

    if (defaultPalette == PaletteIter() && !paletteList.empty())
        defaultPalette = paletteList.cbegin();
}
