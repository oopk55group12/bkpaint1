
/************************************************************************
        setpentoolbar.cc

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "setpentoolbar.hh"

#include <QSlider>
#include <QSignalMapper>
#include <QAction>
#include <QActionGroup>
#include <QMenu>
#include <QLabel>
#include <QPen>
#include <QPainter>


//! Phương thức khởi tạo
/*!
 * Khởi tạo với cha cho trước
 * \param parent đối tượng cha
 * \sa ToolBar::ToolBar(QWidget *), SetPenToolBar(QString const &, QWidget *)
 */
SetPenToolBar::SetPenToolBar (QWidget *parent)
    : SetPenToolBar ("", parent)
{}


//! Phương thức khởi tạo
/*!
 * Khởi tạo với cha và tiêu đề cho trước
 * \param title tiêu đề
 * \param parent đối tượng cha
 * \sa SetPenToolBar(QWidget *), ToolBar:ToolBar(QString const &, QWidget *)
 */
SetPenToolBar::SetPenToolBar (QString const &title, QWidget *parent)
    : ToolBar (title, parent)
{
    setIconSize ( { LargeIconWidth, IconHeight } );
    initWidgets();
    initActions();
    init();
}

// Methods
//


//! Khởi tạo các widget thuộc thanh công cụ
/*!
 * Khởi tạo cho #penWidthSlider
 * \sa init(), initActions()
 */
void SetPenToolBar::initWidgets()
{
    // pen width slider
    //

    penWidthSlider = new QSlider (Qt::Horizontal, this);
    penWidthSlider->setMaximumSize (50, 50);
    penWidthSlider->setRange (0, 40);
    penWidthSlider->setValue (0);
    penWidthSlider->setTickInterval (5);
    penWidthSlider->setTickPosition (QSlider::TicksAbove);
    penWidthSlider->setToolTip (tr ("Pen Width"));

    connect (penWidthSlider, SIGNAL (valueChanged (int)),
             SIGNAL (penWidthChanged (int)));
}


//! Khởi tạo các action thuộc thanh công cụ
/*!
 * Khởi tạo cho #actionPenColor
 * \sa init(), initWidgets()
 */
void SetPenToolBar::initActions()
{
    // Pen color
    actionPenColor = new QAction (tr ("Pen color"), this);
    actionPenColor->setIcon (
                genColorIcon (QPen().color(), IconWidth, IconHeight));
    connect (actionPenColor, SIGNAL (triggered()),
             SIGNAL (penColorNeeded()));
}


//! Khởi tạo thanh công cụ
/*!
 * Kết nối các action, widget với thanh công cụ
 * \sa initWidgets(), initActions()
 */
void SetPenToolBar::init()
{
    // pen color
    //

    addWidget (new QLabel (tr ("Pen color: ")));
    addAction (actionPenColor);

    // pen style
    addSpace (IconHeight / 2);
    addWidget (new QLabel (tr ("Pen style: ")));

    penStyleMenu = new QMenu (tr ("Pen Style"), this);
    penStyleMenu->setStyle (&largeIconWidthStyle);
    penStyleSignalMapper = new QSignalMapper (this);
    penStyleActionGroup = new QActionGroup (this);
    QString penStyleNames[] = { tr ("No style"), tr ("Solid-line"),
                                tr ("Dash-line"), tr ("Dot-line"),
                                tr ("Dash-dot line"), tr ("Dash-dot-dot line")
                              };

    QString penStyleDescs[] = {
        tr ("No line at all"), tr ("A plain line"),
        tr ("Dashes separated by a few pixels"),
        tr ("Dots separated by a few pixels"),
        tr ("Alternate dots and dashes"),
        tr ("One dash, two dots, one dash, to dots")
    };

    QPainter penStylePainter;
    QPen pen;

    pen.setWidth (3);
    for (int i = Qt::NoPen; i <= Qt::DashDotDotLine; ++i) {
        // create icon
        QPixmap icon (LargeIconWidth, 12);

        icon.fill (Qt::transparent);
        pen.setStyle (static_cast<Qt::PenStyle>(i));
        penStylePainter.begin (&icon);
        penStylePainter.setPen (pen);
        penStylePainter.drawLine (3, 5, LargeIconWidth - 3, 5);
        penStylePainter.end();

        // create action
        QAction *action = penStyleMenu->addAction (icon, penStyleNames[i]);

        action->setToolTip (penStyleDescs[i]);
        action->setCheckable (true);
        penStyleActionGroup->addAction (action);
        penStyleSignalMapper->setMapping (action, i);

        connect (action, SIGNAL (triggered()), penStyleSignalMapper,
                 SLOT (map()));

        if (i == Qt::SolidLine) {
            penStyleMenu->setDefaultAction (action);
            penStyleMenu->setIcon (icon);
            action->setChecked (true);
        }
    }

    addAction (penStyleMenu->menuAction());

    connect (penStyleSignalMapper, SIGNAL (mapped (int)),
             SIGNAL (penStyleChanged (int)));


    // pen width
    addSpace (IconHeight / 2);
    addWidget (new QLabel (tr ("Pen width: ")));

    addWidget (penWidthSlider);
    connect (this, SIGNAL (orientationChanged (Qt::Orientation)),
             penWidthSlider, SLOT (setOrientation (Qt::Orientation)));

}

//! Thay đổi màu của #actionPenColor
/*!
 * \param color Màu mới
 * \sa actionPenColor
 */
void SetPenToolBar::reloadPenColorIcon (QColor const &color)
{
    actionPenColor->setIcon (genColorIcon (color, IconWidth, IconHeight));
}

//! Thay đổi icon của #penStyleMenu
/*!
 * Icon mới của penStyleMenu là icon của action với vị trí id thuộc
 * penStyleMenu
 * \param id
 * \sa #penStyleMenu
 */
void SetPenToolBar::reloadPenStyleIcon (int id)
{
    penStyleMenu->setIcon (penStyleMenu->actions()[id]->icon());
}

//! Thay đổi giá trị của #penWidthSlider
/*!
 * \param width độ rộng nét vẽ, cũng là giá trị mới của penWidthSlider
 * \sa penWidthSlider
 */
void SetPenToolBar::setPenWidth (int width)
{
    penWidthSlider->setValue (width);
}
