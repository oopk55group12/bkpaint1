
/************************************************************************
        setbrushtoolbar.cc

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "setbrushtoolbar.hh"

#include <QSignalMapper>
#include <QAction>
#include <QActionGroup>
#include <QMenu>
#include <QLabel>
#include <QBrush>
#include <QPainter>


//! Phương thức khởi tạo
/*!
 * Khởi tạo với cha cho trước
 * \param parent đối tượng cha
 * \sa ToolBar::ToolBar(QWidget *), SetBrushToolBar(QString const &, QWidget *)
 */
SetBrushToolBar::SetBrushToolBar (QWidget *parent)
    : SetBrushToolBar ("", parent)
{}


//! Phương thức khởi tạo
/*!
 * Khởi tạo với cha và tiêu đề cho trước
 * \param title tiêu đề
 * \param parent đối tượng cha
 * \sa SetBrushToolBar(QWidget *), ToolBar:ToolBar(QString const &, QWidget *)
 */
SetBrushToolBar::SetBrushToolBar (QString const &title, QWidget *parent)
    : ToolBar (title, parent)
{
    setIconSize ( { LargeIconWidth, IconHeight } );
    initWidgets();
    initActions();
    init();
}

// Methods
//


//! Khởi tạo các widget thuộc thanh công cụ
/*!
 * \sa init(), initActions()
 */
void SetBrushToolBar::initWidgets()
{
}

//! Khởi tạo các action thuộc thanh công cụ
/*!
 * Khởi tạo cho #actionBrushColor
 * \sa init(), initWidgets()
 */
void SetBrushToolBar::initActions()
{
    // Brush color
    actionBrushColor = new QAction (tr ("Brush color"), this);
    actionBrushColor->setIcon (
                genColorIcon (QBrush().color(), IconWidth, IconHeight));
    connect (actionBrushColor, SIGNAL (triggered()),
             SIGNAL (brushColorNeeded()));
}


//! Khởi tạo thanh công cụ
/*!
 * Kết nối các action, widget với thanh công cụ
 * \sa initWidgets(), initActions()
 */
void SetBrushToolBar::init()
{
    // brush color
    addWidget (new QLabel (tr ("Brush color: ")));
    addAction (actionBrushColor);

    // brush style

    addSpace (IconHeight / 2);
    addWidget (new QLabel (tr ("Brush style: ")));
    brushStyleMenu = new QMenu (tr ("Brush Style"), this);
    brushStyleMenu->setStyle (&largeIconWidthStyle);
    brushStyleSignalMapper = new QSignalMapper (this);
    brushStyleActionGroup = new QActionGroup (this);

    QString brushStyleNames[] = {
        tr ("No brush"), tr ("Solid pattern"), tr ("Dense 1 pattern"),
        tr ("Dense 2 pattern"), tr ("Dense 3 pattern"), tr ("Dense 4 pattern"),
        tr ("Dense 4 pattern"), tr ("Dense 5 pattern"), tr ("Dense 6 pattern"),
        tr ("Dense 7 pattern"), tr ("Horizontal pattern"), tr ("Vertical pattern"),
        tr ("Cross pattern"), tr ("BDiag pattern"), tr ("FDiag pattern"),
        tr ("Diag cross pattern")
    };

    QString brushStyleDescs[] = {
        tr ("No brush pattern"), tr ("Uniform color"),
        tr ("Extremely dense brush pattern"), tr ("Very dense brush pattern"),
        tr ("Somewhat dense brush pattern"), tr ("Half dense brush pattern"),
        tr ("Somewhat sparse brush pattern"), tr ("Very sparse brush pattern"),
        tr ("Extremely sparse brush pattern"), tr ("Horizontal lines"),
        tr ("Vertical lines"), tr ("Crossing horizontal and vertical lines"),
        tr ("Backward diagonal lines"), tr ("Forward diagonal lines"),
        tr ("Crossing diagonal lines")
    };


    QPainter brushStylePainter;

    for (int i = Qt::NoBrush; i <= Qt::DiagCrossPattern; ++i) {
        // Create icon
        QPixmap icon (LargeIconWidth, IconHeight);

        icon.fill (Qt::transparent);
        brushStylePainter.begin (&icon);
        brushStylePainter.fillRect (0, 0, LargeIconWidth, IconHeight,
                                    static_cast<Qt::BrushStyle>(i));
        brushStylePainter.end();

        // Create action
        QAction *action = brushStyleMenu->addAction (icon, brushStyleNames[i]);

        action->setToolTip (brushStyleDescs[i]);
        action->setCheckable (true);
        brushStyleActionGroup->addAction (action);
        brushStyleSignalMapper->setMapping (action, i);

        connect (action, SIGNAL (triggered()), brushStyleSignalMapper,
                 SLOT (map()));

        if (i == Qt::NoBrush) {
            brushStyleMenu->setDefaultAction (action);
            brushStyleMenu->setIcon (icon);
            action->setChecked (true);
        }
    }

    addAction (brushStyleMenu->menuAction());

    connect (brushStyleSignalMapper, SIGNAL (mapped (int)),
             SIGNAL (brushStyleChanged (int)));
}

//! Thay đổi màu của #actionBrushColor
/*!
 * \param color Màu mới
 * \sa actionBrushColor
 */
void SetBrushToolBar::reloadBrushColorIcon (QColor const &color)
{
    actionBrushColor->setIcon (genColorIcon (color, IconWidth, IconHeight));
}

//! Thay đổi icon của #brushStyleMenu
/*!
 * Icon mới của brushStyleMenu là icon của action với vị trí id thuộc
 * brushStyleMenu
 * \param id
 * \sa #brushStyleMenu
 */
void SetBrushToolBar::reloadBrushStyleIcon (int id)
{
    brushStyleMenu->setIcon (brushStyleMenu->actions()[id]->icon());
}
