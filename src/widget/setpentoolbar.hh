/************************************************************************
        setpentoolbar.hh

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef SETPENTOOLBAR_HH
#define SETPENTOOLBAR_HH

#include "toolbar.hh"

class QSlider;
class QSignalMapper;
class QMenu;
class QActionGroup;

//! Thanh công cụ thay đổi nét vẽ
/*!
 * Với SetPenToolBar, có thể thay đổi được màu nét vẽ (#actionPenColor), độ
 * rộng nét vẽ (#penWidthSlider) và kiểu nét vẽ (#penStyleMenu).
 * \sa SetPen, \qt{qt.html#PenStyle-enum,Qt::PenStyle}, QPen::color(),
 * QPen::width()
 */
class SetPenToolBar : public ToolBar {
    
    Q_OBJECT

public:

    // Constructors / Destructors
    //

    //! Phương thức khởi tạo
    SetPenToolBar (QWidget *parent = nullptr);

    //! Phương thức khởi tạo
    SetPenToolBar (QString const &title, QWidget *parent = nullptr);


    // Public Methods
    //

    //! Thay đổi icon của #penStyleMenu
    void reloadPenStyleIcon (int id);

    //! Thay đổi màu của #actionPenColor
    void reloadPenColorIcon (QColor const &);

    //! Thay đổi giá trị của #penWidthSlider
    void setPenWidth (int);

signals:
    // Signals
    //

    //! Yêu cầu chọn màu
    /*!
     * Được phát ra khi #actionPenColor được kích họat
     * \sa actionPenColor
     */
    void penColorNeeded();

    //! Yêu cầu thay đổi kiểu nét vẽ
    /*!
     * Được phát ra khi một trong các action thuộc #penStyleMenu được kích hoạt
     * \param style kiểu nét vẽ được chọn
     * \sa penStyleMenu, \qt{qt.html#PenStyle-enum,Qt::PenStyle}
     */
    void penStyleChanged (int style);

    //! Yêu cầu thay đổi độ rộng nét vẽ
    /*!
     * Được phát ra khi thay đổi giá trị của #penWidthSlider
     * \param width độ rộng mới
     * \sa penWidthSlider
     */
    void penWidthChanged (int width);

private:
    // Private Methods
    //

    //! Khởi tạo thanh công cụ
    void init();

    //! Khởi tạo các widget thuộc thanh công cụ
    void initWidgets();

    //! Khởi tạo các action thuộc thanh công cụ
    void initActions();

private:
    // Private attributes
    //

    //! Yêu cầu thay đổi màu nét vẽ
    /*!
     * \sa QColor, QPen::color()
     */
    QAction *actionPenColor;

    //! Trình đơn chọn kiểu nét vẽ
    /*!
     * \sa \qt{qt.html#PenStyle-enum,Qt::PenStyle}, QPen::style()
     */
    QMenu *penStyleMenu;

    //! Nhóm các hành động chọn kiểu nét vẽ
    /*!
     * \sa penStyleMenu
     */
    QActionGroup *penStyleActionGroup;

    //! Ánh xạ các hành động chọn kiểu nét vẽ vào một signal duy nhất
    /*!
     * \sa penStyleMenu
     */
    QSignalMapper *penStyleSignalMapper;

    //! Thanh trượt thay đổi độ rộng nét vẽ
    /*!
     * \sa QPen::width()
     */
    QSlider *penWidthSlider;

    //! Đối tượng Style với icon có chiều rộng lớn
    LargeIconWidthStyle largeIconWidthStyle;
};

#endif // SETPENTOOLBAR_HH
