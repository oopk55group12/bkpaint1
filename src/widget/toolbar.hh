/************************************************************************
        toolbar.hh

  Copyright (C) 2012 - BOSS14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef TOOLBAR_HH
#define TOOLBAR_HH

#include <QToolBar>
#include <QProxyStyle>

//! Thanh công cụ
/*!
 * Là một "wrapper" của lớp QToolBar. Có cài đặt thêm một số phương thức như
 * #addSpace (int), #genColorIcon()
 * \sa QToolBar
 */
class ToolBar : public QToolBar {
public:
    // Constructors / Destructors
    //

    //! Phương thức khởi tạo
    ToolBar (QWidget *parent = nullptr);

    //! Phương thức khởi tạo
    ToolBar (QString const &title, QWidget *parent = nullptr);

    //! Phương thức hủy ảo
    virtual ~ToolBar();


    // Static Public Methods
    //

    //! Sinh icon với màu và kích thước cho trước
    static QIcon genColorIcon (QColor const &color, int width, int height);


    // Public Methods
    //

    //! Thêm một khoảng trống vào cuối thanh công cụ
    void addSpace (int spaceSize);

public:

    // Static Public attributes
    // 

    //! Độ rộng của Icon
    static const int IconWidth;

    //! Độ cao của Icon
    static const int IconHeight;

    //! Độ rộng của Icon với độ rộng lớn
    /*!
     * \sa LargeIconWidthStyle
     */
    static const int LargeIconWidth; 

protected:
    //! Style với icon có chiều rộng lớn
    /*!
     * Để sử dụng cho SetPenToolBar::penStyleMenu và
     * SetBrushToolBar::brushStyleMenu
     */
    class LargeIconWidthStyle : public QProxyStyle {
    public:
        int pixelMetric (PixelMetric metric,
                         const QStyleOption *option = 0,
                         const QWidget *widget = 0) const {
            int s = QProxyStyle::pixelMetric (metric, option, widget);
            if (metric == QStyle::PM_SmallIconSize) {
                s = LargeIconWidth;
            }
            return s;
        }
    };
};

#endif // TOOLBAR_HH
