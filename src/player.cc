
/************************************************************************
        player.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "player.hh"

#include "paintoperation/paintoperation"
#include "widget/newfileoptiondialog.hh"

#include <QImage>
#include <QPainter>
#include <QSize>
#include <QTimerEvent>
#include <QFile>
#include <QTemporaryFile>
#include <QFileInfo>
#include <QDir>
#include <QApplication>
#include <QClipboard>
#include <utility>

#include <QDebug>

// Constructors/Destructors
//

//! Phương thức khởi tạo
/*!
 * Các thuộc tính đều được khởi tạo về giá trị mặc định
 * \param parent cha của đối tượng Player
 */
Player::Player (QObject *parent /* = 0 */)
    : QObject (parent), painter (new QPainter), buffer (nullptr), 
        changedPart (new QImage), lastTopLeft (new QPointF),
        zoomFactor (1.0), isActive (false)
{
    MousePress::setChangedPart (changedPart);
    MousePress::setLastTopLeft (lastTopLeft);
    ChangeShape::setChangedPart (changedPart);
    ChangeShape::setLastTopLeft (lastTopLeft);

    connect (&timer, SIGNAL (timeout()), SLOT (timerTimeout()));
}

//! Phương thức khởi tạo
/*!
 * Tạo một bản vẽ mới với tùy chọn cho trước
 * \param nfOption tùy chọn cho bản vẽ (file) mới
 * \param parent cha của đối tượng Player
 * \sa Player (QObject*)
 */
Player::Player (NewFileOption const *nfOption, QObject *parent /* = 0 */)
    : Player (parent)
{
    if (nfOption->isFromClipboard) {
        QClipboard *clipboard = QApplication::clipboard();
        QImage image = clipboard->image();
        
        if (!image.isNull()) {
            loadImage(image);
            return;
        }
    }

    originalSize = nfOption->size;
    __background = nfOption->isTransparent ? Qt::transparent : Qt::white;

    buffer = new QImage (originalSize, QImage::Format_ARGB32_Premultiplied);
    buffer->fill (__background);
}

//! Phương thức hủy
/*!
 * Hủy các đối tượng cấp phát động không phải con của đối tượng Player
 * \sa buffer, painter
 */
Player::~Player()
{
    clearAll();
    delete painter;
}

void Player::clearAll()
{
    delete buffer;
    opPairList.clear();
}


//
// Methods
//


//! Thực hiện lại các thao tác
/*!
 * \param painter đối tượng vẽ 
 */
void Player::repaint (QPainter &painter)
{
    for (auto &pr : opPairList)
        pr.effOp->operate (painter);
}


//! Bắt đầu replay
/*!
 * \sa pause(), resume(), stop()
 */
void Player::play()
{
    painter->begin (buffer);
    painter->setBackground (__background);
    painter->scale (zoomFactor, zoomFactor);
    buffer->fill (__background);

    currentPair = opPairList.begin();
    if (!opPairList.empty())
        currentOp = currentPair->ops.cbegin();

    emit playerStarted();

    timer.start();
    isActive = true;
}


//! Tạm dừng quá trình replay
/*!
 * \sa play(), resume(), stop()
 */
void Player::pause()
{
    timer.stop();
}

//! Tiếp tục quá trình replay
/*!
 * \sa play(), pause(), stop()
 */
void Player::resume()
{
    timerTimeout();
}

//! Kết thúc quá trình replay
/*!
 * \sa play(), pause(), resume()
 */
void Player::stop()
{
    painter->end();
    timer.stop();
    isActive = false;
    emit playerStopped();
}


//! Trở về trạng thái trước khi thực hiện thao tác gần nhất
/*!
 * \sa redo(), undoStack, canUndoChanged(), canRedoChanged()
 */
void Player::undo()
{
    undoStack.push_back (std::move (opPairList.back()));
    opPairList.pop_back();

    emit canRedoChanged (true);

    if (opPairList.empty())
        emit canUndoChanged (false);
    emit playerModified (true);
}

//! Thực hiện lại thao tác vừa undo
/*!
 * \sa undo(), undoStack, canUndoChanged(), canRedoChanged()
 */
void Player::redo (QPainter &painter)
{
    auto op = std::move (undoStack.back());

    op.effOp->operate (painter);
    opPairList.push_back (op);
    undoStack.pop_back();

    emit canUndoChanged (true);

    if (undoStack.empty())
        emit canRedoChanged (false);
    emit playerModified (true);
}

//! Kết thúc thời gian chờ
/*!
 * Được gọi khi #timer phát ra tín hiệu timeout()
 * \sa timer
 */
void Player::timerTimeout()
{
    if (currentPair == opPairList.cend())
        stop();
    else {
        QRectF rect = (*currentOp)->operate (*painter);

        if (currentOp + 1 != currentPair->ops.cend())
            ++currentOp;
        else {
            ++currentPair;
            if (currentPair != opPairList.cend())
                currentOp = currentPair->ops.cbegin();
            else
                currentOp = (currentPair - 1)->ops.cend();
        }

        if (!rect.intersect (
                    QRectF ({0,0}, originalSize * zoomFactor)).isNull() )
        {
            emit nextOperation(rect);

            timer.start();
        } else {
            // Chuyển ngay sang thao tác khác nếu không làm thay đổi bức vẽ
            timerTimeout();
        }
    }
}

//! Thay đổi tỉ lệ phóng to, thu nhỏ
/*!
 * Nếu đang ở chế độ replay, #buffer sẽ được vẽ lại từ đầu. Nếu không thì chỉ
 * tạo ra buffer với kích thước mới.
 * \sa sf tỉ lệ phóng to / thu nhỏ
 * \sa zoomFactor
 */
void Player::zoom (qreal sf)
{
    zoomFactor = sf;

    if (isActive) {
        painter->end();

        *buffer = QImage (originalSize * zoomFactor, buffer->format());
        painter->begin (buffer);
        painter->scale (zoomFactor, zoomFactor);
        painter->setBackground (__background);
        buffer->fill (__background);

        for (auto pi = opPairList.cbegin(); pi != currentPair; ++pi)
            (pi->effOp)->operate (*painter);

        if (currentOp != currentPair->ops.cbegin()
                && (* (currentOp - 1))->type() == ChangeShape::Type) {
            *changedPart = buffer->copy (0, 0, 1, 1);
            *lastTopLeft = QPointF (0, 0);
            (* (currentOp - 1))->operate (*painter);
        }

    } else {
        *buffer = QImage (originalSize * zoomFactor, buffer->format());
    }
}


//! Thay đổi kích thước thực của Player
/*!
 * \param newSize kích thước mới
 * \sa originalSize
 */
void Player::resizeBuffer (QSize const &newSize)
{
    originalSize = newSize;
    *buffer = QImage (originalSize * zoomFactor, buffer->format());
}


//! Lưu danh sách các thao tác vẽ vào file
/*!
 * \param file File sẽ ghi vào
 * \sa load ()
 */
void Player::save (QFile &file)
{
    QDataStream out (&file);

    out << (quint32) 0x0A0B0C0E;	// Magic Number
    out << (qint32) 123;		// QDataStream version
    out.setVersion (QDataStream::Qt_4_0);

    out << originalSize;		// size
    out << __background;                // background

    qint32 numOp = 0;

    for (auto &pr : opPairList)
        numOp += pr.ops.size();
    out << numOp;

    for (auto &pr : opPairList)
        for (auto &op : pr.ops)
            op->save (out);
}


//! Tải danh sách các thao tác vẽ từ file
/*!
 * \param file File chứa danh sách các thao tác
 * \return tải thành công hay thất bại
 * \sa save()
 */
bool Player::load (QFile &file)
{
    QDataStream in (&file);

    // Check magic number
    quint32 magic;

    in >> magic;
    if (magic != 0x0A0B0C0E && magic != 0x0A0B0C0D)
        return false;

    // Check QDataStream version
    qint32 version;

    in >> version;
    if (version <= 110)
        in.setVersion (QDataStream::Qt_3_1);
    else
        in.setVersion (QDataStream::Qt_4_0);

    //
    QSize tmpSize;
    QColor tmpBackground;

    in >> tmpSize;
    in >> tmpBackground;

    //
    quint32 numOfOperations;

    in >> numOfOperations;

    OpPairList tmpOpPairList = std::move (opPairList);

    qint32 type;

    //    OperationPointer op;
    PaintOperation *op;
    bool isSuccessful = true;

    blockSignals (true);

    for (quint32 i = 0; i != numOfOperations; ++i) {
        in >> type;
        switch (type) {

        case AddShape::Type: op = new AddShape(); break;
        case ChangeShape::Type: op = new ChangeShape(); break;
        case SetPen::Type: op = new SetPen(); break;
        case SetBrush::Type: op = new SetBrush(); break;
        case Erase::Type: op = new Erase(); break;
        case Fill::Type: op = new Fill(); break;
        case AddImage::Type: op = new AddImage(); break;
        case MousePress::Type: op = new MousePress(); break;
        case SetRenderHint::Type: op = new SetRenderHint(); break;
        default:
            isSuccessful = false;
            goto end_read;
        }

        // Tương thích định dạng cũ
        if (magic == 0x0A0B0C0D && type != ChangeShape::Type 
                && !opPairList.empty()) 
        {
            auto &lastPair = opPairList.back();
            if (lastPair.ops.back()->type() == ChangeShape::Type)
                lastPair.ops.push_back (lastPair.effOp);
        }

        op->load (in);
        addOperation (op);
    }

    blockSignals (false);

end_read:
    if (isSuccessful) {
        originalSize = tmpSize;
        __background = tmpBackground;
        delete buffer;

        buffer = new QImage (originalSize * zoomFactor,
                             QImage::Format_ARGB32_Premultiplied);
        emit canUndoChanged (!opPairList.empty());
        emit canRedoChanged (false);
    } else {
        opPairList.swap (tmpOpPairList);
    }


    return isSuccessful;
}

//! Tải ảnh
/*!
 * Tất cả các thao tác hiện có sẽ bị xoá và thay thế bằng ảnh mới. Kích thước
 * của Player cũng thay đổi bằng kích thước bức ảnh
 * \param image
 */
void Player::loadImage (const QImage &image)
{
    clearAll();
    originalSize = image.size();
    buffer = new QImage (originalSize * zoomFactor, image.format());
    AddImage *ai = new AddImage (image, QPointF (0, 0));

    addOperation (ai);
}


// Accessor methods
//

//! Thêm một thao tác vẽ
/*!
 * \param operation thao tác sẽ thêm vào
 * \sa addFinishChangeShape()
 */
void Player::addOperation (PaintOperation *operation)
{
    OperationPointer op (operation);

    if (op->type() == ChangeShape::Type) {
        auto cs = std::static_pointer_cast<ChangeShape>(op);

//        cs->setChangedPart (changedPart);
//        cs->setLastTopLeft (&lastTopLeft);
        OperationPointer addShapePointer (cs->getAddShape());

        opPairList.back().effOp = addShapePointer;
        opPairList.back().ops.push_back (std::move (op));

    } else if (op->type() == AddShape::Type) {
        auto &lastPair = opPairList.back();

        // Nếu trước op là một chuỗi ChangeShape chưa kết thúc
        if (lastPair.ops.back()->type() == ChangeShape::Type) {
            // op sẽ là thao tác kết thúc
            lastPair.ops.push_back(std::move (op));
            return;;
        } 


        // Nếu không thì op thuộc về một chuỗi AddShape
        //

        if (lastPair.ops.back()->type() != AddShape::Type)
            lastPair.effOp.reset (new AddShape());

        std::static_pointer_cast<AddShape>(lastPair.effOp)
            ->append (*std::static_pointer_cast<AddShape>(op));
        lastPair.ops.push_back (std::move (op));
    } else {
#if defined(__clang__) && __clang_major__ == 3 && __clang_minor__ <=1
        std::deque<OperationPointer> d;
        d.push_back (op);
        opPairList.push_back (std::move ( OpPair { op, d } ));
#else
        opPairList.push_back (std::move ( OpPair { op, { op } }));
#endif
    }

    emit playerModified (true);

    undoStack.clear();
    emit canUndoChanged (true);
    emit canRedoChanged (false);
}

//! Thêm thao tác kết thúc chuỗi các thao tác ChangeShape
/*!
 * \return thao tác đánh dấu cho sự kết thúc của chuỗi thao tác ChangeShape
 * \sa addOperation()
 */
PaintOperation* Player::addFinishChangeShape()
{
    auto &lastPair = opPairList.back();
    lastPair.ops.push_back (lastPair.effOp);
    return lastPair.effOp.get();
}

// Other methods
//
