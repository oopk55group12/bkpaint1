
/************************************************************************
        canvas.cc

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#include "canvas.hh"
#include <QPainter>
#include <QImage>
#include <QPicture>
#include <QMouseEvent>

#include <utility>
#include <cstdlib>
#include <cmath>

#include "player.hh"
#include "paintoperation/paintoperation"

#include <QDebug>

// Static members init
//

QCursor Canvas::cursors[ModeCount];

// Constructors/Destructors
//

//! Phương thức khởi tạo
/*!
 * \param player
 * \param parent
 * \sa player
 */
Canvas::Canvas (Player *player, QWidget *parent /* = 0 */)
    : QWidget (parent), player (player)
{
    //    ui->setupUi (this);
    setBackgroundRole (QPalette::Base);

    zoomFactor = 1.0;

    if (player->getBuffer()) {
        resize (player->size());
        buffer = new QImage (player->size(), 
                            QImage::Format_ARGB32_Premultiplied);
        buffer->fill (player->background());
        painter = new QPainter (buffer);
        painter->setBackground (player->background());

        player->repaint(*painter);
    } else {
        buffer = new QImage (QSize (1, 1),
                             QImage::Format_ARGB32_Premultiplied);
        painter = new QPainter(buffer);
    }

    connect (player, SIGNAL (playerStarted()), SLOT (startPlayer()));
    connect (player, SIGNAL (playerStopped()), SLOT (stopPlayer()));

    connect (player, SIGNAL (nextOperation(QRectF const &)), 
			 SLOT (playNextOp(QRectF const &)));

    mousePressed = drawStarted = false;

    setMouseTracking (true);
}

//! Phương thức hủy
Canvas::~Canvas()
{
    painter->end();
    delete painter;

    //    delete ui;
    delete buffer;
}


// Static Public Methods
//

//! Khởi tạo các con trỏ chuột
/*!
 * \sa cursors
 */
void Canvas::initCursors()
{
    QString cursorPath = ":/cursor/Images/CursorSet/";

    cursors[FreeDraw] = QCursor (cursorPath + "PencilToolCursor.png");
    cursors[DrawLine] = QCursor (cursorPath + "LineToolCursor.png");
    cursors[DrawEllipse] = QCursor (cursorPath + "EllipseToolCursor.png");
    cursors[DrawPolygon] = QCursor (cursorPath + "PolygonToolCursor.png");
    cursors[DrawStar] = QCursor (cursorPath + "StarToolCursor.png");
    cursors[DrawRegularPolygon] = 
                    QCursor (cursorPath + "RegularPolygonToolCursor.png");
    cursors[DrawCurve] = QCursor (cursorPath + "CurveToolCursor.png");
    cursors[DrawRect] = QCursor (cursorPath + "RectangleToolCursor.png");
    cursors[FillArea] = QCursor (cursorPath + "PaintBucketToolCursor.png");
    cursors[Replay] = QCursor (Qt::ArrowCursor);
}


//! Sinh con trỏ chuột cho chế đô tẩy (DrawMode::EraseArea)
/*!
 * \param radius bán kính của con trỏ chuột (cũng là bán kính phần sẽ bị tẩy
 * \sa DrawMode::EraseArea
 */
QCursor Canvas::genEraseCursor (qreal radius)
{
    QPixmap pixmap (2 * radius + 1, 2 * radius + 1);

    pixmap.fill (Qt::transparent);

    QPainter painter (&pixmap);

    painter.drawEllipse (0, 0, 2 * radius, 2 * radius);

    if (radius >= 6) {
        painter.drawLine (radius, radius*2/3, radius, radius*9/10);
        painter.drawLine (radius, radius*11/10 + 1, radius, radius*4/3 + 1);
        painter.drawLine (radius*2/3, radius, radius*9/10, radius);
        painter.drawLine (radius*11/10 + 1, radius, radius*4/3 + 1, radius);
    }

    painter.end();
    return QCursor (pixmap);
}


//! Tính các đỉnh của đa giác đều
/*!
 * \param n số đỉnh của đa giác
 * \param centre tâm của đa giác
 * \param point một đỉnh của của đa giác
 * \return đa giác đều n cạnh có tâm là centre
 */
QPolygonF 
Canvas::regularPolygon (int n, QPointF const &centre, QPointF const &point)
{
    QPolygonF polygon (n+1);
    polygon[0] = polygon[n] = point;

    const qreal alpha = 8 * atan (1.0) / n; // = 2 * PI / n
    const qreal cosa = std::cos (alpha);
    const qreal sina = std::sin (alpha);

    qreal x = point.x() - centre.x(), y = point.y() - centre.y();
    for (int i = 1; i < n; ++i) {
        // Xoay vector (x, y) theo chiều kim đồng hồ một góc alpha
        qreal tmpx = x * cosa + y * sina;
        y = -x * sina + y * cosa;
        x = tmpx;

        polygon[i] = centre + QPointF (x, y);
    }

    return polygon;
}


//! Tính các đỉnh của hình sao
/*!
 * \param n số đỉnh của đa giá đều bao hình sao
 * \param centre tâm của hình sao
 * \param point một đỉnh nhọn của hình sao
 * \return hình sao có n đỉnh nhọn, tâm là centre
 */
QPolygonF
Canvas::regularStar (int n, QPointF const &centre, QPointF const &point)
{
    static const qreal spokeRatio = 0.5;

    QPolygonF star (2*n+1);
    star[0] = star[2*n] = point;

    const qreal alpha = 8 * atan (1.0) / n; // = 2 * PI / n
    const qreal halpha = 4 * atan (1.0) / n; // = half alpha
    const qreal cosa = std::cos (alpha);
    const qreal cosah = std::cos (halpha);
    const qreal sina = std::sin (alpha);
    const qreal sinah = std::sin (halpha);

    qreal x = point.x() - centre.x(), y = point.y() - centre.y();
    for (int i = 1; i < 2*n-1; i+=2) {
        // Đỉnh tù
        qreal x1 = spokeRatio * (x * cosah + y * sinah);
        qreal y1 = spokeRatio * (y * cosah - x * sinah);
        star[i] = centre + QPoint (x1, y1);

        // Đỉnh nhọn
        x1 = x * cosa + y * sina;
        y = y * cosa - x * sina;
        x = x1;
        star[i+1] = centre + QPoint (x, y);
    }

    qreal x1 = spokeRatio * (x * cosah + y * sinah);
    qreal y1 = spokeRatio * (y * cosah - x * sinah);
    star[2*n-1] = centre + QPoint (x1, y1);

    return star;
}


// Public methods
//

//! Tải nội dung file ảnh lên bức vẽ
/*!
 * \param filename file ảnh
 * \sa save()
 */
bool Canvas::load (QString const &filename)
{
    QImage image (filename);

    if (image.isNull()) {
        return false;
    } else {
        if (painter->device())
            painter->end();
        delete buffer;

        buffer = new QImage (
                std::move (image.scaled (image.size() * zoomFactor)));

        painter->begin (buffer);
        painter->scale (zoomFactor, zoomFactor);
        player->loadImage (image);
        resize (buffer->size());
        return true;
    }
}


//! Cập nhật sự thay đổi của player
/*!
 * #buffer sẽ được vẽ lại theo nội dung mới của #player.
 * \sa player
 */
void Canvas::reloadPlayer()
{
    regenerateBuffer();
    player->repaint (*painter);
    resize (player->size() * zoomFactor);

    repaint();
}


// Public accessor methods
//

//! Thay đổi chế độ vẽ
/*!
 * Các hình vẽ chưa được vẽ xong (như đa giác, đường cong) sẽ được kết thúc
 * trước khi chuyển sang chế độ vẽ mới.
 * \param m chế độ vẽ mới
 * \sa mode
 */
void Canvas::setMode (DrawMode m)
{
    
    if (drawStarted && mode != m) {
        drawStarted = false;
        operate (player->addFinishChangeShape());
    }

    mode = m;
    updateCursor();
}


// Public Slots
//

//! Thay đổi bút vẽ
/*!
 * Thêm một đối tượng SetPen vào #player
 * \param pen bút vẽ mới
 * \sa SetPen
 */
void Canvas::setPen (QPen const &pen)
{
    SetPen *sp = new SetPen (pen);

    player->addOperation (sp);
    sp->operate (*painter);
}

//! Thay đổi chổi lông
/*!
 * Thêm một đối tượng SetBrush vào #player
 * \param brush chổi lông mới
 * \sa SetBrush
 */
void Canvas::setBrush (QBrush const &brush)
{
    SetBrush *sb = new SetBrush (brush);

    player->addOperation (sb);
    sb->operate (*painter);
}

//! Thay đổi renderHint
/*!
 * Thêm một đối tượng SetRenderHint vào #player
 * \param hint 
 * \param on mở hay tắt
 * \sa SetRenderHint
 */
void Canvas::setRenderHint (QPainter::RenderHint hint, bool on)
{
    SetRenderHint *sh = new SetRenderHint (hint, on);

    player->addOperation (sh);
    sh->operate (*painter);
}

//! Thay đổi số đỉnh của đa giác đều hoặc ngôi sao
/*!
 * \param _corner số đỉnh
 * \sa corner
 */
void Canvas::setNoCorner (int _corner)
{
    corner = _corner;
}

//! Thay đổi bán kính vùng tẩy
/*!
 * \param radius bán kính
 * \sa eraseRadius
 */
void Canvas::setEraseRadius (int radius)
{
    eraseRadius = radius;
    updateCursor();
}

//! Thay đổi tỉ lệ phóng to/thu nhỏ
/*!
 * Nếu #mode != Replay, #buffer sẽ được vẽ lại từ đầu với tỉ lệ mới. Ngược
 * lại buffer chỉ thay đổi kích thước.
 * \param percent tỉ lệ (tính bằng phần trăm)
 * \sa zoomFactor
 */
void Canvas::setZoomFactor (int percent)
{
    if (zoomFactor != percent / 100.0) {
        old_zoomFactor = zoomFactor;
        zoomFactor = percent / 100.0;
        regenerateBuffer();
        player->zoom (zoomFactor);
        if (mode != Replay) {
            player->repaint (*painter);
            updateCursor();
        }
        resize (player->size() * zoomFactor);
    }
}

//! Trả lại trạng thái trước khi thực hiện thao tác gần nhất
/*!
 * \sa redo()
 */
void Canvas::undo()
{
    player->undo();
    painter->setPen (QPen());
    painter->setBrush (QBrush());
    painter->setRenderHints (painter->renderHints(), false);
    painter->setBackground (player->background());
    buffer->fill (player->background());
    player->repaint (*painter);
    repaint();
}


//! Thực hiện lại thao tác vừa undo
/*!
 * \sa undo()
 */
void Canvas::redo()
{
    player->redo (*painter);
    repaint();
}


//
// Event handlers
//

//! Sự kiện vẽ lại
/*!
 * Vẽ lại phần bị thay đổi từ buffer lên canvas. Nếu ở chế độ #Replay thì sẽ sử
 * dụng buffer của #player.
 * \param e sự kiện vẽ lại
 */
void Canvas::paintEvent (QPaintEvent *e)
{
    QPainter tmpPainter (this);

    QRect rect = visibleRegion().boundingRect() & e->rect();
    fillBackground (tmpPainter, rect);

    if (mode == Replay)
        tmpPainter.drawImage (rect, *player->getBuffer(), rect);
    else
        tmpPainter.drawImage (rect, *buffer, rect);
}


//! Sự kiện nhấn chuột
/*!
 * \param event sự kiện chuột
 * \sa mouseMoveEvent(), mouseReleaseEvent()
 */
void Canvas::mousePressEvent (QMouseEvent *event)
{
    if (mode == Replay)
        return;
    
    QPointF posF = event->posF() / zoomFactor;

    if (event->button() == Qt::RightButton) {
        if (mode == DrawPolygon) {
            // Đóng hình đa giác
            QPainterPath line (lastPoint);
            line.lineTo (startPoint);
            AddShape *as = new AddShape (line);
            player->addOperation (as);
//            operate (as);

            // Kết thúc vẽ đa giác
            drawStarted = false;
            operate (player->addFinishChangeShape());
        } else if (mode == DrawCurve) {
            // Kết thúc vẽ đương cong
            drawStarted = false;
            operate (player->addFinishChangeShape());
        }

        return;
    }

    if (event->button() != Qt::LeftButton)
        return;


    PaintOperation *op = nullptr;

    mousePressed = true;

    switch (mode) {
    case FreeDraw:
    case DrawLine:
    case DrawRect:
    case DrawEllipse:
    case DrawRegularPolygon:
    case DrawStar:
        startPoint = lastPoint = posF;
        op = new MousePress ();
        drawStarted = true;
        break;
    case DrawPolygon:
        if (drawStarted) {
            // Nếu đã bắt đầu vẽ hình đa giác, điểm posF sẽ là đỉnh tiếp theo

            MousePress *mp = new MousePress ();
            player->addOperation (mp);
            mp->operate (*painter);

            QPainterPath line (lastPoint);
            line.lineTo (posF);
            op = new ChangeShape (line);
        } else {
            // Nếu chưa thì bắt đầu vẽ 

            drawStarted = true;
            startPoint = lastPoint = posF;
            op = new MousePress ();
        }
        break;
    case DrawCurve:
        if (!drawStarted) {
            // Nếu chưa bắt đầu vẽ đường cong

            startPoint = lastPoint = posF;
            drawStarted = true;
            controlPoint1 = controlPoint2 = QPointF (-1, -1);
            op = new MousePress ();

        } else {
            QPainterPath curve (startPoint);

            if (controlPoint1.x() < 0) {
                // Nếu chưa chọn controlPoint1

                controlPoint1 = posF;
                curve.quadTo (controlPoint1, lastPoint);
            } else {
                // Nếu chưa chọn controlPoint2 

                controlPoint2 = posF;
                curve.cubicTo (controlPoint1, controlPoint2, lastPoint);
            }

            op = new ChangeShape (curve);
        }
        break;
    case EraseArea:
        op = new Erase (posF, eraseRadius);
        drawStarted = true;
        break;
    case FillArea:
        op = new Fill (posF.toPoint());
        break;
    default:
        ;
    }

    if (op) {
        player->addOperation (op);
        operate (op);
    }
}


//! Sự kiện di chuyển chuột
/*!
 * \param event sự kiện chuột
 * \sa mousePressEvent(), mouseReleaseEvent()
 */
void Canvas::mouseMoveEvent (QMouseEvent *event)
{
    QPointF posF = event->posF() / zoomFactor;
    emit cursorMoved (posF);

    if (!drawStarted)
        return;

    PaintOperation *op = nullptr;
    QPainterPath path;

    if (mode == FreeDraw) {
        path.moveTo (lastPoint);
        path.lineTo (lastPoint = posF);
        op = new AddShape (path);
    } else {
        bool shiftModifier = event->modifiers() & Qt::ShiftModifier;
        static const double sqrt2 = std::sqrt (2.);
        static const double PI = 4 * atan (1.0);

        if (shiftModifier && mode != EraseArea) {
            auto dx = posF.x() - startPoint.x();
            auto dy = posF.y() - startPoint.y();
            auto distance = std::sqrt (dx * dx + dy * dy);

            auto angle = atan2 (dy, dx) * 180 / PI;

            posF = startPoint;

            if ( (angle < -157.5) || (angle >= 157.5))
                posF += { -distance, 0};
            else if (angle < -112.5)
                posF += { -distance / sqrt2, -distance / sqrt2};
            else if (angle < -67.5)
                posF += { 0, -distance};
            else if (angle < -22.5)
                posF += { distance / sqrt2, -distance / sqrt2};
            else if (angle < 22.5)
                posF += { distance, 0};
            else if (angle < 67.5)
                posF += { distance / sqrt2, distance / sqrt2};
            else if (angle < 112.5)
                posF += { 0, distance};
            else
                posF += { -distance / sqrt2, distance / sqrt2};
        } 
        
        switch (mode) {
        case DrawLine:
            path.moveTo (startPoint);
            path.lineTo (posF);
            break;
        case DrawPolygon:
            if (mousePressed) {
                path.moveTo (lastPoint);
                path.lineTo (posF);
            }
            break;
        case DrawRect:
        case DrawEllipse: 
            {
                qreal x = std::min (startPoint.x(), posF.x());
                qreal y = std::min (startPoint.y(), posF.y());
                qreal w = std::abs (posF.x() - startPoint.x());
                qreal h = std::abs (posF.y() - startPoint.y());

                if (shiftModifier)
                    w = h = std::max (w, h);

                if (mode == DrawRect)
                    path.addRect (x, y, w, h);
                else
                    path.addEllipse (x, y, w, h);
            }
            break;
        case DrawRegularPolygon:
            path.addPolygon (regularPolygon (corner, 
                        (startPoint + posF) / 2, posF));
            break;
        case DrawStar:
            path.addPolygon (regularStar (corner, 
                        (startPoint + posF) / 2, posF));
            break;
        case DrawCurve:
            if (mousePressed) {
                if (controlPoint1.x() < 0) {
                    // Nếu chưa chọn được controlPoint1 thì vẽ đương cơ sở
                    path.moveTo (startPoint);
                    path.lineTo (lastPoint = posF);
                } else if (controlPoint2.x() < 0) {
                    // Nếu đã chọn được controlPoint1 nhưng chưa chọn được
                    // controlPoint2 thì sử đường cong bậc 2
                    path.moveTo (startPoint);
                    path.quadTo (controlPoint1 = posF, lastPoint);
                } else {
                    // Nếu đã chọn được controlPoint2 thì vẽ đương cong bậc 3
                    path.moveTo (startPoint);
                    path.cubicTo (controlPoint1, controlPoint2 = posF, lastPoint);
                }
            }
            break;
        case EraseArea:
            op = new Erase (posF, eraseRadius);
            break;
        default:
            break;
        }

        if (!path.isEmpty())
            op = new ChangeShape (path);

    }

    if (op) {
        player->addOperation (op);
        operate (op);
    }
}


//! Sự kiện nhả chuột
/*!
 * \param event sự kiện chuột
 * \sa mousePressEvent(), mouseMoveEvent()
 */
void Canvas::mouseReleaseEvent (QMouseEvent *event)
{
    Q_UNUSED (event)
    
    QPointF posF = event->posF() / zoomFactor;

    mousePressed = false;

    switch (mode) {
        case DrawPolygon:
            if (drawStarted) {
                lastPoint = posF;
                operate (player->addFinishChangeShape());
            }
            break;
        case DrawCurve:
            // Nếu đã chọn được controlPoint2 thì kết thúc vẽ
            if (controlPoint2.x() >= 0) {
                drawStarted = false;
                operate (player->addFinishChangeShape());
            }
            break;
        case FreeDraw:
        case EraseArea:
        case FillArea:
            drawStarted = false;
            break;
        default:
            drawStarted = false;
            operate (player->addFinishChangeShape());
    }
}


// Private methods
//

//! Thực hiện thao tác
/*!
 * ...
 * \param po thao tác vẽ
 */
void Canvas::operate (PaintOperation *po)
{
    QRectF opChangeRect = po->operate (*painter);
    QRect canvasChangeRect = QRectF (opChangeRect.topLeft() * zoomFactor,
                                     opChangeRect.size() * zoomFactor)
                            .toRect();
    repaint (canvasChangeRect);
}	


//! Cập nhật con trỏ chuột
/*!
 * \sa cursors
 */
void Canvas::updateCursor()
{
    if (mode == EraseArea)
        cursors[EraseArea] = genEraseCursor (eraseRadius * zoomFactor);
    setCursor (cursors[mode]);
}


//! Vẽ background 
/*!
 * \param bgPainter đối tượng vẽ 
 * \param rect vùng cần vẽ background
 */
void Canvas::fillBackground (QPainter &bgPainter, QRect const &rect)
{
    static const QColor color0 = QColor (100,100,100);
    static const QColor color1 = QColor (150,150,150);
    static const int sqSize = 20;

    if (player->background() != Qt::transparent) {
        bgPainter.fillRect (rect, player->background());
        return;
    }

    int x0 = rect.x() - rect.x() % (2*sqSize);
    int y0 = rect.y() - rect.y() % (2*sqSize);
    int width = rect.width() + (4*sqSize - rect.width()%(2*sqSize));
    int height = rect.height() + (4*sqSize - rect.height()%(2*sqSize));
    
    for (int y = y0; y < y0+height; y += 2*sqSize) {
        for (int x = x0; x < x0+width; x += 2*sqSize) {
            bgPainter.fillRect (x, y, sqSize, sqSize, color0);
            bgPainter.fillRect (x+sqSize, y, sqSize, sqSize, color1);
            bgPainter.fillRect (x, y+sqSize, sqSize, sqSize, color1);
            bgPainter.fillRect (x+sqSize, y+sqSize, sqSize, sqSize, color0);
        }
    }

}


//! Khởi tạo buffer mới
/*!
 * #buffer mới và #painter sẽ được đưa về mặc định. Sẵn sàng cho việc bắt đầu
 * vẽ lại.
 * \sa buffer, painter 
 */
void Canvas::regenerateBuffer()
{
    painter->end();
    *buffer = QImage (player->size() * zoomFactor, buffer->format());
    buffer->fill (player->background());
    painter->begin (buffer);
    painter->setBackground (player->background());
    painter->scale (zoomFactor, zoomFactor);
}


// Private slots
//


//! Khởi động chế độ replay ở canvas
/*!
 * Được gọi khi player bắt đầu chế độ replay
 */
void Canvas::startPlayer()
{
    if (mode != Replay)
        old_mode = mode;
    mode = Replay;
    setCursor (cursors[Replay]);
    repaint();
}

//! Kết thúc chế độ replay
/*!
 * Được gọi khi player kết thúc chế độ replay.
 * Vẽ lại buffer nếu trong quá trình replay có thay đổi tỉ lệ phóng to / thu
 * nhỏ
 * \sa player, buffer, zoomFactor, old_zoomFactor
 */
void Canvas::stopPlayer()
{
    setMode (old_mode);

    // Nếu trong thời gian replay có zoom thì vẽ lại buffer
    if (old_zoomFactor != zoomFactor) {
        old_zoomFactor = zoomFactor;
        player->repaint (*painter);
    }

    repaint();
}

//! Thực hiện thao tác vẽ tiếp theo ở chế độ Replay
/*!
 * Vẽ lại phần bị thay đổi bởi thao tác vẽ mới nhất lên canvas.
 * Được gọi đến khi #player đã chuyển sang thao tác vẽ tiếp theo.
 * \param rect vùng bị thay đổi
 * \sa player, Player::nextOperation()
 */
void Canvas::playNextOp (QRectF const &rect)
{
    QRect canvasChangeRect = QRectF (rect.topLeft() * zoomFactor,
                                     rect.size() * zoomFactor)
                            .toRect();
    repaint (canvasChangeRect);
}

