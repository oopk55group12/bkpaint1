
/************************************************************************
        mainwindow.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/

#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QMainWindow>
#include <QFlags>

class QActionGroup;
class QSlider;
class QSignalMapper;
class QScrollArea;
class QSpinBox;
class QLabel;
class QFileSystemWatcher;
class QTimer;
class Canvas;
class Player;
class ColorPicker;
class NewFileOption;
class SetPenToolBar;
class SetBrushToolBar;
class ToolBar;

//! Cửa số chính.
/*!
 * Đây là lớp chứa mọi thành phần điều khiển, giao diện của chương trình.
 * Chương trình nhận các thao tác từ người dùng qua MainWindow.
 */
class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    //! Kiểu của file đầu vào
    /*!
     * \sa MainWindow (QString const &filename, FileTypes = Default)
     */
    enum FileType {
        Default = 0x0,  //!< Mặc định, là một file Replay bình thường
        Sample = 0x1,   //!< Là file mẫu của chương trình
        Graphic = 0x2   //!< Là file hình ảnh
    };

    Q_DECLARE_FLAGS (FileTypes, FileType)

public:
    //! Phương thức khởi tạo. Tạo file mới
    MainWindow (NewFileOption const *nfOption);

    //! Phương thức khởi tạo. Mở file
    MainWindow (QString const &filename, FileTypes = Default);

    //! Phương thức hủy
    ~MainWindow();

protected:
    //! Xử lý sự kiện đóng cửa sổ
    void closeEvent (QCloseEvent *event) override;

private slots:
    // File slots

    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    void on_actionOpenSample_triggered();
    bool on_actionSave_triggered();
    bool on_actionSave_As_triggered();
    void openRecentFile();
    void reloadCurrentFile();
    void on_actionImport_triggered();
    bool on_actionExport_triggered();


    // Edit slots

    void on_actionUndo_triggered();
    void on_actionRedo_triggered();
    void on_actionResize_triggered();


    // Draw mode slots

    void on_actionLine_triggered();
    void on_actionRectangle_triggered();
    void on_actionFreeDraw_triggered();
    void on_actionEllipse_triggered();
    void on_actionPolygon_triggered();
    void on_actionCurve_triggered();
    void on_actionFill_triggered();
    void on_actionErase_triggered();
    void on_actionRegularPolygon_triggered();
    void on_actionStar_triggered();


    // Replay slots

    void on_actionReplay_triggered();
    void on_actionPause_triggered();
    void on_actionResume_triggered();
    void on_actionStop_triggered();

    //! #player đã kết thúc Replay
    void playerStopped();

    //! #player đã thay đổi trạng thái chỉnh sửa hay không
    void playerModified (bool b);


    // Zoom slots
    //

    void on_actionOriginal_size_triggered();
    void on_actionZoom_2_1_triggered();
    void on_actionZoom_1_2_triggered();
    void on_actionZoom_In_triggered();
    void on_actionZoom_Out_triggered();


    // Set pen slots
    //

    //! Mở hộp thoại chọn màu nét vẽ và thay đổi theo màu mới chọn
    void openPenColorDialog();

    //! Thay đổi độ rộng nét vẽ
    void setPenWidth (int value);

    //! Thay đổi kiểu nét vẽ
    void setPenStyle (int id);

    //! Thay đổi màu nét vẽ
    void setPenColor (QColor color);

    //! Thay đổi RenderHint
    void setRenderHint (bool on);


    // Set brush slots
    //

    //! Mở hộp thoại chọn màu chổi lông và thay đổi theo màu mới chọn
    void openBrushColorDialog();
    
    //! Thay đổi kiểu chổi lông
    void setBrushStyle (int id);

    //! Thay đổi màu chổi lông
    void setBrushColor (QColor color);


    // Help slots
    void on_actionAbout_triggered();
    void on_actionManual_triggered();


    // cursor moved

    //! Cập nhật vị trí mới của con trỏ chuột lên #labelPosition
    void updateLabelPosition (QPointF);

    // curent file modified outside
//    void fileChanged (QString const &path);
//    void unblockFileWatcher();

    // Other

    //! Cập nhật thông tin về màu đang chọn trên #colorPicker
    void hoveredColor (QString const &colorName);

private:
    //! Khởi tạo các thuộc tính của MainWindow
    void init();

    //! Đọc và thực hiện từ file cấu hình
    void readSettings();
    //! Ghi vào file cấu hình
    void writeSettings();

    //! Khởi tạo các widget
    void initWidgets();
    //! Khởi tạo các action
    void initActions();
    //! Khởi tạo thanh trạng thái
    void initStatusBar();
    //! Khởi tạo các thanh công cụ
    void initToolbars();
    //! Khởi tạo các menu
    void initMenus();

    //! Ghi nội dung bức vẽ vào file ảnh
    bool saveGraphicFile (QString const &filename);
    //! Ghi danh sách các thao tác vẽ vào file Replay
    bool saveReplayFile (QString const &filename);

    //! Tải nội dung từ file
    void loadFile (QString const &filename, FileTypes ft = Default);

    //! Thay đổi file hiện tại
    void setCurrentFile (QString const &filename);
    //! File hiện tại có thể lưu hay không ?
    bool maybeSave();

    //! Cập nhật danh sách các file gần nhất
    void updateRecentFileActions();

    //    static MainWindow* findMainWindow(QString const &filename);

    //! Lấy tên file (không kèm theo đường dẫn đến thư mục)
    static QString strippedName (QString const &fullFileName);

    //! Cập nhật các thành phần điểu khiển
    void reloadControl();

    //! Mở / vô hiệu hoá các thành phần điểu khiển liên quan đến việc vẽ
    void enablePainting (bool);

private:
    //    Ui::MainWindow *ui;

    // File actions

    //! Hành động tạo file mới
    QAction *actionNew;

    //! Hành động mở file
    QAction *actionOpen;

    //! Hành động mở file mẫu
    QAction *actionOpenSample;

    //! Hành động lưu file
    QAction *actionSave;

    //! Hành động lưu file với tên khác #curFile
    QAction *actionSave_As;

    //! Hành động xuất ra file ảnh
    QAction *actionExport;

    //! Hành động nhập vào file ảnh
    QAction *actionImport;

    //! Hành động tải lại file
    QAction *actionReload;

    //! Hành động đóng cửa sổ
    QAction *actionExit;

    enum { 
        MaxRecentFiles = 5 //!< Số file gần đây nhiều nhất có thể hiển thị
    };

    //! Hành động mở file gần đây
    QAction *recentFileActions[MaxRecentFiles];


    // Edit actions
    //

    //! Hành động undo
    QAction *actionUndo;

    //! Hành động redo
    QAction *actionRedo;

    //! Hành động thay đổi kích thước
    QAction *actionResize;


    // Draw mode actions
    //

    //! Nhóm các hành động chọn chế độ vẽ
    QActionGroup *drawModeActionGroup;

    //! Hành động chọn chế độ vẽ đường thẳng
    QAction *actionLine;

    //! Hành động chọn chế độ vẽ hình chữ nhật
    QAction *actionRectangle;

    //! Hành động chọn chế độ vẽ tự do
    QAction *actionFreeDraw;

    //! Hành động chọn chế độ Replay
    QAction *actionReplay;

    //! Hành động chọn chế độ vẽ Elip
    QAction *actionEllipse;

    //! Hành động chọn chế độ vẽ đa giác
    QAction *actionPolygon;

    //! Hành động chọn chế độ vẽ đường cong Bezier
    QAction *actionCurve;

    //! Menu chọn chế độ vẽ đa giác đều hoặc hình sao
    QMenu *menuRegularPolygonAndStar;

    //! Hành động chọn chế độ vẽ đa giác đều
    QAction *actionRegularPolygon;

    //! Hành động chọn chế độ vẽ hình sao
    QAction *actionStar;

    //! Hành động chọn chế độ tô màu
    QAction *actionFill;

    //! Hành động chọn chế độ tẩy
    QAction *actionErase;

    //! Thanh trượt quy định bán kính vùng bị tẩy
    QSlider *eraseRadiusSlider;

    //! Quy định số góc của đa giác đều (hay số góc nhọn của hình sao)
    QSpinBox *regularPolygonNoCornerSpinBox;


    // Zoom actions
    //
    
    //! Hành động trở về kích thước bình thường
    QAction *actionOriginal_size;

    //! Hành động zoom với tỉ lệ 2:1
    QAction *actionZoom_2_1;

    //! Hành động zoom với tỉ lệ 1:2
    QAction *actionZoom_1_2;

    //! Hành động phóng to
    QAction *actionZoom_In;

    //! Hành động thu nhỏ
    QAction *actionZoom_Out;


    // Replay actions
    //

    //! Nhóm các hành động phục vụ cho việc Replay
    QActionGroup *replayActionGroup;

    //! Thanh trượt quy định tốc độ Replay
    QSlider *replaySpeedSlider;

    //! Hành động tạm dừng Replay
    QAction *actionPause;

    //! Hành động kết thúc Replay
    QAction *actionStop;

    //! Hành động tiếp tục Replay sau khi pause
    QAction *actionResume;


    // Set renderhint actions
    //

    //! Hành động chọn chọn chế độ khử răng cưa
    /*!
     * \sa \qt{qpainter.html#RenderHint-enum, QPainter::Antialiasing}
     */
    QAction *actionAntialiasing;

    //! Hành động chọn chế độ khử răng cưa cho text
    /*!
     * \sa \qt{qpainter.html#RenderHint-enum, QPainter::TextAntialiasing}
     */
    QAction *actionTextAntialiasing;

    //! Hành động chọn chọn chế độ SmoothPixmapTransform
    /*!
     * \sa \qt{qpainter.html#RenderHint-enum, QPainter::SmoothPixmapTransform}
     */
    QAction *actionSmoothPixmapTransform;

    //! Hành động chọn chọn chế độ khử răng cưa bằng openGL
    /*!
     * \sa \qt{qpainter.html#RenderHint-enum, QPainter::HighQualityAntialiasing}
     */
    QAction *actionHighQualityAntialiasing;

    //! Hành động chọn chọn chế độ NonCosmeticDefaultPen
    /*!
     * \sa \qt{qpainter.html#RenderHint-enum, QPainter::NonCosmeticDefaultPen}
     */
    QAction *actionNonCosmeticDefaultPen;

    //! Menu chọn RenderHint
    QMenu *renderHintMenu;

    //! Nhóm các hành động chọn RenderHint
    QActionGroup *renderHintActionGroup;


    // Help actions
    //

    //! Hành động xem hướng dẫn
    QAction *actionManual;

    //! Hành động xem thông tin chương trình
    QAction *actionAbout;

    //! Hành động xem thông tin về Qt
    QAction *actionAboutQt;


    // Menu
    //

    //! Thanh menu
    /*!
     * Ở trên MAC OSX thì menuBar không phải con của MainWindow
     */
    QMenuBar *menuBar;

    //! Menu thao tác với file
    QMenu *menuFile;

    //! Menu chọn chế độ vẽ
    QMenu *menuDraw;

    //! Menu thay đổi cách hiển thị
    QMenu *menuView;

    //! Menu zoom
    QMenu *menuZoom;

    //! Menu ẩn / hiện các thanh công cụ
    QMenu *menuToolbar;

    //! Menu phục vụ việc Replay
    QMenu *menuReplay;

    //! Menu chỉnh sửa file
    QMenu *menuEdit;

    //! Menu trợ giúp
    QMenu *menuHelp;


    // ToolBar
    //

    //! Thanh công cụ thao tác với file
    ToolBar *fileToolBar;

    //! Thanh công cụ zoom
    ToolBar *zoomToolBar;

    //! Thanh công cụ chọn chế độ vẽ
    ToolBar *mainToolBar;

    //! Thanh công cụ Replay
    ToolBar *replayToolBar;

    //! Thanh công cụ thay đổi nét vẽ
    SetPenToolBar *setPenToolBar;

    //! Thanh công cụ thay đổi chổi lông
    SetBrushToolBar *setBrushToolBar;

    //! Thanh công cụ chọn màu
    /*!
     * \sa colorPicker
     */
    ToolBar *colorPickerToolBar;

    //! Thanh trạng thái 
    QStatusBar *statusBar;


    // Widgets
    //

    //! Widgets trung tâm
    QWidget *centralWidget;

    //! 
    QScrollArea *scrollArea;

    //! Thanh chọn màu
    /*!
     * \sa colorPickerToolBar
     */
    ColorPicker *colorPicker;

    //! Thay đổi hệ số zoom
    QSpinBox *zoomFactorSpinBox;

    //! Toạ độ con trỏ chuột trên #canvas
    QLabel *labelPosition;


    // Other
    //

    //! Canvas
    Canvas *canvas;

    //! Player
    Player *player;

    //! File hiện tại đã có tên hay chưa
    bool isUntitled;

    //! File hiện tại
    QString curFile;

/*     QFileSystemWatcher *fileWatcher;
 *     QTimer *fileWatcherBlockTimer;
 *     enum { BlockTime = 50 };
 */
};

Q_DECLARE_OPERATORS_FOR_FLAGS (MainWindow::FileTypes)

#endif // MAINWINDOW_HH
