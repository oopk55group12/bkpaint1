/************************************************************************
        player.hh

  Copyright (C) 2012 - boss14420

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

**************************************************************************/


#ifndef PLAYER_HH
#define PLAYER_HH


#include <QObject>

#include <QString>
#include <QPointF>
#include <QFile>
#include <QSize>
#include <QImage>
#include <QPainter>
#include <QTimer>
#include <QSharedPointer>

#include <deque>
#include <memory>

class PaintOperation;
class NewFileOption;

/*! \file */


//! Kiểu con trỏ chia sẻ PaintOperation
typedef std::shared_ptr<PaintOperation> OperationPointer;

//! Danh sách các con trỏ OperationPointer
typedef std::deque<OperationPointer> OperationList;

//! Cặp thao tác vẽ
/*!
 * Bao gồm danh sách các thao tác vẽ liên tiếp nhau (#ops) và một thao tác vẽ
 * có hiệu lực tương đương (#effOp). Thực hiện effOp có tác dụng giống như thực
 * hiện liên tục các thao tác thuộc ops.
 * \sa OperationPointer, OperationList
 */
struct OpPair {
    //! Thao tác vẽ có ích
    OperationPointer effOp;
    //! Danh sách các thao tác vẽ
    OperationList ops;
};

//! Kiểu danh sách các cặp thao tác vẽ
/*!
 * \sa OpPair
 */
typedef std::deque<OpPair> OpPairList;


//! Đối tượng lưu trữ và thực hiện lại các thao tác vẽ
/**
 * Đối tượng Player có thuộc tính #opPairList chứa các cặp thao tác vẽ
 * (OpPair).
 *
 * Mỗi đối tượng Player được gắb với một đối tượng Canvas và một đối tượng
 * MainWindow. Khi người dùng thao tác lên Canvas hoặc các điều khiển của
 * MainWindow thì một thao tác vẽ mới (PaintOperation) sẽ đuợc tạo ra và thêm
 * vào Player thông qua phương thức #addOperation().
 * Nếu một chuỗi các thao tác ChangeShape đã được thêm vào thì nó sẽ được đánh
 * dấu kết thúc bằng phương thức #addFinishChangeShape().
 *
 * Khi replay, lần lượt các thao tác vẽ trong OpPair::ops được thực hiện để mô
 * phỏng lại chính xác các thao tác của người dùng nhập vào. Ngược lại, khi
 * #repaint() thì chỉ cần thực hiện các thao tác OpPair::effOp.
 *
 * \sa Canvas, MainWindow, OpPair, OperationList, addOperation(),
 * addFinishChangeShape(), repaint()
 */
class Player : public QObject {
    Q_OBJECT

public:

    // Constructors/Destructors
    //

    //! Phương thức khởi tạo
    Player (NewFileOption const *nfOption, QObject *parent = 0);

    //! Phương thức khởi tạo
    Player (QObject *parent = 0);

    //! Phương thức hủy
    ~Player();


    // Public methods
    //

    //! Thêm một thao tác vẽ
    void addOperation (PaintOperation *operation);

    //! Thêm thao tác kết thúc chuỗi các thao tác ChangeShape
    PaintOperation *addFinishChangeShape();

    //! Lưu danh sách các thao tác vẽ vào file
    void save (QFile &file);

    //! Tải danh sách các thao tác vẽ từ file
    bool load (QFile &file);

    //! Tải ảnh
    void loadImage (QImage const &image);

    //! Kiểm tra đối tượng Player có đang rỗng hay không
    /*!
     * Player rõng có nghĩa là trong #opPairList hiện không có thao tác nào
     * \return Player có rỗng hay không
     * \sa opPairList
     */
    bool isEmpty() {
        return opPairList.empty();
    }

    //! Thực hiện lại các thao tác
    void repaint (QPainter &painter);

    //! Thay đổi tỉ lệ phóng to, thu nhỏ
    void zoom (qreal sf);

    //! Trở về trạng thái trước khi thực hiện thao tác gần nhất
    void undo();
    
    //! Thực hiện lại thao tác vừa undo
    void redo (QPainter &painter);

    //! Thay đổi kích thước thực của Player
    void resizeBuffer (QSize const &newSize);

    //! Trả về buffer của đối tượng Player
    /*!
     * \return buffer
     * \sa buffer
     */
    QImage const *getBuffer() const {
        return buffer;
    }

    //! Trả về kích thước thực của Player
    /*!
     * \return kích thước thực của Player 
     * \sa originalSize
     */
    QSize size() {
        return originalSize;
    }

    //! Trả về màu nền
    /*!
     * \return màu nền
     * \sa __background
     */
    QColor background() {
        return __background;
    }

signals:

    // Signals
    //

    //! Player đã bắt đầu replay
    void playerStarted();

    //! Player đã kết thúc replay
    /*!
     * Tín hiệu được phát ra khi quá trình replay đã kết thúc. Có thể là do đã
     * thực hiện hết tất cả các thao tác hoặc là do người dùng nhấn nút kết
     * thúc.
     */
    void playerStopped();

    //! Chuyển sang thao tác tiếp theo trong chế độ replay
    void nextOperation(QRectF const &);

    //! Player đã thay đổi nội dung
    void playerModified (bool b);

    //! Player đã thay đổi khả năng undo
    /*!
     * Khi Player đang rỗng, nếu thêm một thao tác vào thì khả phát ra
     * canUndoChanged(true). 
     * Khi Player chỉ còn 1 thao tác, nếu gọi undo thì
     * Player rỗng, tín hiệu canUndoChanged (false) được phát ra.
     * \param b Player có thể undo hay không
     * \sa undo(), #opPairList
     */
    void canUndoChanged (bool b);

    //! Player đã thay đổi khả năng redo
    /*!
     * Khi #undoStack đang rỗng, nếu thực hiện #undo() thì sẽ phát ra tín hiệu
     * canRedoChanged (true).
     * Khi undoStack chỉ còn 1 phần tử, nếu thực hiện #redo() thì sẽ phát ra
     * canRedoChanged (false).
     * Thêm một thao tác, undoStack sẽ bị xoá, canRedoChanged (false) được phát
     * ra.
     * \param b Player có thể redo được hay không
     * \sa redo(), undo(), #undoStack 
     */
    void canRedoChanged (bool b);

public slots:
    // Public slots
    //

    //! Bắt đầu replay
    void play();

    //! Tạm dừng quá trình replay
    void pause();

    //! Tiếp tục quá trình replay
    void resume();

    //! Kết thúc quá trình replay
    void stop();

    //! Thay đổi thời gian chờ giữa 2 thao tác liên tiếp
    /*!
     * \param speed tốc độ replay
     * \sa timer
     */
    void setIntervalBySpeed (int speed) {
        timer.setInterval (10000 / speed);
    }

private slots:

    // Private slots
    //

    //! Kết thúc thời gian chờ
    void timerTimeout();

private:

    // Private attributes
    //

    //! Danh sách các cặp thao tác vẽ
    OpPairList opPairList;

    //! Stack gồm các cặp thao tác đã bị loại ra khỏi opPairList do thực hiện \
    undo
    OpPairList undoStack;

    //! Cặp thao tác vẽ hiện tại (đang replay)
    OpPairList::const_iterator currentPair;

    //! Thao tác sẽ thực hiện tiếp theo sau khi hết thời gian chờ
    OperationList::const_iterator currentOp;

    //! Đối tượng vẽ phục vụ cho quá trình replay
    QPainter *painter;

    //! buffer phục vụ cho quá trình replay
    QImage *buffer;

    //! Nội dung bị thay đổi bởi thao tác gần nhất
    QSharedPointer<QImage> changedPart;

    //! Điểm ở góc phía trên bên trái của phần bị thay đổi
    QSharedPointer<QPointF> lastTopLeft;

    //! Hệ số phóng to / thu nhỏ
    qreal zoomFactor;

    //! Kích thước thực của Player (cũng là của bức vẽ)
    QSize originalSize;

    //! Màu nền
    QColor __background;

    //! Đối tượng đếm thời gian chờ giữa 2 thao tác
    QTimer timer;

    //! Player có đang trong chế độ replay hay không
    bool isActive;

private:
    // Private method
    //

    //! Xoá hết tất cả trạng thái hiện tại
    void clearAll();
};

#endif // PLAYER_HH
